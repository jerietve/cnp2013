package nl.vu.cs.cn;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import android.util.Log;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.util.Interceptor;

/**
 * This class represents a TCP stack. There is really nothing to see here, because all the action
 * happens in its inner class Socket. Sockets can be created using the socket() method of TCP. You
 * are strongly adviced never to create and use multiple sockets on the same TCP stack, because
 * multiplexing is not supported.
 */
public class TCP
{
	public static final int LOG_LEVEL = 1;
	public static final boolean DEBUG = LOG_LEVEL <= 1;
	public static final boolean INFO = LOG_LEVEL <= 2;
	public static final boolean WARN = LOG_LEVEL <= 3;
	public static final boolean ERROR = LOG_LEVEL <= 4;
	
	/* TCP is protocol number 6 */
	public static final short PROTOCOL = 6;
	/* The maximum value of the IP packet identification number */
	protected static final int MAX_IP_ID = 65536;	
	/* Log base tag */
	private static final String BASE_TAG = "CNP_TCP";
	/* The ID of the next IP packet to send. Some implementations increment this number per socket,
	 * some always set it to zero, some do global incrementing per TCP stack. We take the last
	 * option. */
	protected int nextIPID;
	
	/** The underlying IP stack for this TCP stack. */
	protected IP ip;
	
	
	private ArrayList<Socket> socketList;
	
	/**
	 * This class represents a TCP socket. It provides public methods and constants related to
	 * socket behavior. A socket can only be created by calling {@link TCP#socket()} or
	 * {@link TCP#socket(int)}.
	 * 
	 * The Socket depends for all of its activity on a separate thread which is sending and
	 * receiving packets in the background. This thread can be told either write or read packets, or
	 * to connect, accept or close a connection. This background thread, of type SocketThread, will
	 * continue taking requests for operations, while the main Socket may be blocked on a call such
	 * as {{@link #accept()} or {@link #read(byte[], int, int)}. 
	 * 
	 * //TODO explain how you can use multiple threads to do a call to write() while another thread
	 * is doing a call to read() on the same TCP.Socket.. Because Jeroen doesn't understand yet :)
	 */
	public class Socket
	{		
		/* The highest allowed port number */
		public static final int MAX_PORT_NUMBER = 65535;
		/* Ports at or above this number can be bound to by any process */
		public static final int MIN_FREE_PORT_NUMBER = 1024;
		/* The maximum number of transmissions before giving up */
		public static final int MAX_TRANSMISSIONS = 10;
		/* The maximum time in seconds to wait for a reply, before assuming it is lost and doing
		 * a retransmission of the packet. */
		public static final int MAX_TIME_BEFORE_RETRANSMISSION = 1;
		/* The time we have to wait (in state TIME_WAIT) before we can reset the socket in order to 
		 * reuse it	 */
		public static final int CONNECTION_WAIT_BEFORE_RESET_TIME = 1;
		
		/** The thread that does the actual work for the Socket */
		private SocketThread socketThread;
		
		/* Some variables used for reading */
		private ByteBuffer currentPacketByteBuffer;
		private PacketData currentPacketData;
		
		/**
		 * Construct a client socket.
		 */
		private Socket()
		{
			int localPort = 0;

			for(int i = MIN_FREE_PORT_NUMBER; i <= Socket.MAX_PORT_NUMBER; i++)
			{
				boolean taken = false;
				for(Socket socket : socketList)
				{
					if(socket.getLocalPort() == i)
					{
						taken = true;
						break;
					}
				}
				if(taken) continue; 
				localPort = i;
				break;
			}
			if(localPort == 0) throw new RuntimeException("All ports above " + MIN_FREE_PORT_NUMBER +
					" already bound to sockets");
			
			socketThread = new SocketThread(TCP.this, localPort);
		}
		
		
		/**
		 * Construct a server socket bound to the given local port.
		 * 
		 * @param port the local port to use
		 */
		private Socket(int port)
		{
			/* If all possible port numbers above MIN_FREE_PORT_NUMBER have been taken, the default
			 * constructor will throw an exception, making it impossible to create a low numbered
			 * port even though it may be available. This is so unlikely that I'm going to stop
			 * writing this comment. */
			this();
			if(port < 0 || port > MAX_PORT_NUMBER)
			{
				throw new IllegalArgumentException("Invalid port number: " + port);
			}

			socketThread = new SocketThread(TCP.this, port);
		}
		
		
		/**
		 * Connect this socket to the specified destination and port.
		 * 
		 * @param dst the destination to connect to
		 * @param port the port to connect to
		 * @return true if the connect succeeded.
		 */
		public boolean connect(IpAddress dst, int port)
		{
			if(port < 0 || port > MAX_PORT_NUMBER)
			{
				throw new IllegalArgumentException("Port number out of range!");
			}
			
			/* Lets the socketThread connect. The actual thread will start as soon as connect was
			 * successful! */
			return socketThread.connect(dst, port);
		}
		
		
		/**
		 * Accept a connection on this socket. This call blocks until a connection is made. A new
		 * connection cannot be accepted while another one is still active. Trying to do so will
		 * result in this function returning immediately, without a connection having been set up.
		 */
		public void accept()
		{
			socketThread.accept();
		}
		
		
		/**
		 * Attempts to read up to maxlen bytes into "buf", starting at "offset". If insufficient
		 * data is received from the network, this method may block indefinitely until it reads
		 * the requested data.
		 * 
		 * In case the connection is, or ends up, in the CLOSE_WAIT, CLOSING or TIME_WAIT state
		 * during this call, only data that had already been buffered will be returned. If there is
		 * no such data available, 0 is returned.
		 * 
		 * @param buf		the buffer to read into. May not be null
		 * @param offset    the offset to begin reading data into
		 * @param maxlen    the maximum number of bytes to read
		 * @return 			the number of bytes read, or -1 if an error occurs.
		 */
		public int read(byte[] buf, int offset, int maxlen)
		{
			if(DEBUG) Log.d(sockTag(), "Socket.read() called");
			int bytesToCopy = 0;
			int bytesCopied = 0;		
		
			/* Check if a buffer is given */
			if (buf == null)
			{
				if(ERROR) Log.e(sockTag(), "Buf must not be null!");
				return -1;				
			}
			
			/* Test if offset and maxlen are good! */
			if (offset < 0 || maxlen < 0 || offset+maxlen > buf.length)
			{
				if(ERROR)
					Log.e(sockTag(), "Read: offset or maxlen is negative or their sum exceeds the buffer length!");
				return -1;					
			}
			
			/* Reads of zero-length are always successful */
			if (maxlen == 0) return 0;
			
			if(socketThread.isConnecting())
			{
				if(ERROR) Log.e(sockTag(), "Read: reading is not allowed while we are connecting.");
				return 0;
			}
			
			try
			{
				while (bytesCopied < maxlen)
				{
					/* When we have no "current buffer", read() new data to fill it */
					if (currentPacketData == null)
					{
						/* Read a packet and make it accessible through a new ByteBuffer */
						currentPacketData = socketThread.readPacket();
						if (currentPacketData == null) break;
						currentPacketByteBuffer = ByteBuffer.wrap(currentPacketData.getData(), 0, 
																  currentPacketData.getLength());						
					}
					
					/* Copy bytes from the current buffer to the user buffer */
					bytesToCopy = Math.min(currentPacketByteBuffer.remaining(), maxlen - bytesCopied);
					currentPacketByteBuffer.get(buf, offset + bytesCopied, bytesToCopy);
					bytesCopied += bytesToCopy;
					
					/* When the current buffer is empty, free it! */
					if (!currentPacketByteBuffer.hasRemaining())
					{
						currentPacketByteBuffer = null;
						socketThread.freePacket(currentPacketData);
						currentPacketData = null;
					}				
				}
				
				/* Return the number of bytes that were read (always equal to maxlen!!!) */
				return bytesCopied;
			}
			catch (NoMoreDataException nmde)
			{
				/* Halfway the read, we discovered that there was no more data. Return the data that
				 * was read. When nothing was read, return 0 */
				if(ERROR) Log.e(sockTag(), "read: Failed. No more data can be read!");
				if (bytesCopied > 0) 	return bytesCopied;
				else					return 0;
			}
			catch (IOException ioe)
			{
				/* Halfway reading, a fatal error occurred. If data was read, return the number of
				 * read bytes. If nothing was read, return -1. */
				if(ERROR) Log.e(sockTag(), "read: Failed because of an IOException!");
				if (bytesCopied > 0) 	return bytesCopied;
				else					return -1;				
			}
		}
		
		/**
		 * Writes to the socket from the buffer.
		 * 
		 * @param buf 		the buffer containing data to be written. May not be null
		 * @param offset	the offset to begin writing data from
		 * @param len		the number of bytes to write
		 * 
		 * @return the number of bytes written or -1 if an error occurs.
		 */
		public int write(byte[] buf, int offset, int len)
		{
			/* Check if a buffer is given */
			if (buf == null)
			{
				if(ERROR) Log.e(sockTag(),"Buf must not be null!");
				return -1;				
			}	
			
			/* Test if offset and len are good! */
			if (offset < 0 || len < 0 || offset+len > buf.length)
			{
				if(ERROR) 
					Log.e(sockTag(),"Write: offset or len is negative or their sum exceeds the buffer length!");
				return -1;					
			}
			
			/* Writes of zero-length are always successful */
			if (len == 0) return 0;

			if(!socketThread.isWritingAllowed())
			{
				if(ERROR) Log.e(sockTag(),"Write: writing is not allowed in this state!");
				return -1;
			}
			
			int nextIndexToSend = offset;
			int lastIndexToSend = offset + len;
			int bytesWritten = 0;
			
			/* Send the data in steps of maximum packet size, until all has been sent */
			try
			{
				while(nextIndexToSend < offset + len)
				{
					int numBytesInPacket = Math.min(TCPPacket.MAX_TCP_DATA, lastIndexToSend - nextIndexToSend);
					boolean morePacketsComing = lastIndexToSend - nextIndexToSend > TCPPacket.MAX_TCP_DATA;
					
					/* Grab a data packet, fill it with the data and dispatch it to the socketThread */
					PacketData data = socketThread.newPacket();
					data.fromBuffer(buf, nextIndexToSend, numBytesInPacket);
					bytesWritten = socketThread.writePacket(data, morePacketsComing);
					if (bytesWritten == -1)
					{
						if(ERROR) Log.e(sockTag(),"Error in write(). Written "+(nextIndexToSend - offset)+" bytes."); 
						break;
					}
					else
					{
						/* bytesWritten is either 0 or numBytesInPacket. Nothing else! */
						nextIndexToSend += bytesWritten;
					}
				}
			}
			catch (IOException ioe)
			{
				/* An internal IOException was thrown, so if we have not written anything yet,
				 * return -1 to report the error */
				if (nextIndexToSend == offset) return -1;
			}
			
			/* If nothing was written, return -1 */
			return nextIndexToSend - offset == 0 ? -1 : nextIndexToSend - offset;
		}
		
		
		/**
		 * Closes the connection for this socket. If there is no connection (it was closed before,
		 * and connect() or accept() never succeeded), it returns false and nothing happens. If
		 * there is a connection, the remote is told we want to quit. The host then has ten seconds
		 * to send us any data is still wants to write, and then tell us it also wants to quit. If
		 * this does not happen within that time, the connection is just reset without confirmed
		 * approval from the other side. It could take 11 seconds, give or take, because the socket
		 * thread might be stuck in a call to the IP layer before it notices it needs to close.
		 * 
		 * @return true unless no connection was open.
		 */
		public boolean close()
		{
			return socketThread.close();
		}		
		
		/**
		 * @return The local port number
		 */
		protected int getLocalPort()
		{
			return socketThread.getLocalPort();
		}
		
		/* Get a unique sockTag() for this socket so we can distinguish it in the logfile */
		private String sockTag()
		{
			return BASE_TAG+"_"+getLocalPort();
		}
		
		
		/**
		 * Allows tests to install interceptors, which allows them to drop/corrupt packets in a
		 * controlled fashion.
		 * 
		 * @param interceptors The interceptors which live between the TCP and IP layer
		 */
		protected void installInterceptors(ArrayList<Interceptor> interceptors)
		{
			this.socketThread.installInterceptors(interceptors);
		}
		
		/**
		 * Directly sets the Initial sequence number of the socket. This should be called before
		 * connect or accept!
		 */
		protected void setISN(long isn)
		{
			this.socketThread.setISN(isn);
		}
	}
	
	
	/**
	 * Constructs a TCP stack for the given virtual address. The virtual address for this TCP stack
	 * is then 192.168.1.address.
	 * 
	 * @param 	address	The last octet of the virtual IP address 1-254.
	 * @throws 	IOException if the IP stack fails to initialize.
	 */
	public TCP(int address) throws IOException
	{
		ip = new IP(address);
		nextIPID = 1;
		socketList = new ArrayList<TCP.Socket>();
	}
	
	
	/**
	 * @return a new socket for this stack
	 */
	public Socket socket()
	{
		Socket newSocket = new Socket();
		socketList.add(newSocket);
		return newSocket;
	}
	
	
	/**
	 * @return a new server socket for this stack bound to the given port
	 * @param port the port to bind the socket to.
	 */
	public Socket socket(int port)
	{
		for(Socket socket : socketList)
		{
			if(socket.getLocalPort() == port)
			{
				throw new IllegalArgumentException("A socket is already bound to port " + port);
			}
		}
		Socket newSocket = new Socket(port);
		socketList.add(newSocket);
		return newSocket;
	}
	
	/**
	 * Retrieves the local address of this TCP instance
	 * @return
	 */
	protected IpAddress getLocalAddress() 
	{
		return this.ip.getLocalAddress();
	}
}
