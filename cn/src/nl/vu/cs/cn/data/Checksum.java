package nl.vu.cs.cn.data;

import java.nio.ByteBuffer;

/**
 * This class provides one public static method, whose {@link #calculate(ByteBuffer...) JavaDoc} you
 * should probably read.
 */
public class Checksum
{
	/**
	 * Avoid the instantiation of this class of static methods.
	 */
	private Checksum()
	{
		
	}
	
	
	/**
	 * Calculates the TCP header checksum. Pads the array with a zero byte if the number of bytes is
	 * uneven. Then takes every pair of bytes as a 16 bit number, and adds all of them, taking the
	 * carry over to the least significant bit. Finally the result is complemented and returned.
	 * This method does not mutate any of the ByteBuffers.
	 * 
	 * This method can be used to perform the TCP header type checksum on any data you want. For
	 * example, you can use it to check certain received bytes against the bytes you expected to
	 * receive, checking less or more than exactly one packet.
	 * 
	 * @param buffers
	 *            A number of ByteBuffers for which we want the checksum. This
	 *            should be the pseudo-header, TCP header and the data provided
	 *            by the application and nothing else.
	 * @return The TCP header checksum
	 */
	public static short calculate(ByteBuffer... buffers)
	{
		if(buffers == null) return (short)0xffff;
		
		/* Use int internally to avoid constant casting. The highest 16 bits will always be zero
		 * because upon carry-over into the 17th bit, this bit is immediately discarded. One is also
		 * added to the value, but this can never cause another carry. */
		int checksum = 0;
		
		/* "uneven" Indicates that the previous bytebuffers together contained an uneven number of
		 * bytes. "lastByte" contains that last byte. "b1", "b2" hold the bytes read from the
		 * buffers */
		boolean uneven = false;
		byte lastByte = 0;
		
		/* Compute the checksum for the given sequence of buffers */
		for (ByteBuffer originalBuf: buffers) 
		{
			if(originalBuf == null) continue;
			ByteBuffer buf = originalBuf.duplicate();
			buf.rewind();
			if(buf.limit() - buf.position() == 0) continue;
			
			/* Last buffer had an uneven length. Take lastByte and the first byte of the new buffer */
			if (uneven) 
			{
				checksum = addBytes(lastByte,  buf.get(0), checksum);
			}
			
			/* Iterate the bytes of the buffer. Add an offset of one byte, if the previous buffers
			 * together had an uneven length. negate "uneven" if we are left with one byte at the
			 * end, and save that last byte. */
			for(int i = ((uneven) ? 1 : 0); i < buf.limit(); i += 2)
			{
				/* When the buffer has a uneven number of bytes and we are at the last one Save this
				 * byte and move on with the next buffer. In the other case, just use the next two
				 * bytes */
				if (i == buf.limit() - 1)
				{
					lastByte = buf.get(i);
					uneven = true;
					break;
				}
				else 
				{
					checksum = addBytes(buf.get(i), buf.get(i+1), checksum);
					uneven = false;
				}				
			}
		}	
		
		/* In case of a total uneven number of bytes, add padding of zeros */
		if (uneven) 
		{
			checksum = addBytes(lastByte, (byte)0, checksum);
		}
		
		return (short)(~checksum);
	}
	
	
	/**
	 * Modifies the checksum for the given two additional bytes. 
	 * 
	 * @param	b1 			The first byte
	 * @param	b2 			The second byte
	 * @param	checksum	The old checksum
	 * @return				The new checksum for the old buffer with b1 and b2 appended.
	 */
	private static int addBytes(byte b1, byte b2, int checksum) 
	{
		/* First cut off the high 1s possibly padded to b2, then finally the 1s possibly padded
		 *  to the result */
		int word16 = ((b1 << 8) | b2 & 0xff) & 0xffff;
		checksum += word16;
		if(checksum >> 16 == 1) checksum = (checksum & 0x0000ffff) + 1;		
		return checksum;
	}
}
