package nl.vu.cs.cn.data;

import java.math.BigInteger;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;

/**
 * This class contains static methods which allow reading and writing of the least significant x
 * bytes of certain primitive types into a ByteBuffer. The write methods all mutate the data
 * underlying the provided ByteBuffer and increase its position. It is tested whether the data to be
 * written would fit, and if not, a BufferOverflowException will be thrown. If the buffer is
 * read-only, a ReadOnlyBufferException is thrown immediately.
 * 
 * NOTE: The write (put) methods for this class have been deprecated because you can just use the
 * default methods provided by {@link ByteBuffer}
 */
public class BufferHelper
{
	/**
	 * Avoid the instantiation of this class of static methods.
	 */
	private BufferHelper()
	{
		
	}
	
	
	/**
	 * Takes the two least significant bytes of value and stores them, big-endian, in buf.
	 * 
	 * @param buf	The buffer to add the unsigned short value to
	 * @param value	The integer representing the unsigned short [0, 2^16)
	 * @deprecated	Instead of using this method, simply cast your int to a short and use
	 * 				{@link ByteBuffer#putShort(short)}
	 */
	@Deprecated
	public static void putUnsignedShort(ByteBuffer buf, int value)
	{
		if(buf.limit() - buf.position() < 2) throw new BufferOverflowException();
		if((value & 0xffff0000) != 0)
				throw new IllegalArgumentException("Value must be in range [0, 2^16) but is: " + value);
		buf.put((byte)(value >> 8));
		buf.put((byte)(value));
	}
	
	
	/**
	 * Takes the four least significant bytes of value and stores them, big-endian, in buf.
	 * 
	 * @param buf	The buffer to add the unsigned short value to
	 * @param value	The long representing the unsigned integer [0, 2^32)
	 * @deprecated	Instead of using this method, simply cast your long to an int and use
	 * 				{@link ByteBuffer#putInt(int)}
	 */
	@Deprecated
	public static void putUnsignedInteger(ByteBuffer buf, long value)
	{
		if(buf.limit() - buf.position() < 4) throw new BufferOverflowException();
		if(value >= 4294967296L || value < 0)
				throw new IllegalArgumentException("Value must be in range [0, 2^32) but is: " + value);
		buf.put((byte)(value >> 24));
		buf.put((byte)(value >> 16));
		buf.put((byte)(value >> 8));
		buf.put((byte)value);
	}
	
	
	/**
	 * Reads the next two bytes from buf, returning the value made by using the first as the second
	 * least significant byte of an integer, and the second one as the least significant.
	 * 
	 * @param buf	The buffer to read from. It should have two more bytes from position before
	 * 				limit
	 * @return	The integer constructed from the two bytes
	 */
	public static int getUnsignedShort(ByteBuffer buf)
	{
		if(buf.limit() - buf.position() < 2) throw new BufferOverflowException();
		/* Get the two bytes, as ints to make them insusceptible to 1-padding. Then shift and
		 * combine. */
		int b3 = buf.get() & 0xff;
		int b4 = buf.get() & 0xff;
		return (b3 << 8) | b4;
	}
	
	
	/**
	 * Reads the next four bytes from buf, returning the value made by using them as the least
	 * significant bytes of a long, from most to least significant within the group of four.
	 * 
	 * @param buf	The buffer to read from. It should have four more bytes from position before
	 * 				limit
	 * @return	The long constructed from the four bytes
	 */
	public static long getUnsignedInteger(ByteBuffer buf)
	{
		/* This method cannot work the same way as the one for short. This is because bitwise
		 * operators work on integers. That means that if the most significant byte starts with a 1,
		 * conversion to long would result in a negative number. This cannot be taken care of with
		 * a bit mask, because, again, all bitwise operations take place on integers. Therefore, we
		 * make use of the BigInteger class. The constructor used here takes an array of bytes, and
		 * creates an arbitrarily large value using two-complements notation. This means that again
		 * it would be a problem if the most significant bit were 1. Therefore we add a zero byte at
		 * the beginning of the array. */
		byte[] intBytes = new byte[5];
		buf.get(intBytes, 1, 4);
		return new BigInteger(intBytes).longValue();
	}
}
