package nl.vu.cs.cn;

import java.util.Random;

import nl.vu.cs.cn.TCPPacket.FlagSet;

import android.util.Log;


/**
 * Implements the finite state machine for the TCP protocol, using the flags of received packets. It
 * also keeps track of the sequence and acknowledgment numbers.
 * 
 * Each time a packet is received, it must be provided to the {@link #processInbound(TCPPacket)}
 * method. The internal state of what we expect to receive next is then updated, possibly changing
 * sequence and acknowledgment numbers and the connection state. What exactly happens depends on
 * two things: the current connection state, and the type  of the incoming packet. Here "type"
 * really means two things. First is the flags. A FIN packet requires different action than an ACK
 * packet. The second is whether the packet is a reply to the packet we currently have in transit,
 * or a simultaneously sent packet, or a retransmitted packet. This method returns true if the
 * received packet is allowed in this state, and false if it should be ignored. Deciding what needs
 * to be done in response to the given packet, is not the responsibility of TCPState, but of
 * {@link SocketThread}.
 * 
 * The type of packet depending on flags is described in {@link FlagSet}
 * The type of packet like retransmission or reply is described in {@link ReceiveType}
 * The different states a connection can be in are described in {@link ConnectionState}
 * 
 * Before sending a packet, {@link #processOutbound(TCPPacket)} is called by SocketThread. This
 * method sets the sequence number and acknowledgment number of that packet. 
 * 
 * If you have access to the source, having a look at the code for the
 * {@link #transition(FlagSet, ReceiveType)} method will be a great help in understanding the state
 * transitions in case of different current states and incoming packets. These have been designed
 * exactly as they should happen according to the TCP protocol standard.
 * 
 * TCPState keeps the following state:
 * - The current TCP state (e.g CLOSED, LISTEN, SYN_SENT...)
 * - The next sequence number to use for a packet that we will send
 * - The next sequence number of the next packet we expect to receive
 */

public class TCPState 
{
	/** All the states our TCP connection can be in */
	public enum ConnectionState 
	{
		/** The default state for a connection that is not set up */
		S_CLOSED, 
		/** The server socket is accept()'ing new connections */
		S_LISTEN, 
		/** The client has sent SYN as part of the three-way handshake */
		S_SYN_SENT, 
		/** The server has received SYN from the client and replied with SYNACK */
		S_SYN_RECEIVED, 
		/** The connection is established */
		S_ESTABLISHED,
		/** We have sent FIN and we wait for the remote to close or to acknowledge our FIN */
		S_FIN_WAIT_1, 
		/** The remote has acknowledged our FIN. The connection is "half open".
		 * Now we wait for the remote to close. */
		S_FIN_WAIT_2,
		/** We have sent FIN and received FIN from the remote. We have acknowledged the FIN from the 
		 * remote and we wait waiting for the remote to acknowledge ours (Simultaneous close!) */
		S_CLOSING,		
		/**	We have received FIN from the remote and we are waiting for a close from the local
		 * program.	*/
		S_CLOSE_WAIT, 		
		/** The connection is closed, but we must wait to make sure that all packets are either
		 * dropped or delivered at the remote (most importantly the last ACK) */
		S_TIME_WAIT, 
		/**	The remote is now in TIME_WAIT and has sent his last ACK. We need to wait for it, before
		 * we can finally close. */
		S_LAST_ACK;
	}
	
	/**
	 * Used to classify packets based on their sequence and acknowledgment numbers, with respect to
	 * the current state.
	 * 
	 * NOTE: There are special cases for RT_SYN and RT_SYNACK since we first need to exchange these
	 * before both sides have all the required sequence numbers set up to find out what kind of
	 * receive type they are dealing with.
	 */
	public enum ReceiveType 
	{
		/** The packet has invalid sequence/acknowledgment numbers and should be ignored */
		RT_INVALID,
		/** The first packet (SYN)*/
		RT_SYN,
		/** The second packet (SYNACK) */
		RT_SYNACK,
		/** This packet is the reply to the last packet we have sent */
		RT_REPLY,
		/** This packet is the next packet we expected to receive, but is not a reply to the
		 * last sent packet. This packet and the last packet we have sent were in transit 
		 * simultaneously. */
		RT_SIMULTANEOUS,
		/** This packet was already received, but the remote resent it because of a lost ACK */
		RT_RETRANSMITTED
	}
	
	/** The current state of the connection */
	private ConnectionState state;
	
	/** Our local initial sequence number */
	private long localISN;
	/** An application defined local ISN (for some tests) */
	private long applicationDefinedISN = -1;
	/** The remote initial sequence number */
	private long remoteISN;	
	/** The sequence number of the current outbound packet */
	private long sendNextSeq;
	/** Length of current outbound packet (SYN/FIN flags count as 1 and are included in this length) */
	private long bytesInTransit;	
	/** The expected sequence number of the next inbound packet, in other words, the 
	 * acknowledgment number of the current outbound packet. */
	private long receiveNextSeq;
	/** The type of the last received packet */
	private ReceiveType lastReceiveType;
	/** Size of the last received packet */
	private int lastReceivedPacketLength;
	
	/** logtag to be used for logging */
	private String logtag;

	
	public TCPState(String logtag) 
	{
		state = ConnectionState.S_CLOSED;
		this.logtag = logtag;
		reset();
	}
	
	
	/**
	 * Updates the TCP state according to a new packet that has been received. Such an update only
	 * happens when the given packet is valid in the current context.
	 * 
	 * This method must not be called in states S_CLOSED or S_TIME_WAIT because we should not
	 * process any incoming packets in these states. 
	 * 
	 * @param in		The packet that was received
	 * @return 			True if "in" is a valid packet and the state is updated.
	 * 					In other cases, false is returned.
	 */
	public boolean processInbound(TCPPacket in) 
	{
		/* Determine the receive type of this packet */
		lastReceiveType = getReceiveType(in);
		
		/* Save its length */
		lastReceivedPacketLength = getPacketLength(in);
		
		/* Attempt to do a transition */
		ConnectionState newState = transition(in.getFlags(), lastReceiveType);
		
		if(TCP.DEBUG) Log.d(logtag, "processInbound: State " + state + ", Flags " + in.getFlags() + ", Receive-type " +
				lastReceiveType.name() + ", Data? " + in.hasData());
		
		if (newState != null) 
		{			
			/* Update the state */
			setState(newState);
			
			/* The packet is valid. Now only update sequence numbers when it is actually new! */
			if (lastReceiveType != ReceiveType.RT_RETRANSMITTED)	processSequenceNumbers(in);
			
			return true;
		}			
		else
			return false;
	}
	
	/**
	 * Processes a packet before it is sent to the remote. This method sets the right sequence and
	 * acknowledgment numbers that are kept by TCPState. 
	 * 
	 * @param out 			The packet we are going to send to the remote
	 */
	public void processOutbound(TCPPacket out)
	{
		if (bytesInTransit > 0 && bytesInTransit != getPacketLength(out) && getPacketLength(out) != 0)
			throw new IllegalArgumentException("Its illegal to retransmit a different packet. Wait for ACK first.");
		
		if (sendNextSeq == -1)
		{
			/* This is the first packet we will send. Generate new ISN!
			 * However, if the application (some tests do this) specified an ISN to use... use that one!  */
			if (applicationDefinedISN == -1)
				localISN  	= Math.abs(new Random().nextLong()) % (TCPPacket.MAX_TCP_SEQUENCE + 1);
			else
				localISN	= applicationDefinedISN;
			sendNextSeq = localISN;
		}
		
		/* Store the length (SYN/FIN included) of the outbound packet, unless this is a reply to a
		 * simultaneous or retransmitted packet. */
		if(lastReceiveType != ReceiveType.RT_SIMULTANEOUS && lastReceiveType != ReceiveType.RT_RETRANSMITTED)
			bytesInTransit = getPacketLength(out);	

		/* Set the sequence numbers */
		out.setSequenceNumber(sendNextSeq);
		if(out.getFlags().hasAck()) 
		{
			out.setAcknowledgmentNumber(receiveNextSeq);
		}
		if(TCP.DEBUG) Log.d(logtag, "processOutbound: State " + state + ", Flags: " + out.getFlags().toString() +
				", SEQ " + out.getSequenceNumber() + ", ACK " + out.getAcknowledgmentNumber() + ", Data? " +
				out.hasData());
	}
	
	/**
	 * Resets the flags, sequence and acknowledgment numbers so that a new connection can be
	 * established. This must be done during accept() and connect() and can only be done while the
	 * state is already the fictional CLOSED state.
	 */
	public void reset()
	{
		if (this.state == ConnectionState.S_CLOSED)
		{
			/* Reset the connection */
			localISN  		= -1;
			remoteISN 		= -1;
			sendNextSeq 	= -1;
			receiveNextSeq 	= -1; 					
			bytesInTransit 	= 0;
		}
		else
			throw new IllegalStateException("Reset() is illegal on a connection that is not closed!");
	}
	
	
	/**
	 * Let the state know it should forget about any bytes in transit
	 */
	public void writeFailed()
	{
		bytesInTransit = 0;
		lastReceiveType = ReceiveType.RT_INVALID;
	}
	
	
	/**
	 * Returns the current state of the connection.
	 * @return
	 */
	public ConnectionState getState()
	{
		return this.state;
	}
	
	/**
	 * Sets the state of the connection
	 * 
	 * @param state
	 */
	protected void setState(ConnectionState state)
	{
		if (state != this.state)
		{
			if(TCP.DEBUG) Log.d(logtag, "Transition from " + this.state+" to " + state);
			this.state = state;
		}
	}
	
	/**
	 * Returns the receive-type of the last packet that was given to processInbound().
	 * 
	 * @return The receive-type of the last received packet.
	 */
	public ReceiveType getLastReceiveType()
	{
		return this.lastReceiveType;
	}
	
	/**
	 * Returns the state we should transition into when receiving a packet with the given flags
	 * and the given receive type.
	 * 
	 * @param flags		The flags of a received packet
	 * @param rt		The receive-type of a received packet
	 * @return			Returns:
	 * 					> A new state, when the flags/rt caused a state change
	 * 					> The state we are in, when the flags/rt are valid but do not cause a
	 * 						transition
	 * 					> NULL, when flags/rt are invalid.
	 */
	private ConnectionState transition(FlagSet flags, ReceiveType rt)
	{
		if (rt == ReceiveType.RT_INVALID)
			return null;
		
		switch (this.state)
		{
			case S_LISTEN:
				if (flags == FlagSet.F_SYN && rt == ReceiveType.RT_SYN) 
					return ConnectionState.S_SYN_RECEIVED;
				break;
			case S_SYN_SENT:
				if (flags == FlagSet.F_SYNACK && rt == ReceiveType.RT_SYNACK) 
					return ConnectionState.S_ESTABLISHED;
				break;
			case S_SYN_RECEIVED:
				if (flags == FlagSet.F_ACK && rt == ReceiveType.RT_REPLY) 
					return ConnectionState.S_ESTABLISHED;
				/* When our SYNACK was lost, we can get SYN retransmissions. However, we will ignore
				 * these (see design decision document). */
//				if (flags == FlagSet.F_SYN && rt == ReceiveType.RT_RETRANSMITTED) 
//					return ConnectionState.S_SYN_RECEIVED;
				/* When their ACK was lost, we could get a FIN packet */
				if(flags == FlagSet.F_FINACK && rt == ReceiveType.RT_REPLY)
					return ConnectionState.S_CLOSE_WAIT;
				break;
			case S_ESTABLISHED:
				/* Check for ACK packets that are part of a normal data transmission */
				if (flags == FlagSet.F_ACK)
					return ConnectionState.S_ESTABLISHED;
				/* When receiving a retransmitted SYNACK, just accept it */
				if (flags == FlagSet.F_SYNACK || rt == ReceiveType.RT_RETRANSMITTED)
					return ConnectionState.S_ESTABLISHED;
				/* When receiving FINACK, the remote wants to close */
				if (flags == FlagSet.F_FINACK && (rt == ReceiveType.RT_REPLY || rt == ReceiveType.RT_SIMULTANEOUS))
					return ConnectionState.S_CLOSE_WAIT;
				break;
			case S_CLOSE_WAIT:
				/* In CLOSE_WAIT, we can send data and thus receive ACKs (or possibly an ACK to the
				 * last data packet we sent in ESTABLISHED, but this is also a reply ACK. */
				if (flags == FlagSet.F_ACK && rt == ReceiveType.RT_REPLY)
					return ConnectionState.S_CLOSE_WAIT;
				/* We might also receive a retransmit of the FIN packet that got us into this state */
				if(flags == FlagSet.F_FINACK && rt == ReceiveType.RT_RETRANSMITTED)
					return ConnectionState.S_CLOSE_WAIT;
				break;
			case S_LAST_ACK:
				/* In case a new ACK comes in, that is that famous last ACK! */
				if (flags == FlagSet.F_ACK && rt == ReceiveType.RT_REPLY)
					return ConnectionState.S_CLOSED;
				/* An old FINACK too must simply be acked */
				if(flags == FlagSet.F_FINACK && rt == ReceiveType.RT_RETRANSMITTED)
					return ConnectionState.S_LAST_ACK;
				break;
			case S_FIN_WAIT_1:
				/* If we received a reply to our FIN, go to FIN_WAIT_2 */
				if (flags == FlagSet.F_ACK && rt == ReceiveType.RT_REPLY)
					return ConnectionState.S_FIN_WAIT_2;
				/* When our FIN was transmitted simultaneously with a last data packet, we must
				 * accept it */
				if (flags == FlagSet.F_ACK && rt == ReceiveType.RT_SIMULTANEOUS)
					return ConnectionState.S_FIN_WAIT_1;
				/* When the remote replies with a FINACK after seeing our FIN, his FIN was actually
				 * lost, so we see a retransmission. The remote knows that it wants to close but
				 * also knows that we want to close, so it can acknowledge our FIN while sending
				 * his. This results in a 3-way teardown */
				if (flags == FlagSet.F_FINACK && rt == ReceiveType.RT_REPLY)
					return ConnectionState.S_TIME_WAIT;
				/* When we send and receive a FINACK simultaneously, we are in a "simultaneous
				 * close". */
				if (flags == FlagSet.F_FINACK && rt == ReceiveType.RT_SIMULTANEOUS)
					return ConnectionState.S_CLOSING;	
				/* Our ACK to their SYNACK was lost and we called close() immediately, so we get an
				 * old SYNACK. ACK it. */
				if(flags == FlagSet.F_SYNACK && rt == ReceiveType.RT_RETRANSMITTED)
					return ConnectionState.S_FIN_WAIT_1;
				break;
			case S_FIN_WAIT_2:
				/* We can receive data and retransmissions here, since the connection is half-open */
				if (flags == FlagSet.F_ACK && (rt == ReceiveType.RT_REPLY || rt == ReceiveType.RT_RETRANSMITTED))
					return ConnectionState.S_FIN_WAIT_2;
				/* When receiving a FINACK, we transition to TIME_WAIT */
				if (flags == FlagSet.F_FINACK && (rt == ReceiveType.RT_REPLY))
					return ConnectionState.S_TIME_WAIT;
				break;
			case S_CLOSING:
				/* Check whether we have received the last ACK as a reply to our FIN */
				if (flags == FlagSet.F_ACK && (rt == ReceiveType.RT_REPLY))
					return ConnectionState.S_TIME_WAIT;	
				/* When our ACK to the remote's FIN was lost, it may retransmit its FIN. We must
				 * accept it */
				if (flags == FlagSet.F_FINACK && (rt == ReceiveType.RT_RETRANSMITTED))
					return ConnectionState.S_CLOSING;	
				break;
			case S_CLOSED:
			case S_TIME_WAIT:
				/* We should not receive/process packets in these states */
		}
		
		return null;
	}
	
	/**
	 * Classifies a packet according to its sequence and acknowledgment number.
	 * 
	 * WARNING: This method must be called before sending the next packet. Sending a packet alters
	 * the state that is used to perform this check!
	 * 
	 * @param in		The received packet
	 * @return			The receive-type of this packet.
	 */
	private ReceiveType getReceiveType(TCPPacket in)
	{
		if (receiveNextSeq == -1)
		{
			/* This is the first packet we have ever received. Did we ever send one? */
			if (sendNextSeq == -1)
			{
				/* We have not sent a packet, so this must be the first SYN */
				return ReceiveType.RT_SYN;
			}
			else
			{
				/* Apparently we have send something (the SYN), so this must be SYNACK */
				return ReceiveType.RT_SYNACK;
			}
		}
		else if (in.getSequenceNumber() == receiveNextSeq)
		{
			/* This is the packet we expect to receive */
			if (in.getAcknowledgmentNumber() == incrSeqNum(sendNextSeq, bytesInTransit))
			{
				/* The remote has seen our last sent packet */
				return ReceiveType.RT_REPLY;
			} 
			else if (in.getAcknowledgmentNumber() == sendNextSeq)
			{
				/* The remote has not seen your last-sent packet yet */
				return ReceiveType.RT_SIMULTANEOUS;	
			} 
		}
		else if (seqWithinPacketRangeBelowExpected(in) && in.getAcknowledgmentNumber() == sendNextSeq ||
				in.getAcknowledgmentNumber() == 0)
		{
			/* We have already received this data but the remote has not seen our last-sent packet
			 * (ACK) 
			 * 
			 * NOTE: We must also allow acknowledgmentNumber == 0 here, because we need to recognize
			 * retransmitted SYN's (SYN packets always have acknowledgmentNumber == 0). */
			return ReceiveType.RT_RETRANSMITTED;
		}
		else if(in.getSequenceNumber() == incrSeqNum(receiveNextSeq, -lastReceivedPacketLength) &&
				in.getAcknowledgmentNumber() == sendNextSeq + bytesInTransit)
		{
			return ReceiveType.RT_REPLY;
		}

		if(TCP.DEBUG) Log.d(logtag, "INVALID packet! SEQ " + in.getSequenceNumber() + " ACK " +
				in.getAcknowledgmentNumber() + " recNextSEQ " + receiveNextSeq + " sendNextSeq " + sendNextSeq);
		return ReceiveType.RT_INVALID;
	}
	
	
	public boolean seqWithinPacketRangeBelowExpected(TCPPacket in)
	{
		/* A packet's sequence number is between 1 and a packet length (inclusive) below the
		 * expected number in two cases. The first is really easy. This is the case if the expected
		 * sequence number has not flown over (it's higher than a packet length). Check this case
		 * first. */
		long inSeq = in.getSequenceNumber();
		if(inSeq < receiveNextSeq && inSeq >= receiveNextSeq - TCPPacket.MAX_TCP_DATA) return true;
		/* If there has been overflow */
		if(receiveNextSeq < TCPPacket.MAX_TCP_DATA)
		{
			/* The point of one packet length behind the expected sequence number is: */
			long a = receiveNextSeq + TCPPacket.MAX_TCP_SEQUENCE + 1 - TCPPacket.MAX_TCP_DATA;
			/* If the in sequence number is between a and the max (inclusive), it's "in the zone". */
			if(inSeq >= a && inSeq <= TCPPacket.MAX_TCP_SEQUENCE) return true;
		}
		return false;
	}
	
	
	/**
	 * Updates the sequence numbers based on a received packet. This method only updates the 
	 * sequence numbers when receiving a packet we have not seen before.
	 * 
	 * @param in	A packet which is not invalid and not a retransmission
	 */
	private void processSequenceNumbers(TCPPacket in)
	{
		/* This is the next packet we expect to receive. Update sequence numbers now*/
		if (receiveNextSeq == -1)
		{
			/* This is the first packet we receive. Initialize ISN. */
			remoteISN = in.getSequenceNumber();		
			receiveNextSeq = incrSeqNum(remoteISN, 1);
		}
		else
		{
			/* Update the acknowledgment number to send. We hereby acknowledge this incoming
			 * packet */
			receiveNextSeq = incrSeqNum(receiveNextSeq, getPacketLength(in));
		}
		
		/* Update the sequence number to send, if the remote acknowledged our outbound packet */
		if (in.getFlags().hasAck())
		{
			sendNextSeq = in.getAcknowledgmentNumber();
			if(lastReceiveType != ReceiveType.RT_SIMULTANEOUS)
				bytesInTransit = 0;
		}
	}
	
	/**
	 * Increments a sequence number, properly wrapping around if it gets too big
	 * @param seqNum		The sequence number to increment
	 * @param toAdd			The number to add to the sequence number
	 * @return				The incremented, wrapped sequence number
	 */
	private static long incrSeqNum(long seqNum, long toAdd)
	{
		if(seqNum < 0 || seqNum > TCPPacket.MAX_TCP_SEQUENCE)
		{
			throw new IllegalArgumentException("Invalid sequence number: " + seqNum);
		}
		if(toAdd > TCPPacket.MAX_TCP_SEQUENCE)
		{
			throw new IllegalArgumentException("Illegal sequence number increment: " + toAdd);
		}
		if(toAdd < 0) return (seqNum + TCPPacket.MAX_TCP_SEQUENCE + 1 + toAdd) % (TCPPacket.MAX_TCP_SEQUENCE + 1);
		else return (seqNum + toAdd) % (TCPPacket.MAX_TCP_SEQUENCE + 1);
	}
	
	/**
	 * Returns the length of the packet, taking SYN and FIN flags into account.
	 * 
	 * Sequence and acknowledgment numbers need to be incremented with the number of bytes that
	 * is in a received or "to be sent" packet. a SYN or FIN flag also counts as one byte here. 
	 * 
	 * @param packet	The packet to send or receive.
	 * @return			The "length" number to add to the sequence/acknowledgment number.
	 */
	private static int getPacketLength(TCPPacket packet)
	{
		int packetLength = packet.getDataLength();
		if (packet.getFlags().hasSyn() || packet.getFlags().hasFin())
		{
			packetLength += 1;
		}
		return packetLength;
	}
	
	
	/**
	 * Directly sets the Initial sequence number of the socket. This should be called before
	 * connect or accept!
	 */
	protected void setISN(long isn)
	{
		this.applicationDefinedISN = isn;
	}	
	
}
