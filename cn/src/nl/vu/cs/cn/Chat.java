package nl.vu.cs.cn;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Chat extends Activity implements OnClickListener
{
	/** The log tag */
	private static final String TAG = "ChatApp";
	
	/** Used to specify a chat window */
	private enum ChatWindow
	{
		TOP,
		BOTTOM
	}
	
	/**
	 * Top is the server and bottom is the client!
	 */
	private final static int TOP_PORT 			= 1111;
	private final static int BOTTOM_PORT 		= 2222;
	private final int TOP_ADDRESS 				= 100;	
	private final int BOTTOM_ADDRESS 			= 101;
	private final IP.IpAddress TOP_IP_ADDRESS = IP.IpAddress.getAddress("192.168.0." + TOP_ADDRESS);	
	
	/** The length of a packet in this chat protocol */
	private final int PACKET_LENGTH = 512; 
	
	/** The two TCP stacks and sockets */
	private TCP topTCP;
	private TCP bottomTCP;	
	private TCP.Socket topSocket;
	private TCP.Socket bottomSocket;
	
	/** Indicates which chat instances have connected */
	private boolean topConnected = false;
	private boolean bottomConnected = false;
	
	/** Indicates that we want to close the threads!
	 * This one is volatile so we don't have to synchronize it explicitly */
	private volatile boolean mustClose = false;
	
	/** The two threads that read on behalf of both chat windows */
	private Thread topReadThread;
	private Thread bottomReadThread;
	
	/** The chat windows, send buttons and message edit boxes */
	private EditText topChat;
	private EditText bottomChat;
	private Button topSendButton;
	private Button bottomSendButton;
	private EditText topMessageEdit;
	private EditText bottomMessageEdit;
	
	/** The lock object to protect the UI from concurrent access */
	private Object lock = new Object();	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		/* Initialize GUI components */
		topChat 			= (EditText)findViewById(R.id.topChat);
		bottomChat 			= (EditText)findViewById(R.id.bottomChat);
		topSendButton 		= (Button)findViewById(R.id.topSendButton);
		bottomSendButton 	= (Button)findViewById(R.id.bottomSendButton);
		topMessageEdit 		= (EditText)findViewById(R.id.topMessageEdit);
		bottomMessageEdit 	= (EditText)findViewById(R.id.bottomMessageEdit);
		
		topChat.setMovementMethod(new ScrollingMovementMethod());
		bottomChat.setMovementMethod(new ScrollingMovementMethod());
		
		/* Initialize network stacks and sockets */
		try
		{
			mustClose = false;
			
			topTCP = new TCP(TOP_ADDRESS);
			bottomTCP = new TCP(BOTTOM_ADDRESS);
			topSocket = topTCP.socket(TOP_PORT);
			bottomSocket = bottomTCP.socket(BOTTOM_PORT);
			
			if(TCP.DEBUG) Log.d(TAG, "Starting read threads...");
			
			/* Start the two threads. These will execute accept() and connect() respectively */
			topReadThread = new Thread(new ReadThread(ChatWindow.TOP));
			topReadThread.start();
			bottomReadThread = new Thread(new ReadThread(ChatWindow.BOTTOM));
			bottomReadThread.start();
			
			if(TCP.DEBUG) Log.d(TAG, "Connecting...");
			
			/* Wait until we are sure that both ends connected.
			 * This is to ensure that any calls to sendMessageTo() will succeed. */
			synchronized (lock)
			{
				while (!topConnected || !bottomConnected)
				{
					lock.wait();
				}
			}			
			
			if(TCP.DEBUG) Log.d(TAG, "Connected!");
			
			/* Finally install onClick listeners, when connection is fully established */
			topSendButton.setOnClickListener(this);
			bottomSendButton.setOnClickListener(this);
		}
		catch (IOException ioe)
		{
			if(TCP.ERROR) Log.e(TAG, "IOException during setup!");
			ioe.printStackTrace();
		}		
		catch (InterruptedException ie)
		{
			if(TCP.ERROR) Log.e(TAG, "We were interrupted!!!");
			ie.printStackTrace();
		}
	}
	
	@Override
	public void onDestroy()
	{
		/* This lets both connection threads terminate */
		mustClose = true;
		
		try
		{
			topReadThread.join();
			bottomReadThread.join();
			if(TCP.DEBUG) Log.d(TAG,"Both threads have closed!");
		}
		catch (InterruptedException ie)
		{
			if(TCP.DEBUG) Log.d(TAG,"Interrupted while closing!");
		}
		
		super.onDestroy();
	}
	
	/**
	 * Called when one of the send buttons is clicked/touched
	 */
	public void onClick(View v)
	{
		if (v == topSendButton) 
		{
			/* Send message from top chat window to bottom chat window */
			String message = topMessageEdit.getText().toString();
			if(message.trim().length() == 0) sendMessage(ChatWindow.TOP, message + "\n");
			topMessageEdit.getText().clear();
		}
		else if (v == bottomSendButton)
		{
			/* Send message from bottom chat window to top chat window */
			String message = bottomMessageEdit.getText().toString();
			if(message.trim().length() != 0) sendMessage(ChatWindow.BOTTOM, message + "\n");
			bottomMessageEdit.getText().clear();
		}
	}
	
	/**
	 * Writes a message to the remote side!
	 * 
	 * @param sender		The one who sends the message
	 * @param message		The string that must be sent
	 */
	private void sendMessage(ChatWindow sender, CharSequence message)
	{
		if (mustClose)
		{
			if(TCP.ERROR) Log.e(TAG, "Write failed because we are closing...");
			return;
		}
		
		/* A new byte array is always fully zero'ed! */
		byte[] writeBuf = new byte[PACKET_LENGTH];
		
		try
		{
			/* Copy bytes of string into writeBuf */
			byte[] stringBuf = message.toString().getBytes("UTF-8");
			System.arraycopy(stringBuf, 0, writeBuf, 0, stringBuf.length);
			
			if (getSocketByChatWindow(sender).write(writeBuf, 0, PACKET_LENGTH) == -1)
				if(TCP.ERROR) Log.e(TAG, "Write failed!");			
		}
		catch (UnsupportedEncodingException uee)
		{
			if(TCP.ERROR) Log.e(TAG, "Unsupported encoding!");
		}
	}
	
	/**
	 * Returns the socket, given the window!
	 * @param window
	 * @return
	 */
	private TCP.Socket getSocketByChatWindow(ChatWindow window)
	{
		if (window == ChatWindow.TOP)	return topSocket;
		else							return bottomSocket;		
	}
	
	/**
	 * Returns the view of the chat window, given the ChatWindow enum
	 * @param window
	 * @return
	 */
	private View getViewByChatWindow(ChatWindow window)
	{
		if (window == ChatWindow.TOP)	return topChat;
		else							return bottomChat;		
	}
	
	/**
	 * Implements the connection phase, reading and the close phase of both chat windows. Writes are
	 * done from the main thread, but only after both read threads have fully executed the
	 * connect-phase.
	 */
	private class ReadThread implements Runnable
	{
		private ChatWindow receiver;
		
		public ReadThread(ChatWindow receiver)
		{
			this.receiver = receiver;
		}
		
		public void run()
		{
			/* First execute the connection-phase */
			if (receiver == ChatWindow.TOP)
			{
				topSocket.accept();
				
				/* Wake up main thread when top is connected */
				synchronized (lock)
				{
					topConnected = true;
					lock.notify();
				}
			}
			else
			{
				bottomSocket.connect(TOP_IP_ADDRESS, TOP_PORT);
				
				/* Wake up main thread when bottom is connected */
				synchronized (lock)
				{
					bottomConnected = true;
					lock.notify();
				}
			}
			
			byte[] readBuf = new byte[PACKET_LENGTH];
			int readResult = 0;
			int bytesRead = 0;
			
			/* Read from the network until we need to close */
			while (!mustClose)
			{
				bytesRead = 0;
				while (bytesRead < PACKET_LENGTH)
				{
					/* Read from the "right" socket here */
					readResult = getSocketByChatWindow(receiver).read(readBuf, bytesRead, PACKET_LENGTH - bytesRead);
					if (readResult == -1)
					{
						if(TCP.ERROR) Log.e(TAG, "ReadThread: read failed!");
						break;
					}
					else if (readResult == 0)
					{	
						if(TCP.WARN) Log.w(TAG, "ReadThread: we are closing! Current read interrupted.");
						break;
					}
					else
						bytesRead += readResult;
				}
				
				if (readResult > 0) processReceivedData(readBuf);
			}
			
			/* Close the connection */
			getSocketByChatWindow(receiver).close();
			if(TCP.DEBUG) Log.d(TAG, "ReadThread: closed!");
		}
		
		/**
		 * After receiving data, this method will extract the string and write it to the chat
		 * window!
		 * 
		 * @param buffer	The bytes we have received. Must have size equal to PACKET_LENGTH!
		 */
		private void processReceivedData(byte[] buffer)
		{
			/* Find first null character */
			int length = 0;
			for (length = 0; buffer[length] != '\0'; length++) ;
			
			try
			{
				/* Create a message and store it in a final variable as well as the receiver */ 
				final CharSequence message = new String(buffer, 0, length, "UTF-8");
				final ChatWindow receiver  = this.receiver;
				
				/* Dispatch a new runnable which will be executed in the main thread.
				 * This is necessary since android does not allow us to //TODO to what? Edit UI elements?
				 * Because we made our variables final, we can pass them into this code. */
				getViewByChatWindow(receiver).post( new Runnable()
				{
					public void run()
					{
						if (receiver == ChatWindow.TOP)	
							topChat.append(message);
						else							
							bottomChat.append(message);
					}
				});
			}
			catch (UnsupportedEncodingException uee)
			{
				if(TCP.ERROR) Log.e(TAG, "Unsupported encoding!");
			}	
		}
	}
}
