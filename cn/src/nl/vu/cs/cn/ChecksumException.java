package nl.vu.cs.cn;

/**
 * Thrown when the checksum check for an incoming IP packet used to create a TCP packet fails.
 */
public class ChecksumException extends Exception 
{
	private static final long serialVersionUID = 5660063326682751058L;

	
	public ChecksumException(String message)
	{
		super(message);
	}
}
