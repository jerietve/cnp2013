package nl.vu.cs.cn;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Locale;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.data.BufferHelper;
import nl.vu.cs.cn.data.Checksum;
import nl.vu.cs.cn.util.Util;

/**
 * An instance of this class represents one TCP packet. This class provides public getters and
 * setters which allow the safe reading and changing of fields of this class. This class also
 * provides some public constants related to TCP packets.
 * 
 * A TCP packet to be sent is created using the constructor, after which almost all setter methods
 * must be used one by one to fill it with the correct properties. Most of these are stored in the
 * TCP header, which is costructed when {@link #build()} is called. It may be worthwhile to read the
 * JavaDoc for that method.
 * 
 * A TCP packet for receiving is more easily created by simply providing an IP packet to the
 * {@link #receiveTCPPacket(nl.vu.cs.cn.IP.Packet)} method. This is another important method which
 * has its own JavaDoc to check out.
 * 
 * After using a TCP packet, in order to avoid accidentally forgetting to set any of the fields, it
 * can be reset using the {@link #reset(boolean)} method. Again, the JavaDoc for this method
 * provides more information on exactly what is does.
 */
public class TCPPacket 
{
	/**
	 * This enum is used to set the combination of flags for TCP packets. The only three flags
	 * supported by this implementation of TCP are SYN, FIN and ACK. PSH is always set, all other
	 * flags are always unset. This enum provides one name for all valid flag combinations, and an
	 * invalid state used in all other cases. Note that it is illegal to have actual 'FIN' packets,
	 * even though they are called this in literature and in the code as well. This is because the
	 * ACK flag must be set on all packets after the first SYN packet (and any retransmissions of
	 * it). See <a
	 * href="http://en.wikipedia.org/wiki/Transmission_Control_Protocol#TCP_segment_structure"
	 * >Wikipedia</a>, or <a href=
	 * "http://www.juniper.net/techpubs/software/junos-es/junos-es92/junos-es-swconfig-security/tcp-headers-with-fin-flag-and-without-ack-flag.html"
	 * >here</a> or <a href=
	 * "http://packetlife.net/blog/2010/jun/7/understanding-tcp-sequence-acknowledgment-numbers/"
	 * >here (Packet #38)</a>
	 */
	public enum FlagSet
	{
		F_INVALID(false,false,false),	/* Used as a default when we don't know what to do */
		F_SYN(true,false,false),
		F_SYNACK(true,false,true),
		//F_FIN(false,true,false), (packets with the FIN flag set and ACK flag unset shouldn't exist)
		F_FINACK(false,true,true),
		F_ACK(false,false,true);
		
		private boolean hasSyn;
		private boolean hasFin;
		private boolean hasAck;
		
		private FlagSet(boolean hasSyn, boolean hasFin, boolean hasAck)
		{
			this.hasSyn = hasSyn;
			this.hasFin = hasFin;
			this.hasAck = hasAck;
		}
		
		public boolean hasSyn()
		{
			return this.hasSyn;
		}
		
		public boolean hasFin()
		{
			return this.hasFin;
		}
		
		public boolean hasAck()
		{
			return this.hasAck;
		}
		
		/**
		 * Returns a flagset enum from the given booleans. When the combination is not valid,
		 * F_INVALID is returned.
		 * 
		 * @param hasSyn
		 * @param hasFin
		 * @param hasAck
		 * @return
		 */
		public static FlagSet fromBooleans(boolean hasSyn, boolean hasFin, boolean hasAck)
		{
			if (hasSyn  && !hasFin && !hasAck) return FlagSet.F_SYN;
			if (hasSyn  && !hasFin && hasAck)  return FlagSet.F_SYNACK;
			if (!hasSyn && hasFin  && hasAck)  return FlagSet.F_FINACK;
			if (!hasSyn && !hasFin && hasAck)  return FlagSet.F_ACK;
			return FlagSet.F_INVALID;
		}
	}
	
	/* Maximum amount of data that fits in an IP packet next to the IP and TCP headers */
	public static final int MAX_TCP_DATA = 8059;
	/* The number of bytes in a TCP header */
	public static final int TCP_HEADER_NO_OPTIONS_SIZE_BYTES = 20;
	/* The maximum port number */
	public static final int MAX_TCP_PORT_NR = 65535;
	/* The maximum value of the TCP packet sequence number */
	public static final long MAX_TCP_SEQUENCE = 4294967295L;
	/* The size in bytes of the IP pseudo header */
	private static final int IP_PSEUDO_HEADER_SIZE = 12;
	
	/* The location in the header of different items */
	private static final int HEADER_CHECKSUM_LOC = 16;
	/* The location in the flag byte of different flags */
	private static final int FLAG_BYTE_ACK_LOC = 0x10;
	private static final int FLAG_BYTE_PSH_LOC = 0x08;
	private static final int FLAG_BYTE_SYN_LOC = 0x02;
	private static final int FLAG_BYTE_FIN_LOC = 0x01;
		
	/* The locale we pass to String.format() */
	private static final Locale LOCALE = Locale.US;
	
	/* Destination port, 16 bits. Saved as int for lack of unsigned short. */
	private int destinationPort;
	/* Destination IP address */
	private IpAddress destinationIP;
	/* Local port, 16 bits. Saved as int for lack of unsigned short. */
	private int sourcePort;
	/* Source IP address */
	private IpAddress sourceIP;
	/* The sequence number for this TCP packet */
	private long sequenceNumber;
	/* Acknowledgment number */
	private long acknowledgmentNumber;
	/* The TCP flags we use as a set */
	private FlagSet flags;
	/* Misc TCP flags */
	private boolean pshFlag;
	/* Data that should encapsulated by the TCP packet (user data) */
	private byte[] tcpData;
	/* Length of tcpData that is actually used */
	private int tcpDataLength;
	/* Data that is encapsulated by the IP packet. This contains the complete TCP header + data 
	 * build() must be called to generate this data, from the rest of the fields of this object */
	private byte[] ipData;
	/* ByteBuffer on top of the "ipData" byte array containing the TCP header only */
	private ByteBuffer header;
	/* needsBuild indicates whether another call to build() is necessary generate ipData */
	private boolean needsBuild;

	
	/**
	 * Creates a new packet with all fields set to default values by reset(). This packet needs
	 * to be built before it can be used.
	 */
	public TCPPacket()
	{
		tcpData = new byte[MAX_TCP_DATA];
		flags = FlagSet.F_INVALID;
		reset(true);
	}
	
	
	/**
	 * Resets the entire packet, except for the local and remote port and IP address if* requested.
	 * After a reset, most individual fields should probably be initialized again by calling the
	 * various set***() methods of this class. Finally, the packet must be built before it can be
	 * sent. The default values set by this method are as follows:
	 * 
	 *	destinationPort				= -1;	(illegal value), or untouched
	 *	destinationIP				= null;	(illegal value), or untouched
	 *	sourcePort					= -1;	(illegal value), or untouched
	 *	sourceIP					= null;	(illegal value), or untouched
	 *	sequenceNumber				= -1L;	(illegal value)
	 *	acknowledgmentNumber		= -1L;	(illegal value)
	 *	synFlag 					= false;
	 *	ackFlag 					= false;
	 *	pshFlag 					= true;
	 *	finFlag 					= false;
	 *
	 * The TCP data will be ignored after a reset until it is set again, and the IP data and
	 * header will be null.
	 * 
	 * @param 
	 */
	public void reset(boolean resetSourceAndDest)
	{
		if(resetSourceAndDest)
		{
			destinationPort			= -1;
			destinationIP			= null;
			sourcePort				= -1;
			sourceIP				= null;
		}
		sequenceNumber				= -1L;
		acknowledgmentNumber		= -1L;
		tcpDataLength				= 0;
		flags						= FlagSet.F_INVALID;
		pshFlag 					= true;		
		needsBuild 					= true;
		ipData						= null;
		header						= null;
	}
	
	/**
	 * Rebuilds the packet from the data contained by this packet object. This must be done before
	 * the packet can be sent. A call to this method will throw a RuntimeException if any of the
	 * following fields has not been explicitly set since construction or the last reset:
	 * 
	 * sourcePort
	 * destinationPort
	 * sourceIP
	 * destinationIP
	 * sequenceNumber
	 * acknowledgmentNumber
	 */		
	public void build() 
	{
		if (!this.needsBuild) return;
		
		if (this.sourcePort == -1 ||
			this.destinationPort == -1 ||
			this.sourceIP == null ||
			this.destinationIP == null ||
			this.flags == FlagSet.F_INVALID ||
			this.sequenceNumber < 0 ||
			this.flags.hasAck() && this.acknowledgmentNumber < 0) 
		{
			/* Uncomment this line in case build() starts throwing exceptions */
//			System.err.printf("Packet incomplete:\n%s\n", this);
			throw new RuntimeException("build() failed because packet is incomplete!");
		}
		
		/* Create a new ipData and header */
		ipData = new byte[TCP_HEADER_NO_OPTIONS_SIZE_BYTES + tcpDataLength];
		header = ByteBuffer.wrap(ipData, 0, TCP_HEADER_NO_OPTIONS_SIZE_BYTES);
		
		/* Fill the TCP header, effectively filling ipData, then copy the data */
		constructTCPHeader();
		System.arraycopy(tcpData, 0, ipData, TCP_HEADER_NO_OPTIONS_SIZE_BYTES, tcpDataLength);		
		
		this.needsBuild = false;
	}
	
	/**
	 * Called by testing code to corrupt this packet. It will change the given bit of the given byte
	 * in the ipData.
	 * 
	 * @param	byteIndex	The index of the byte in ipData, from 0 for the high-order byte of the
	 * 						source port	to ipData.length - 1 for the last data byte
	 * @param	biteIndex	The bit within that byte: 0 for the highest bit to 7 for the lowest
	 */
	protected void corrupt(int byteIndex, int bitIndex)
	{
		/* Find the current value of the bit (either 0 or 1) */
		int bitValue = ipData[byteIndex] >> (7 - bitIndex) & 0x1;
		/* Invert it */
		bitValue = bitValue == 1 ? 0 : 1;
		/* Put it back */
		ipData[byteIndex] = (byte)(ipData[byteIndex] & bitValue << (7 - bitIndex));
	}
	
	/**
	 * Sets the source port and IP. In case of invalid input, the packet is not changed.
	 * 
	 * @param sourceIp		our IP address (must not be null)
	 * @param sourcePort	the port we send from [0..MAX_TCP_PORT_NR]
	 */
	public void setSource(IpAddress sourceIp, int sourcePort)
	{
		if(sourcePort < 0 || sourcePort > MAX_TCP_PORT_NR)
		{
			throw new IllegalArgumentException("Invalid port number");
		}
		if(sourceIp == null)
		{
			throw new IllegalArgumentException("Source IP must not be null");
		}
		this.sourceIP   = sourceIp;
		this.sourcePort = sourcePort;
		this.needsBuild = true;
	}
	
	/**
	 * Sets the destination for this packet. In case of invalid input, the packet is not
	 * changed.
	 * 
	 * @param destinationIp		The IP address of the destination host (must not be null)
	 * @param destinationPort	The port at the destination host [0..MAX_TCP_PORT_NR]
	 */
	public void setDestination(IpAddress destinationIp, int destinationPort)
	{
		if(destinationPort < 0 || destinationPort > MAX_TCP_PORT_NR)
		{
			throw new IllegalArgumentException("Invalid port number");
		}
		if(destinationIp == null)
		{
			throw new IllegalArgumentException("Source IP must not be null");
		}
		this.destinationIP = destinationIp;
		this.destinationPort = destinationPort;
		this.needsBuild = true;
	}
	
	/**
	 * Sets the sequence number for this packet. In case of invalid input, the packet is not
	 * changed.
	 *  
	 * @param seqNumber [0..MAX_TCP_SEQUENCE]
	 */
	public void setSequenceNumber(long seqNumber)
	{
		if(seqNumber < 0 || seqNumber > MAX_TCP_SEQUENCE)
				throw new IllegalArgumentException("Invalid sequence number: " + seqNumber);
		else
		{
			this.sequenceNumber = seqNumber;
			this.needsBuild = true;
		}
	}
	
	/**
	 * Sets the acknowledgment number for this packet. In case of invalid input, the packet is
	 * not changed.
	 * 
	 * @param ackNumber [0..MAX_TCP_SEQUENCE]
	 */
	public void setAcknowledgmentNumber(long ackNumber)
	{
		if (ackNumber < 0 || ackNumber > MAX_TCP_SEQUENCE) 
				throw new IllegalArgumentException("Invalid acknowledgment number: " + ackNumber);
		else
		{
			this.acknowledgmentNumber = ackNumber;
			this.needsBuild = true;
		}
	}
	
	/**
	 * Sets the data that should be sent by this packet. An IllegalArgumentException is thrown
	 * if the amount of data exceeds MAX_TCP_DATA. In this case the packet is not changed. The
	 * data is copied from the provided array.
	 * 
	 * @pre	offset + length <= data.length
	 * @pre offset >= 0
	 * @pre length >= 0 
	 * @param data			An array of bytes containing the data to send
	 * @param offset		An offset in "data" indicating the first byte to send
	 * @param length		The number of bytes to copy from "data"
	 */
	public void setData(byte[] data, int offset, int length)
	{
		if(length < 0 || length > MAX_TCP_DATA) 
		{
			throw new IllegalArgumentException("Length of TCP data must be in range [0.." + MAX_TCP_DATA + "] bytes");
		}
		if(data == null) return;
		
		tcpDataLength = length;
		System.arraycopy(data, offset, tcpData, 0, length);
		this.needsBuild = true;
	}
	
	/**
	 * Sets the packet data using a PacketData object.
	 * @param packet
	 */
	public void setData(PacketData packet)
	{
		if(packet == null) return;
		tcpDataLength = packet.getLength();
		System.arraycopy(packet.getData(), 0, tcpData, 0, tcpDataLength);
		this.needsBuild = true;
	}
	
	/**
	 * Sets the flags of this packet. 
	 * @param flags		The flagset we want to set
	 */
	public void setFlags(FlagSet flags) 
	{
		if (flags == FlagSet.F_INVALID)
			throw new IllegalArgumentException("Setting \"no flags\" is not allowed!");
		
		this.flags = flags;
		this.needsBuild = true;
	}	

	/**
	 * Writes the TCP header to "header" which is a ByteBuffer on top of "ipData".
	 */
	private void constructTCPHeader()
	{			
		/* Set source and destination ports (takes 2 least-significant bytes) */
		header.putShort((short)sourcePort);
		header.putShort((short)destinationPort);
		
		/* Now add the sequence number and acknowledgment number */
		header.putInt((int)sequenceNumber);
		if(flags.hasAck()) header.putInt((int)acknowledgmentNumber);
		else header.putInt(0);
		
		/* Set header length, expressed in words (32bits). This is shifted 4 bits to make room for
		 * the reserved bits and the NS bit. We always set the NS bit to zero. */
		header.put((byte)(TCP_HEADER_NO_OPTIONS_SIZE_BYTES / 4 << 4));
		
		/* The next byte contains, in this order, the flags CWR, ECE, URG, ACK, PSH, RST, SYN and
		 * FIN. The CWR, ECE, URG and RST flags are always unset in this implementation. PSH is set
		 * on all outgoing packets. */
		int flagbits = 0;
		flagbits |= ((flags.hasAck()) ? 1 : 0) << 4;
		flagbits |= ((pshFlag) ? 1 : 0) << 3;
		flagbits |= ((flags.hasSyn()) ? 1 : 0) << 1;
		flagbits |= ((flags.hasFin()) ? 1 : 0);
		header.put((byte)flagbits);
		
		/* Next are 16 bits containing the window size, which is always one. */
		header.putShort((short)1);
	
		/* Compute the checksum, includes generation of pseudo-header */
		short checksum = generateChecksum(sourceIP.getAddress(), destinationIP.getAddress(), header, tcpData,
				tcpDataLength);
		
		/* Write the checksum */
		header.putShort(checksum);
		
		/* Finally the header concludes with 16 bits of urgent pointer, which are just zero */
		header.putShort((short)0);
	}
	
	
	/** 
	 * Parses the data of the given IP Packet to initialize all fields of this TCP packet. The
	 * checksum check is also performed. Note that no copy of the entire IP packet is made into our
	 * private "ipData" array, since this field is not useful for received packets.
	 * 
	 * @param	packet		The IP packet received from the IP stack.
	 */
	public void receiveTCPPacket(IP.Packet packet) throws IOException, IllegalTCPFlagsException, ChecksumException 
	{
		if (packet.length < TCP_HEADER_NO_OPTIONS_SIZE_BYTES) 
			throw new IOException("Invalid TCP header received");
		
		/* We now read everything from packet.data, through this ByteBuffer instance. However, we do
		 * not copy this data into ipData, since we just don't need to. Therefore, needBuild must be
		 * true, because ipData is not initialized. */
		header = ByteBuffer.wrap(packet.data, 0, TCP_HEADER_NO_OPTIONS_SIZE_BYTES);
		ipData = null;
		needsBuild = true;
		
		/* Set source and destination IP addresses */
		sourceIP 		= IpAddress.getAddress(packet.source);			
		destinationIP 	= IpAddress.getAddress(packet.destination);	
		
		/* Read source- and destination ports and sequence numbers */
		sourcePort 				= BufferHelper.getUnsignedShort(header);
		destinationPort 		= BufferHelper.getUnsignedShort(header);
		sequenceNumber			= BufferHelper.getUnsignedInteger(header);
		acknowledgmentNumber 	= BufferHelper.getUnsignedInteger(header);
		
		/* Parse header length and flags */
		int lengthAndFlags	= BufferHelper.getUnsignedShort(header);
		int headerLength 	=  (lengthAndFlags & 0xF000) >>> 12;
		pshFlag         	= ((lengthAndFlags & FLAG_BYTE_PSH_LOC) != 0);
		boolean ackFlag 	= ((lengthAndFlags & FLAG_BYTE_ACK_LOC) != 0);
		boolean synFlag 	= ((lengthAndFlags & FLAG_BYTE_SYN_LOC) != 0);
		boolean finFlag 	= ((lengthAndFlags & FLAG_BYTE_FIN_LOC) != 0);
		flags 				= FlagSet.fromBooleans(synFlag, finFlag, ackFlag);
		if (flags == FlagSet.F_INVALID)
			throw new IllegalTCPFlagsException();
		
		if (headerLength != TCP_HEADER_NO_OPTIONS_SIZE_BYTES / 4)
			throw new IOException("Unexpected TCP header size: " + headerLength);
		
		/* Discard window size */
		BufferHelper.getUnsignedShort(header);
		
		/* Copy the data from the IP packet to this packet */
		System.arraycopy(packet.data, TCP_HEADER_NO_OPTIONS_SIZE_BYTES, tcpData, 0, 
						 packet.length - TCP_HEADER_NO_OPTIONS_SIZE_BYTES);	
		tcpDataLength = packet.length - TCP_HEADER_NO_OPTIONS_SIZE_BYTES;			
		
		/* Get checksum and then erase it from the original byte array. This is needed since we will
		 * compute the checksum from this data and in order to do that correctly, the checksum part
		 * should be zero'ed out. */
		short checksum 						= header.getShort();
		packet.data[HEADER_CHECKSUM_LOC] 	= 0;
		packet.data[HEADER_CHECKSUM_LOC+1] 	= 0;
		
		/* Now compute the checksum on the original data */
		ByteBuffer headerUpToChecksum = ByteBuffer.wrap(packet.data, 0, TCP_HEADER_NO_OPTIONS_SIZE_BYTES);
		short generatedChecksum = generateChecksum(sourceIP.getAddress(), destinationIP.getAddress(),
				headerUpToChecksum, tcpData, tcpDataLength);
		
		/* Now compare our "saved" checksum with the computed one. */
		if (generatedChecksum != checksum) 
				throw new ChecksumException("Is " + Util.toBinaryString(checksum) + " but should be " +
						Util.toBinaryString(generatedChecksum));
	
		/* Put back checksum */
		header.putShort(HEADER_CHECKSUM_LOC, checksum);
	}
	
	
	/**
	 * Computes the TCP checksum from the given source/dest addresses, tcp-header and data.
	 * 
	 * @param	sourceAddress	The source IP address, required for pseudo-header construction
	 * @param	destAddress		The dest IP address, required for pseudo-header construction
	 * @param	tcpHeader		A bytebuffer of the TCP header. This bytebuffer is duplicated and
	 * 							rewound before copying it's data
	 * @param	data			The data that must be sent (user data)
	 * @param	length			The number of bytes to copy from "data".
	 */
	public short generateChecksum(int sourceAddress, int destAddress, ByteBuffer tcpHeader, byte[] data, int length)
	{
		/* Create a byte-buffer backed by a byte-array and write the pseudo header */
		ByteBuffer ipPseudoHeader = ByteBuffer.allocateDirect(IP_PSEUDO_HEADER_SIZE);
		
		/* First 32 bits containing the source address, big-endian. Everything in the header is
		 * big-endian */
		ipPseudoHeader.putInt(Util.switchEndian(sourceAddress));
		/* Then the destination address */
		ipPseudoHeader.putInt(Util.switchEndian(destAddress));
		/* Then a byte of zeros and a byte containing the TCP protocol number (6). Or, equivalently,
		 * a short of value 6. */
		ipPseudoHeader.putShort(TCP.PROTOCOL);
		/* Finally, the length of the TCP packet excluding the pseudo header (which doesn't really
		 * exist anyway). */
		ipPseudoHeader.putShort((short)(length+TCP_HEADER_NO_OPTIONS_SIZE_BYTES));								

		/* Wrap the data with a byte-buffer */
		ByteBuffer dataBuffer = ByteBuffer.wrap(data, 0, length);
		
		/* Compute and return the checksum */
		return Checksum.calculate(ipPseudoHeader, tcpHeader, dataBuffer);
	}
	
	
	/**
	 * @return The source port, or -1 if the packet is not initialized.
	 */
	public int getSourcePort() 
	{
		return this.sourcePort;
	}
	
	
	/**
	 * @return The destination port, or -1 if the packet is not initialized.
	 */
	public int getDestinationPort() 
	{
		return this.destinationPort;
	}
	
	/**
	 * @return The source IP address of this packet
	 */
	public IpAddress getSourceIP() 
	{
		return this.sourceIP;
	}		
	
	/**
	 * @return The destination IP address, or null if the packet is not initialized.
	 */
	public IpAddress getDestinationIP() 
	{
		return this.destinationIP;
	}
	
	
	/**
	 * @return The sequence number, or -1 if the packet is not initialized.
	 */
	public long getSequenceNumber() 
	{
		return this.sequenceNumber;
	}
	
	
	/**
	 * @return The acknowledgment number, or -1 if the packet is not initialized.
	 */
	public long getAcknowledgmentNumber() 
	{
		return this.acknowledgmentNumber;
	}
	
	/**
	 * Gets the current set of flags of this packet
	 * @return
	 */
	public FlagSet getFlags()
	{
		return this.flags;
	}
	
	/**
	 * Returns whether the PSH flag is set for this packet
	 * @return
	 */
	public boolean hasPsh()
	{
		return this.pshFlag;
	}
	
	/**
	 * Tells us whether this packet will transmit application defined data to the remote.
	 * @return	True, when the packet has data, false otherwise.
	 */
	public boolean hasData()
	{
		return (tcpDataLength > 0);
	}
	
	/**
	 * Returns the application defined data. Any data in the returned array at or beyond index
	 * getDataLength() is garbage. Use this to spy on things sent across this socket earlier.
	 * 
	 * WARNING: You may not write into the bytearray that is returned. Always use setData().
	 * 
	 * @return	
	 */
	public byte[] getData()
	{
		return tcpData;
	}
	
	/**
	 * Returns the number of bytes of application defined data we are about to transmit.
	 * @return	The size of the application defined data of this packet, in bytes.
	 */
	public int getDataLength()
	{
		return tcpDataLength;
	}
	
	/**
	 * Get the array containing exactly the data representing this TCP packet to be put into an
	 * IP packet. Will be null until a packet is initialized.
	 * 
	 * @return This TCP packet's IP data.
	 */
	public byte[] getIPData()
	{
		return this.ipData;
	}
	
	
	/**
	 * Returns the size of the TCP header + data, or -1 if the packet is not initialized.
	 */
	public int getIPDataLength() 
	{
		if (needsBuild) 
			return -1;
		return this.ipData.length;
	}		
	
	
	/**
	 * @return The checksum in the header of this packet. Will return 0 if the packet is not
	 * initialized.
	 */
	public short getChecksum()
	{
		if (this.needsBuild) 
			return 0;
		return header.getShort(HEADER_CHECKSUM_LOC);
	}
	
	/**
	 * @return true iff the data in this packet is in inconsistent state and must be rebuild.
	 */
	public boolean needsBuild()
	{
		return needsBuild;
	}
	
	/**
	 * Writes the packet fields to a string. Usefull for debugging.
	 * 
	 * @return 		A string containing the fields of this object.
	 */
	@Override
	public String toString()
	{
		return String.format(LOCALE,
				"Source/Destination: %s-%d, %s-%d\n"+
				"Sequence/Acknowledgment Numbers: %d %d\n"+
				"Flags: %s  Data length: %d",
				sourceIP.toString(), sourcePort, destinationIP.toString(), destinationPort,
				sequenceNumber, acknowledgmentNumber,
				flags.name(),
				tcpDataLength);	
	}		
}
