package nl.vu.cs.cn;

/**
 * Is thrown by SocketThread.read() to tell the caller that no more data will be available for
 * reading.
 */
public class NoMoreDataException extends Exception 
{
	private static final long serialVersionUID = 1L;

	public NoMoreDataException()
	{
		super();
	}
}
