package nl.vu.cs.cn;

/**
 * Thrown when the flag combination for an incoming IP packet used to create a TCP packet is one
 * which is not supported by this TCP implementation.
 */
public class IllegalTCPFlagsException extends Exception 
{
	private static final long serialVersionUID = 1L;

	public IllegalTCPFlagsException()
	{
		super();
	}
}
