package nl.vu.cs.cn;

/**
 * Simple container for packet data. The internal array always has a size of TCPPacket.MAX_TCP_DATA
 * and the actual length is given by "length", which allows PacketData objects to be reused for
 * packets with different lengths.
 */
public class PacketData 
{
	private byte[] data;
	private int length;
	
	/**
	 * Creates an empty data packet
	 */
	public PacketData()
	{
		data = new byte[TCPPacket.MAX_TCP_DATA];
		length = 0;
	}
	
	/**
	 * Extracts the data from the given packet and stores it into this PacketData object.
	 */
	public void fromPacket(TCPPacket packet)
	{
		this.length = packet.getDataLength();
		System.arraycopy(packet.getData(), 0, data, 0, length);
	}
	
	/**
	 * Load data into the buffer
	 * 
	 * @param buf		The source buffer we copy data from
	 * @param offset	Offset in the source buffer, indicating where we start to copy bytes
	 * @param length	The number of bytes to copy
	 */
	public void fromBuffer(byte[] buf, int offset, int length)
	{
		this.length = length;
		System.arraycopy(buf, offset, this.data, 0, this.length);
	}
	
	/**
	 * Returns the data of the packet.
	 * @return
	 */
	public byte[] getData()
	{
		return this.data;
	}
	
	/**
	 * Returns the number of bytes that are actually used.
	 * @return
	 */
	public int getLength()
	{
		return this.length;
	}
	
	/**
	 * Returns whether the packet is empty.
	 * 
	 * @return	True when empty, false otherwise
	 */
	public boolean isEmpty()
	{
		return (this.length == 0);
	}
	
	/**
	 * Marks the packetData instance as empty
	 */
	public void empty()
	{
		this.length = 0;
	}
}
