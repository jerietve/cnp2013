package nl.vu.cs.cn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import junit.framework.AssertionFailedError;

import android.util.Log;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;
import nl.vu.cs.cn.TCPPacket.FlagSet;
import nl.vu.cs.cn.TCPState.ConnectionState;
import nl.vu.cs.cn.TCPState.ReceiveType;
import nl.vu.cs.cn.util.Interceptor;
import nl.vu.cs.cn.util.Timeout;

/**
 * The SocketThread is really the core of the TCP implementation. It takes care of all the hard
 * work. {@link TCP.Socket} calls the constructor, which creates a new Thread, which lives inside
 * the SocketThread instance as a separate instance. Socket then forwards (after doing its own
 * accounting) calls to its methods to methods of this class of the same or similar name.
 * SocketThread works with single packets (see below). After connect() or accept() establishes a
 * connection, the thread is started (SocketThread takes care of this by itself). It will run, and
 * check local private fields which tell it whether there is something to write, and whether it
 * should be checking for incoming packets. Once close() is called, the connection is nicely closed,
 * either by doing an active close or by finishing a passive close initiated by the remote host, and
 * afterward the thread is stopped. The same SocketThread instance can then be reused to set up a
 * new connection, possibly with a different remote host.
 * 
 * Socket uses some public methods provided by this class to check whether or not it makes sense to
 * ask SocketThread to read, write, accept, connect or close.
 * 
 * Socket reuses PacketData objects to store the data receives and has to send. Whenever Socket
 * wants to write a packet, it first asks SocketThread to provide it with such a container to put
 * its data inside of, before giving this back to be sent. SocketThread will try to reuse an old
 * one, and create a new one only if there are no old ones available. When reading, SocketThread
 * provides the data of incoming packets in PacketData objects to Socket. These PacketData objects
 * survive a connection reset. They are not used for packets not containing data, such as FIN and
 * SYN packets, or ACKs without data.
 * 
 * SocketThread tries to use the connection and the CPU efficiently. Once a connection is
 * established, it spends its time in a loop, checking the fields which tell it whether it has
 * something to do. Whenever it has nothing to do, it yields to other threads. If a packet is
 * expected (because acceppt() is waiting, read() was called or a reply to a sent packet is being
 * awaited) it asks the IP layer to wait for this packet the minimum time of 1 second. During this
 * time, until a packet arrives, no write operations can take place. Calls to the SocketThread
 * instance (not the internal thread) will still be handled immediately of course. Whenever a packet
 * that was received needs to be ACKed, while there is also data that needs to be sent, these are
 * combined in one packet. This only happens in the ESTABLISHED and CLOSE_WAIT states, meaning
 * FINACK packets never contain data. SYN and SYNACK packets also never contain data, because
 * write() can't have been called yet when still connecting or accepting. FIN packets never contain
 * data because these packets are sent outside the main read/write loop. As soon as close() is
 * called, the next iteration of this loop is not entered, any read() and write() calls are aborted
 * and only then an empty FIN packet it sent.
 */
public class SocketThread implements Runnable 
{
	private enum WriteStatus
	{
		/** We are waiting for the application to give us something to write */
		WS_IDLE,
		/** The application has called writePacket() so there is data pending to be written. This
		 * data is in sendBuffer */
		WS_PENDING,
		/** The socketThread has written the data to the network and is waiting for an
		 * acknowledgment or a timeout. When we receive an acknowledgment in this state, we go to
		 * WS_SUCCESS. When we receive a timeout in this state, and we have attempts left we go to
		 * WS_PENDING. If we have no more attempts left, we go to WS_FAILED */
		WS_WAITING,
		/** The write was successful but the application must still be informed. When it is
		 * informed we go back to WS_IDLE. */
		WS_SUCCES,
		/** The write has failed but the application must still be informed. When it is informed we
		 * go back to WS_IDLE. */
		WS_FAILED
	}
	
	private enum ReadStatus
	{
		/** There is no reader so we don't have to do anything */
		RS_IDLE,
		/** We have a waiting reader */
		RS_WAITING,
		/** We have received data, so the reader can give it back to the application. This is equal
		 * to receivedBuffers.isEmpty() being false, while the reader was waiting. */
		RS_SUCCES,
		/** An error occurred while the reader was waiting, so pass the error to the
		 * application */
		RS_FAILED
	}
	
	
	/* ===== CONSTANTS ===== */
	
	/* Log base tag */
	private static final String BASE_TAG = "CNP_TCP";
	/* Minimum time we have to spend in ip_receive */
	private static final int MIN_IP_TIMEOUT = 1;
	
	/* ===== CONNECTION RELATED VARIABLES ===== */
	
	/* The current TCP state of the connection (including current seq/ack numbers) */
	private TCPState state;	
	/* Destination IP address in little-endian form, 32 bits */
	private IpAddress remoteIP;
	/* Destination port, 16 bits. Saved as int for lack of unsigned short. */
	private int remotePort;
	/* Local port, 16 bits. Saved as int for lack of unsigned short. */
	private int localPort;		
	/* This timeout is started when a connection is closed, and before starting a new one we need to
	 * wait for it to time out */
	private Timeout restartTimeout;
	
	/* ===== READ / WRITE RELATED VARIABLES ===== */
	
	/** Data we must write to the remote */
	private PacketData sendBuffer;
	/** The current write status */
	private WriteStatus writeStatus;
	/** Timeout that keeps track of the time since the last written packet */
	private Timeout writeTimeout;
	/** Are there more packets to write coming immediately after this one? */
	private boolean moreWritesComing;
	
	/** Data we have received from the remote */
	private LinkedList<PacketData> receivedBuffers;	
	/** Contains reusable data buffers to keep the GC happy */
	private LinkedList<PacketData> unusedBuffers;
	/** Contains the state of the current read operation */
	private ReadStatus readStatus;	
	/** A saved IOException that can be re-thrown in the calling thread of the application */
	private IOException ioException;
	
	/* ===== MISCELANEOUS ===== */
	
	/* Lock for internal synchronizing */
	private final Object lock = new Object();
	/* Another lock for multiple write packet placement */
	private final Object moreWritePacketsLock = new Object();
	/* Enclosing TCP instance */
	private TCP tcp;
	/* The thread that executes all the network I/O */
	private Thread socketThread;
	/* TCP Packet objects used for sending and receiving */
	private TCPPacket toSend, received;
	/* IP Packet objects used for sending and receiving */
	private IP.Packet sendIPPacket, receiveIPPacket;
	/* The timeout used in doReadingAndWriting() */
	private Timeout receiveTimeout;
	/* When true, we must send an acknowledgment when executing doNetworkIO() */
	private boolean sendAckOnNextIteration;
	/* The number of transmissions we have left, until we give up */
	private int transmissionsLeft;
	/* close() is called, so we must stop reading from/writing to the network */
	private boolean closeCalled;
	/* An interceptor is an optional component that intercepts, manipulates or drops inbound and
	 * outbound packets. It is used by tests to determine how the implementation responds to
	 * corruption and packet loss */
	private ArrayList<Interceptor> interceptors;
	
	
	public SocketThread (TCP tcp, int localPort)
	{
		this.tcp 				= tcp;
		this.localPort			= localPort;
		this.state				= new TCPState(sockTag());
		this.sendIPPacket		= new IP.Packet();
		this.receiveIPPacket 	= new IP.Packet();
		this.toSend 			= new TCPPacket();
		this.received 			= new TCPPacket();	
		this.writeTimeout		= new Timeout();
		this.restartTimeout		= new Timeout();
		this.receiveTimeout		= new Timeout();
		this.sendBuffer 		= null;
		this.receivedBuffers 	= new LinkedList<PacketData>();
		this.unusedBuffers 		= new LinkedList<PacketData>();
		this.socketThread 		= new Thread(this);
		this.interceptors		= new ArrayList<Interceptor>();
		
		reset();
	}
	
	/* ============================ APPLICATION CALLS =========================================== */
	
	/**
	 * Attempts to establish a connection with the remote.
	 * 
	 * @param dst	The IPaddress of the destination
	 * @param port	The port we want to connect to
	 * @return 		True, when the connection was successfully established (The SocketThread was
	 * 				started)
	 * 				False, when an error occurred or all SYNs/SYNACKs were lost.
	 */
	public boolean connect(IpAddress dst, int port)
	{
		if(TCP.DEBUG) Log.d(sockTag(), "Connect called in state " + state.getState());
		if(!isConnectionSetupAllowed())
		{
			if(TCP.ERROR) Log.e(sockTag(), "connect() failed because we are already connected or closing!");
			return false;
		}	
		
		/* In case we're restarting, pretend to wait a while for the imaginary packets in the 
		 * imaginary network to 'disappear'. */
		leaveTimeWaitPeriod();
		
		/* We attempt to set up a new connection so all data of the previous connection must be deleted */
		clearReceiveBuffers();
		
		remoteIP = dst;
		remotePort = port;
		
		/* Connecting means sending a SYN packet to the remote host, waiting for a SYNACK packet
		 * in return, and ACKing that. Of course, that's the ideal situation. */			
		try
		{
			// First time completely reset the send packet
			toSend.reset(true);
			toSend.setDestination(remoteIP, remotePort);
			toSend.setSource(tcp.getLocalAddress(), localPort);
			
			/* Initialize ISN */
			state.reset();				
			
			int attemptsLeft = TCP.Socket.MAX_TRANSMISSIONS;
			while(attemptsLeft > 0)
			{
				attemptsLeft--;
				
				/* Set the SYN flag and send the packet */
				if(TCP.DEBUG) Log.d(sockTag(), "Connect: Sending SYN packet "+attemptsLeft);
				toSend.setFlags(FlagSet.F_SYN);
				state.processOutbound(toSend);
				sendTCPPacket(toSend);
				
				state.setState(ConnectionState.S_SYN_SENT);
				
				/* Now wait for an acknowledgment (SYNACK packet) */
				Timeout timeout = new Timeout();
				timeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
					
				/* Loop until we either get a timeout or a SYNACK*/
				if(TCP.DEBUG) Log.d(sockTag(), "Connect: Waiting for SYNACK packet");
				/* When true, we received something. When false, timeout! */
				boolean hasReceivedPacket = false;
				do
				{
					hasReceivedPacket = receiveTCPPacket(received, remoteIP, timeout);
				}
				while(hasReceivedPacket && !state.processInbound(received));
					
				if (hasReceivedPacket)
				{
					/* We have received a valid SYNACK so we are in ESTABLISHED */
					if(TCP.DEBUG) Log.d(sockTag(), "Connect: SYNACK packet received");					

					/* Reply with an ACK and assume it to arrive (!) */
					if(TCP.DEBUG) Log.d(sockTag(), "Connect: Sending ACK packet");
					ack();					
					
					/* Start socket thread */
					socketThread.start();	
					
					return true;						
				}
			}
		}
		catch(IOException ioe)
		{
			/* Log exception, reset the connection and return false. */
			if(TCP.ERROR) Log.e(sockTag(),"fatal IOException in connect()\n");
		}
		
		/* IOException or timeout */
		this.reset();
		return false;
	}
	
	/**
	 * Waits for an incoming connection.
	 */
	public void accept()
	{
		if(TCP.DEBUG) Log.d(sockTag(), "Accept called in state " + state.getState());
		if(!isConnectionSetupAllowed())
		{
			if(TCP.ERROR) Log.e(sockTag(), "accept() failed because we are already connected or closing!");
			return;
		}
		
		if(TCP.DEBUG) Log.d(sockTag(), "Accept: starting to accept new connections...");
		
		/* In case we're restarting, pretend to wait a while for the imaginary packets in the 
		 * imaginary network to 'disappear'. */
		leaveTimeWaitPeriod();
		
		/* We attempt to set up a new connection so all data of the previous connection must be
		 * deleted */
		clearReceiveBuffers();
		
		/* Repeat procedure until somebody interrupts us. When we are connected, return will be used
		 * to break out of this loop */
		while (!Thread.interrupted())
		{
			try
			{
				/* Reset the seq/ack numbers and go to the listen state */
				//TODO fix this next line after fixing that setstate function
				state.setState(ConnectionState.S_CLOSED);
				state.reset();
				state.setState(ConnectionState.S_LISTEN);
				remoteIP	= null;
				remotePort  = -1;
				
				/* Prepare our send packet */
				toSend.reset(true);
				toSend.setSource(tcp.getLocalAddress(), localPort);
				
				/* Wait indefinitely for SYN packet (from any source) */
				do
				{
					if(TCP.DEBUG) Log.d(sockTag(), "Accept: waiting for SYN packet");
					receiveTCPPacket(received, null, null);
					if(TCP.DEBUG) Log.d(sockTag(), String.format("Accept: received packet:\n%s\n", received));
				} 
				while (!state.processInbound(received));
				if(TCP.DEBUG) Log.d(sockTag(), "Accept: SYN packet received");
				
				/* Remote has been identified... */
				remoteIP	= received.getSourceIP();
				remotePort  = received.getSourcePort();
				toSend.setDestination(remoteIP, remotePort);
				
				/* Send SYNACK and wait for ACK. Repeat up to MAX_TRANSMISSIONS times */
				int attemptsLeft = TCP.Socket.MAX_TRANSMISSIONS;
				Timeout timeout = new Timeout();					
				while (attemptsLeft > 0)
				{
					/* (Re)send SYNACK */
					if(TCP.DEBUG) Log.d(sockTag(), "Accept: sending SYNACK " + (TCP.Socket.MAX_TRANSMISSIONS + 1 - attemptsLeft));
					
					toSend.setFlags(FlagSet.F_SYNACK);
					state.processOutbound(toSend);
					sendTCPPacket(toSend);
					attemptsLeft--;
					
					/* Wait for ACK from remote. */
					timeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
					
					/* Loop until we either have a timeout or an ACK from the remote */
					if(TCP.DEBUG) Log.d(sockTag(), "Accept: waiting for ACK packet");
					/* When true, we received something. When false, timeout! */
					boolean hasReceivedPacket = false;
					hasReceivedPacket = receiveTCPPacket(received, remoteIP, timeout);
					/* Why is there no loop? We always have a loop.. See huge comment below: */
					if(hasReceivedPacket)
					{
						if(TCP.DEBUG) Log.d(sockTag(), String.format("Accept: received packet:\n%s\n", received));						
						/* This is a little tricky. In the likely situation where our SYNACK was
						 * dropped, and a new SYN packet came in, it's almost time to resend our
						 * SYNACK anyway. Not only would it be a bad idea to enter another period of
						 * waiting for the network for at least a second, as we do in all other
						 * places where we wait for packets, this way we can actually get DOS
						 * (denial of service)ed. If during that second we're waiting we receive
						 * another SYN packet (which is quite likely) we will enter the same loop
						 * again, and again and again. Instead, just sit out the rest of the second
						 * here and then resend the SYNACK. This does mean that if we first get an
						 * attack packet, and then a valid one (FINACK or data ACK), the remote will
						 * have to wait a while before getting a reply to it, and will receive a
						 * SYNACK first. That's not really an issue though. */
						if(!state.processInbound(received))
						{
							timeout.waitForTimeout();
						}
						else
						{
							/* We actually want this packet. It will be handled, then accept() is
							 * done. */
							if(received.hasData())
							{
								bufferData(received);
								sendAckOnNextIteration = true;
							}
							if (state.getState() == ConnectionState.S_ESTABLISHED)
							{
								/* We are connected! Start socket thread and return! */
								if(TCP.DEBUG) Log.d(sockTag(), "Accept: ACK received, connection established");
								socketThread.start();
								return;
							}
							else if(state.getState() == ConnectionState.S_CLOSE_WAIT)
							{
								/* We are connected, but the host already wants to close. Let's tell
								 * him how it is, because we can still write. */
								if(TCP.DEBUG) Log.d(sockTag(), "Accept: FIN received, connection half-open");
								socketThread.start();
								return;
							}
						}
					}
				} 
			}
			catch (IOException ioe)
			{
				/* Log exception and reset the socket to try again! */
				if(TCP.ERROR) Log.e(sockTag(), "fatal IOException in accept()\n");
				reset();
			}
		}
		if(TCP.WARN) Log.w(sockTag(), "Accept: Interrupted!");
	}
	
	/**
	 * Returns a new instance of PacketData. We must fill it with data and then pass it to
	 * writePacket to write it to the network, or to freePacket if we decide not to write it.
	 * 
	 * @return	A new packet data instance.
	 */
	public PacketData newPacket()
	{
		synchronized(lock)
		{
			if(!this.unusedBuffers.isEmpty()) return this.unusedBuffers.remove();
			else return new PacketData();
		}
	}
	
	/**
	 * Accepts a packet that the application does not need anymore and empties it.
	 * 
	 * It should always be called after readPacket(). When freePacket was already called on this 
	 * packet, nothing happens.
	 */
	public void freePacket(PacketData data)
	{
		synchronized(lock)
		{
			if(!this.unusedBuffers.contains(data))
			{
				data.empty();
				this.unusedBuffers.add(data);
			}
		}
	}	
	
	/**
	 * Reads a packet. Only one thread can execute this method at a time.
	 * 
	 * After a packet is read and the application is done with it, freePacket() should be called.
	 * This is used to recycle PacketData instances.
	 * 
	 * @return	When a packet was read successfully, it is returned.
	 * 			NULL is returned when something failed, but we can try to read() again in the
	 * 			future. This is true when another thread is executing read.
	 * 
	 * @throws NoMoreDataException
	 *			Is thrown in case of a non-fatal failure:
	 *			> No more data will come in, because the remote closed the connection
	 *			> We were interrupted and the caller should stop reading and should return
	 *
	 * @throws IOException	
	 * 			Is thrown when a fatal error occurred (internal IOException!)
	 */
	public PacketData readPacket() throws IOException, NoMoreDataException
	{
		synchronized(lock)
		{
			/* Only allow a single thread to read */
			if(readStatus != ReadStatus.RS_IDLE) 
			{
				if(TCP.DEBUG) Log.d(sockTag(), "Read: Another thread is already reading.");
				return null;
			}
			if(!receivedBuffers.isEmpty())
			{
				/* Satisfy request from the receive buffers */
				if(TCP.DEBUG) Log.d(sockTag(), "Read: Reading from receive-buffer");
				return receivedBuffers.remove();	
			}
			else if(isReadingFromNetworkAllowed())
			{
				/* This makes sure no other readers can get here!! */
				readStatus = ReadStatus.RS_WAITING;
				try
				{
					if(TCP.DEBUG) Log.d(sockTag(), "Read: Reading from network...");
					
					/* Wait until the outcome of the read operation is known or until the remote is
					 * closing, indicating that no more data will come. */
					while(readStatus != ReadStatus.RS_SUCCES &&
						  readStatus != ReadStatus.RS_FAILED && !isRemoteClosing())
					{
						lock.wait();
					}
					
					if (readStatus == ReadStatus.RS_SUCCES)
					{
						if(TCP.DEBUG) Log.d(sockTag(), "Read: Reading from network successful");
						return receivedBuffers.remove();							
					} 
					else if (readStatus == ReadStatus.RS_FAILED && ioException != null)
					{
						if(TCP.ERROR) Log.d(sockTag(), "Read: Reading from network failed due to an IOException");
						throw this.ioException;
					} 
					else if (isRemoteClosing())
					{
						if(TCP.ERROR) Log.e(sockTag(), "Read: Reading from network failed. Connection is closing...");
						throw new NoMoreDataException();
					}
					else throw new IllegalStateException("Your programmer has made a mistake!");
				}
				catch(InterruptedException ie)
				{
					/* We were interrupted. May be caused by a test that timed out. Just abort! */
					if(TCP.WARN) Log.w(sockTag(), "Read: Interrupted!");
					return null;
				}
				finally
				{
					/* Another reader can read now.
					 * Also clear the exception we may have thrown! */
					readStatus = ReadStatus.RS_IDLE;
					ioException = null;
				}
			} 
			else
			{
				if(TCP.ERROR) Log.w(sockTag(), "Read: Failed. Connection is closing and the buffers are empty.");
				throw new NoMoreDataException();
			}
		}
	}
	
	/**
	 * Instructs the thread to write the packet to the remote. The packet passed to writePacket
	 * is automatically recycled so there is no need to call freePacket().
	 * Only one thread can execute this method at a time.
	 * 
	 * @param data	A packet worth of data to write.
	 * @return		The number of bytes written OR
	 * 				0, when we failed because another thread wrote first, but we can try again
	 * 				-1, when we failed to write because all our packets (or their ACKs) were lost
	 * 
	 * @throws IOException 	When a write failed because of an IOException
	 */
	public int writePacket(PacketData data, boolean gotMore) throws IOException
	{
		synchronized(lock)
		{
			if(!isWritingAllowed()) 
			{
				if(TCP.ERROR) Log.e(sockTag(), "Cannot write() when there is no connection!");
				return -1;
			}	
			if(data.isEmpty())
			{
				if(TCP.ERROR) Log.e(sockTag(), "Write() failed. Packet is empty!");
				return -1;			
			}
			if(sendBuffer != null)
			{
				/* Another write was first, so we will inform the caller that we failed but that it
				 * is worthwhile to try it again. */
				if(TCP.WARN) Log.w(sockTag(), "Write: Another thread is already writing.");
				return 0;				
			}
			else
			{
				/* We have the opportunity to write! */
				try
				{
					moreWritesComing	= gotMore;
					sendBuffer 			= data;		
					writeStatus			= WriteStatus.WS_PENDING;
					transmissionsLeft	= TCP.Socket.MAX_TRANSMISSIONS;
					
					synchronized(moreWritePacketsLock)
					{
						moreWritePacketsLock.notify();
					}
					
					if(TCP.DEBUG) Log.d(sockTag(), "Write: Trying to write....");
					
					/* We wait until the outcome of our write operation is known or until the
					 * connection is closed. */
					while (writeStatus != WriteStatus.WS_SUCCES &&
						   writeStatus != WriteStatus.WS_FAILED && !isClosing())
					{
						lock.wait();
					}
					
					if (writeStatus == WriteStatus.WS_SUCCES)
					{
						if(TCP.DEBUG) Log.d(sockTag(), "Write: successful.");
						return sendBuffer.getLength();						
					}
					else if (writeStatus == WriteStatus.WS_FAILED && ioException != null)
					{
						/* We failed and an ioException was set. Re-throw it! Because of this error
						 * we do not expect any more writes from the application. */
						moreWritesComing = false;
						throw ioException;
					}
					else if (writeStatus == WriteStatus.WS_FAILED || isClosing())
					{
						/* We failed either because of too many lost packets/ACKs or because the
						 * connection was closed while we were writing. Because of the error we do
						 * not expect any more writes from the application */
						moreWritesComing = false;
						if(TCP.WARN) Log.w(sockTag(), "Write: failed.");
						return -1; 
					}
					else throw new IllegalStateException("Your programmer has made a mistake!");
				}
				catch(InterruptedException ie)
				{
					/* We were interrupted and we do not know whether the data was written! Return
					 * an error. */
					Log.w(sockTag(), "Write: Interrupted!");
					return -1;
				}
				finally
				{
					sendBuffer 	= null;
					ioException = null;
					writeStatus	= WriteStatus.WS_IDLE;
				}
			}
		}
	}
	
	/**
	 * Does exactly what it says in {@link TCP.Socket#close()}.
	 * 
	 * @return	Whether the close was graceful or not.
	 */
	public boolean close()
	{
		synchronized (lock)
		{
			if (!isEstablished()) 
			{
				if(TCP.ERROR) Log.e(sockTag(), "close() failed. No connection or busy with teardown!");
				return false;
			}
			
			/* Terminates the read/write loop, ensuring the thread will stop, no matter what, within
			 * give or take ten seconds. */
			if(TCP.DEBUG) Log.d(sockTag(), "close() called!");
			closeCalled = true;
		}
		
		try
		{
			if(TCP.DEBUG) Log.d(sockTag(), "close: Waiting for join!");
			this.socketThread.join();
			
			/* Create a new thread immediately, since the same thread object can never be started twice! */
			this.socketThread = new Thread(this);
		}
		catch (InterruptedException ie)
		{
			if(TCP.WARN) Log.w(sockTag(), "close: Interrupted while waiting for thread to terminate!");
		}
		
		if(TCP.DEBUG) Log.d(sockTag(), "close: Returning true");
		return true;
	}
	
	/**
	 * Returns whether the connection is established. Note that the connection can be established
	 * even when the remote is already closing.
	 * 
	 * Use isEstablished() to determine whether we can call close() or write().
	 */
	public boolean isEstablished()
	{
		return (state.getState() == ConnectionState.S_ESTABLISHED ||
				state.getState() == ConnectionState.S_CLOSE_WAIT);
	}
	
	/**
	 * Returns true, when we are closing and will not send any data to the remote.
	 */
	public boolean isClosing()
	{
		return (state.getState() == ConnectionState.S_FIN_WAIT_1 ||
				state.getState() == ConnectionState.S_FIN_WAIT_2 ||
				state.getState() == ConnectionState.S_CLOSING ||
				state.getState() == ConnectionState.S_TIME_WAIT ||
				state.getState() == ConnectionState.S_LAST_ACK);
	}
	
	/**
	 * Returns true when the remote is closing and will not send any data to us.
	 * In other words, we have received a FIN.
	 */
	public boolean isRemoteClosing()
	{
		return (state.getState() == ConnectionState.S_CLOSING ||
				state.getState() == ConnectionState.S_TIME_WAIT ||
				state.getState() == ConnectionState.S_CLOSE_WAIT ||
				state.getState() == ConnectionState.S_LAST_ACK);		
	}
	
	/**
	 * Returns true if we are currently in the connect-phase
	 * @return
	 */
	public boolean isConnecting()
	{
		return	 (state.getState() == ConnectionState.S_LISTEN ||
				  state.getState() == ConnectionState.S_SYN_SENT ||
				  state.getState() == ConnectionState.S_SYN_RECEIVED);		
	}	
	
	/**
	 * Reading from the network only makes sense if we have a connection and the host is not
	 * closing. in other words, reading is allowed when in ESTABLISHED, FIN_WAIT_1 or FIN_WAIT_2!
	 */
	private boolean isReadingFromNetworkAllowed()
	{
		return !isConnecting() && !isRemoteClosing() && state.getState() != ConnectionState.S_CLOSED;
	}
	
	
	/**
	 * You can only start a new connection (call connect() or accept()) if you're in CLOSED or
	 * TIME_WAIT.
	 */
	private boolean isConnectionSetupAllowed()
	{
		return state.getState() == ConnectionState.S_CLOSED || state.getState() == ConnectionState.S_TIME_WAIT;
	}
	
	
	/**
	 * Writing is allowed in states ESTABLISHED, CLOSE_WAIT
	 */
	public boolean isWritingAllowed()
	{
		return isEstablished();
	}
	
	/* ============================= INTERNALS ================================================== */
	
	public void run()
	{
		try
		{			
			/* Read and write from/to the network while the application lets us continue. Do not
			 * exit if we still need to ack something, or it will never happen however. */
			while (!closeCalled() || sendAckOnNextIteration)
			{
				/* This method will take at most 1 second, give or take */
				doReadingAndWriting();
				
				/* Allow other threads to run as well! */
				Thread.yield();
			}
			
			if(TCP.DEBUG) Log.d(sockTag(), "Run: closing connection....");
			
			/* The application has called close() so we must tear down this connection. This metho
			 * will return within ten seconds, give or take. */
			closeConnection();
		}
		/* We assume that IOExceptions and too many timeouts are fatal and will reset the connection
		 * if this is not the case, they should be caught before ending up here! */
		catch (IOException ioe)
		{
			if(TCP.ERROR) Log.e(sockTag(), "IOException in SocketThread:");
			ioe.printStackTrace();
		}
		catch (InterruptedException ie)
		{
			if(TCP.WARN) Log.w(sockTag(), "SocketThread was interrupted!");
		}
		
		/* Reset all the state of the connection */
		reset();
		if(TCP.DEBUG) Log.d(sockTag(), "SocketThread: terminating thread");
	}
	
	/**
	 * Handles pending read and write operations.
	 * 
	 * It first sends a packet when needed. This could be two things:
	 * 		> An acknowledgment
	 * 		> Pending data
	 * 
	 * Then it waits for a packet from the network and processes it. First of all, the state is
	 * updated by processInbound() and the packet is classified using a receive-type. Depending on
	 * the receive-type and the flags of the packet, we will take action:
	 * 		> Incoming new acknowledgments will cause our current write operation to succeed and
	 * 			unblock
	 * 		> Incoming new data will be buffered and  acknowledged
	 * 		> Retransmissions will be acknowledged
	 * 		> FIN packets will be acknowledged
	 * 
	 */
	private void doReadingAndWriting() throws InterruptedException
	{
		try
		{
			/* First test if the write timer has already timed out. This would mean that we need to
			 * retransmit our data packet */
			handleWriteTimeouts();
			
			/* If we were told more writes are coming immediately, wait a little while before going
			 * to the read phase */
			if(moreWritesComing) waitForNextWrite();
			
			/* We must send a packet in two cases:
			 * 
			 * 		> We have pending data to be sent
			 * 		> We need to acknowledge a received packet */
			if (writeStatus == WriteStatus.WS_PENDING || sendAckOnNextIteration)
			{
				toSend.reset(false);
				toSend.setFlags(FlagSet.F_ACK);
				if (writeStatus == WriteStatus.WS_PENDING) 
				{	
					/* This is a data packet. Insert data and reset timer */
					toSend.setData(sendBuffer);
					writeStatus = WriteStatus.WS_WAITING;
					writeTimeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
					writeTimeout.start();
				}
				state.processOutbound(toSend);
				sendTCPPacket(toSend);			
				
				sendAckOnNextIteration = false;
			}
			
			/* Next we will read, which will block this thread for one second! We only want to read
			 * when there is a waiting reader or when we are waiting for an ACK from the remote.
			 * Return in all other cases! */
			if(readStatus  != ReadStatus.RS_WAITING && writeStatus != WriteStatus.WS_WAITING) return;
			
			/* Now wait until we get something back from the network */
			receiveTimeout.reset(MIN_IP_TIMEOUT);
			
			if (receiveTCPPacket(received, remoteIP, receiveTimeout)) 
			{
				/* We received something before the timeout */
				
				synchronized (lock)
				{
					/* Process packet */
					state.processInbound(received);
					
					/* Lets see what we have.... */
					switch (state.getLastReceiveType())
					{
						case RT_REPLY:
							if (writeStatus == WriteStatus.WS_WAITING) 
							{
								/* We got a reply to our previously sent data packet. So write was
								 * successful */
								writeStatus = WriteStatus.WS_SUCCES;
								
								/* Wake up both reader and writer. The writer is known to unblock! */
								lock.notifyAll();
							}
							//Fallthrough!
						case RT_SIMULTANEOUS:
							if(received.hasData()) 
							{
								/* We have received data. Buffer it and acknowledge! */
								if(TCP.DEBUG) Log.d(sockTag(), "doReadingAndWriting: Buffering data");
								bufferData(received);
								sendAckOnNextIteration = true;
							}
							else if (received.getFlags() == FlagSet.F_FINACK)
							{
								/* Other side wants to close. Acknowledge! */
								sendAckOnNextIteration = true;
								
								/* Any reader waiting for more data should now fail because no more
								 * data will be coming from the network. */
								lock.notifyAll();								
							}	
							break;
						case RT_RETRANSMITTED:
							/* Data or SYNACK retransmission, just acknowledge... */
							sendAckOnNextIteration = true;
							break;
						default:
							if(TCP.WARN) Log.w(sockTag(), "IO: unexpected receive-type: "+state.getLastReceiveType());
							return;
					}
				}
			}
		}
		catch (IOException ioe)
		{
			if(TCP.ERROR) Log.e(sockTag(),"fatal IOException during network I/O");		
			
			/* Report the error to the reader and the writer, if they are waiting for data/ACK. */
			synchronized (lock)
			{
				if (readStatus == ReadStatus.RS_WAITING)  	readStatus = ReadStatus.RS_FAILED;
				if (writeStatus == WriteStatus.WS_WAITING)	writeStatus = WriteStatus.WS_FAILED;
				
				if (readStatus == ReadStatus.RS_WAITING || 
					writeStatus == WriteStatus.WS_WAITING)
				{
					/* Save the IOException and wake up the reader and writer so they can rethrow it */
					ioException = ioe;
					lock.notifyAll();
				}
			}
		}
	}
	

	/**
	 * Provides a synchronized way of accessing the closeCalled field.
	 */
	private boolean closeCalled()
	{
		synchronized(lock)
		{
			return this.closeCalled;
		}
	}
	
	/**
	 * Will process any timeouts caused by writing
	 * 
	 * When a write operation is in progress, we will test whether the write timer has timed out.
	 * If it has, this means that we did not get an ACK back in time, so we need to retransmit.
	 * This is done by resetting the writeStatus to WS_PENDING so that the packet will be resent.
	 * 
	 * If we ran out of attempts, the write operation is marked as failed and the writer is
	 * notified.
	 */
	private void handleWriteTimeouts()
	{
		synchronized (lock)
		{
			if (writeStatus == WriteStatus.WS_WAITING) 
			{
				try
				{
					writeTimeout.split();
				}
				catch (InterruptedException ie)
				{
					/* We actually timed out */
					transmissionsLeft--;
					if (transmissionsLeft <= 0)
					{
						/* Set the status and wake up the writer! */
						writeStatus = WriteStatus.WS_FAILED;	
						state.writeFailed();
						transmissionsLeft = TCP.Socket.MAX_TRANSMISSIONS;
						lock.notifyAll();
					}	
					else
					{
						/* Lets try again */
						writeStatus = WriteStatus.WS_PENDING;						
					}
				}
			}
		}		
	}
	
	/**
	 * Blocks until either:
	 * > A small amount of time elapsed
	 * > The writer has provided a new packet to write 
	 * 
	 * @throws InterruptedException
	 */
	private void waitForNextWrite()
	{
		synchronized(moreWritePacketsLock)
		{
			long millisToWait = 50;
			long starTime = System.currentTimeMillis();
			while(writeStatus != WriteStatus.WS_PENDING && System.currentTimeMillis() - starTime < millisToWait)
			{
				try
				{
					moreWritePacketsLock.wait(starTime + millisToWait - System.currentTimeMillis());
				}
				catch(InterruptedException e)
				{
					/* We were just being nice, and it's only 50ms anyway, so never mind */
				}
			}
		}		
	}
	
	/**
	 * Performs the necessary send/receive operations to close the connection.
	 * When closeConnection() terminates, the socket must still be reset using reset().
	 * 
	 * @throws InterruptedException	When timeWait gets interrupted
	 */
	private void closeConnection() throws IOException
	{
		synchronized(lock)
		{
			/* State transitions in close method:
			 * 
			 * 1. Active close:
			 * ESTABLISHED -(CLOSE/FIN)-> FIN_WAIT_1 -(ACK/-)-> FIN_WAIT_2 - (FIN/ACK) -> TIME_WAIT	
			 * 
			 * 2. Active simultaneous close:
			 * ESTABLISHED -(CLOSE/FIN)-> FIN_WAIT_1 -(FIN/ACK)-> CLOSING -(ACK/-)-> TIME_WAIT
			 * 
			 * 3. Passive close:
			 * CLOSE_WAIT -(CLOSE/FIN)-> LAST_ACK -(ACK/-)-> CLOSED 
			 * */
			if(state.getState() == ConnectionState.S_ESTABLISHED) doActiveClose();
			else if(state.getState() == ConnectionState.S_CLOSE_WAIT) doPassiveClose();
			else throw new IllegalStateException("Your programmer has made a mistake!");
		}
	}

	/**
	 * Does an active close on the connection and sets closeResult.
	 * 
	 * @throws InterruptedException	When timeWait gets interrupted
	 */
	private void doActiveClose()
	{
		synchronized(lock)
		{
			/* We will return triumphantly within 10 seconds, no matter what happens */
			Timeout closeTimeout = new Timeout();
			closeTimeout.reset(TCP.Socket.MAX_TRANSMISSIONS * TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
			closeTimeout.start();
			
			/* Active close means first going from ESTABLISHED to FIN_WAIT_1 */
			state.setState(ConnectionState.S_FIN_WAIT_1);
			
			/* We must notify the writer because it should fail at this point */
			lock.notifyAll();
			
			try
			{
				/* We will send a FIN packet */
				
				/* Attempt to send the FIN packet and get the ACK */
				int attempts = TCP.Socket.MAX_TRANSMISSIONS;
				Timeout timeout = new Timeout();
				while(attempts > 0)
				{
					
					/* Only (re)send the FIN packet if the timer isn't still running with time left. */
					if(!(timeout.isRunning() && timeout.hasTimeLeft()))
					{
						/* (Re)send the FIN packet */
						attempts--;
						if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: Sending FIN (attemptLeft: "+attempts+")");
						
						fin();
						timeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
					}
					
					/* Wait for a timeout or a valid packet (new, simultaneous, retransmission) */
					boolean hasReceivedPacket = false;
					do
					{
						hasReceivedPacket = receiveTCPPacket(received, remoteIP, timeout);
					}
					while(hasReceivedPacket && !state.processInbound(received));
					
					/* Once we end up here, one of two things happened. Either we timed out, or
					 * a valid packet was received. Valid packets are the following:
					 * 
					 * -	A retransmitted SYNACK			-> FIN_WAIT_1 (no change)
					 * -	A simultaneous FINACK ("FIN")	-> CLOSING
					 * -	A simultaneous ACK				-> FIN_WAIT_1 (no change)
					 * -	A new FINACK ("FINACK")			-> TIME_WAIT
					 * -	A new ACK 						-> FIN_WAIT_2
					 * 
					 * -	A retransmitted FINACK when running this loop again after having
					 * 		transitioned into the CLOSING state.
					 * 
					 * Handle all these cases separately for clarity. */
					if(!hasReceivedPacket) continue;
					else if(received.getFlags() == FlagSet.F_SYNACK &&
							state.getLastReceiveType() == ReceiveType.RT_RETRANSMITTED)
					{
						/* The remote doesn't know we're closing. It doesn't even know we're
						 * connected. Make our intentions known. They probably already received the
						 * previously sent FIN packet before this one arrives, but ah what the hell. */
						fin();
					}
					else if(received.getFlags() == FlagSet.F_FINACK &&
							state.getLastReceiveType() == ReceiveType.RT_SIMULTANEOUS)
					{
						/* This case is quite elaborate, so a special method will handle it */
						if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: simultaneous close detected!");
						doSimultaneousClose(timeout, attempts);
						return;
					}
					else if(received.getFlags() == FlagSet.F_ACK &&
							state.getLastReceiveType() == ReceiveType.RT_SIMULTANEOUS)
					{
						if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: Buffering data from SIM ACK in FW1");
						bufferData(received);
						ack();
					}
					else if(received.getFlags() == FlagSet.F_FINACK &&
							state.getLastReceiveType() == ReceiveType.RT_REPLY)
					{
						/* Other side was like "OK let's close." ACK, and exit. */
						/* A quick sanity check */
						if(state.getState() != ConnectionState.S_TIME_WAIT)
							throw new IllegalStateException("Your programmer made a mistake");
						if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: three way close detected!");
						ack();
						startRestartTimer();
						return;
					}
					// This else always returns
					else if(received.getFlags() == FlagSet.F_ACK && state.getLastReceiveType() == ReceiveType.RT_REPLY)
					{
						/* Other side was like "OK well I'm just gonna keep on talking." */
						/* A quick sanity check */
						if(state.getState() != ConnectionState.S_FIN_WAIT_2)
							throw new IllegalStateException("Your programmer made a mistake");
						
						if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: Our end of the connection is closed. We can only receive now.");
						
						/* That ack could have contained data, in which case we need to buffer that
						 * and reply. */
						if(received.hasData())
						{
							bufferData(received);
							ack();
						}
						while(state.getState() == ConnectionState.S_FIN_WAIT_2 && closeTimeout.hasTimeLeft())
						{
							/* The remote is not ready to close. Keep ACKing until we receive a FIN. */
							do
							{
								hasReceivedPacket = receiveTCPPacket(received, remoteIP, closeTimeout);
								Log.i(sockTag(), "doActiveClose: received packet");
							}
							while(!state.processInbound(received));
							
							/* We received something. It's one of three packets:
							 * 
							 * -	A new ACK			-> A new data packet from the remote
							 * -	A retransmitted ACK	-> Our last ACK didn't arrive
							 * -	A new FINACK		-> The remote is ready to close now */
							if(received.getFlags() == FlagSet.F_ACK &&
									state.getLastReceiveType() == ReceiveType.RT_REPLY)
							{
								/* Buffer data and acknowledge */
								if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: Buffering data from new ACK in FW2");
								bufferData(received);
								ack();
							}
							else if(received.getFlags() == FlagSet.F_ACK &&
									state.getLastReceiveType() == ReceiveType.RT_RETRANSMITTED)
							{
								ack();
							}
							else if(received.getFlags() == FlagSet.F_FINACK &&
									state.getLastReceiveType() == ReceiveType.RT_REPLY)
							{
								ack();
							}
							/* A quick sanity check */
							else throw new IllegalStateException("Your programmer made a mistake");
						}
						
						/* Either the remote is done as well, or its time ran out. Close! */
						if(TCP.DEBUG) Log.d(sockTag(), "doActiveClose: Fully closed!");
						state.setState(ConnectionState.S_TIME_WAIT);
						startRestartTimer();
						return;
					}
					/* A quick sanity check */
					else throw new IllegalStateException("Your programmer made a mistake");
					
					/* In case of a timeout or a retransmission, we just need to re-iterate */
				}
				
				/* We did several attempts to get an acknowledgment, but the remote did not respond.
				 * We will reset the connection on this side, and return true anyway. */
				state.setState(ConnectionState.S_TIME_WAIT);
				startRestartTimer();
				return;
			}
			catch (IOException ioe)
			{
				/* Fatal IO error */
				if(TCP.ERROR) Log.e(sockTag(), "Fatal IO error during close().\n");
			}
		}
	}
	
	
	/**
	 * Entered CLOSING: simultaneous closing time
	 * @throws IOException From the IP layer
	 * @throws InterruptedException When timeWait gets interrupted
	 */
	private void doSimultaneousClose(Timeout timeout, int attemptsLeft) throws IOException
	{
		/* A quick sanity check */
		if(state.getState() != ConnectionState.S_CLOSING)
			throw new IllegalStateException("Your programmer made a mistake");
		ack();
		// the attempts are handled inside
		while(true)
		{
			/* Wait for a packet to arrive, or until it's time to resend our FIN. Use
			 * the same, still running, timeout we used before. */
			boolean hasReceivedPacket = false;
			do
			{
				hasReceivedPacket = receiveTCPPacket(received, remoteIP, timeout);
			}
			while(hasReceivedPacket && !state.processInbound(received));
			
			/* Either a timeout occurred, or one of two packet arrived:
			 * 
			 * -	Timeout							-> Resend FIN packet
			 * -	A new ACK						-> A reply to our FIN, go to TIME_WAIT
			 * -	An retransmitted FINACK ("FIN")	-> Our ACK to their FIN got lost, reACK */
			if(!hasReceivedPacket)
			{
				/* Timeout expired.. If we're allowed to, send the FIN packet again and wait for
				 * the packets described above. If not, give up. */
				
				if(attemptsLeft == 0)
				{
					/* That's it, no more next time. Since the host wants to close as well, just
					 * close . */
					state.setState(ConnectionState.S_TIME_WAIT);
					startRestartTimer();
					return;
				}
				/* Resend the FIN packet */
				fin();
				attemptsLeft--;
				timeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
			}
			else if(received.getFlags() == FlagSet.F_ACK && state.getLastReceiveType() == ReceiveType.RT_REPLY)
			{
				/* A quick sanity check */
				if(state.getState() != ConnectionState.S_TIME_WAIT)
					throw new IllegalStateException("Your programmer made a mistake");
				startRestartTimer();
				return;
			}
			else if(received.getFlags() == FlagSet.F_FINACK &&
					state.getLastReceiveType() == ReceiveType.RT_RETRANSMITTED)
			{
				fin();
			}
			/* A quick sanity check */
			else throw new IllegalStateException("Your programmer made a mistake");
		}
	}
	

	private void ack() throws IOException
	{
		toSend.reset(false);
		toSend.setFlags(FlagSet.F_ACK);
		state.processOutbound(toSend);
		sendTCPPacket(toSend);
	}
	
	private void fin() throws IOException
	{
		toSend.reset(false);
		toSend.setFlags(FlagSet.F_FINACK);
		state.processOutbound(toSend);
		sendTCPPacket(toSend);
	}	
	
	private void doPassiveClose() throws IOException
	{
		synchronized(lock)
		{
			/* We're in CLOSE_WAIT, and close() was called. Transition to LAST_ACK */
			state.setState(ConnectionState.S_LAST_ACK);
			
			/* We must notify the reader and writer since they must fail with an error at this point */
			lock.notifyAll();
			
			/* Attempt to send the FIN packet and get the ACK */
			int attempts = TCP.Socket.MAX_TRANSMISSIONS;
			Timeout timeout = new Timeout();
			while(attempts > 0)
			{
				
				/* Only (re)send the FIN packet if the timer isn't still running with time left. */
				if(!(timeout.isRunning() && timeout.hasTimeLeft()))
				{
					/* (Re)send the FIN packet */
					attempts--;
					fin();
					if(TCP.DEBUG) Log.d(sockTag(), "doPassiveClose: Sending FIN (attemptLeft: "+attempts+")");
					timeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
				}
				
				/* Wait for a timeout or a valid packet */
				boolean hasReceivedPacket = false;
				do
				{
					hasReceivedPacket = receiveTCPPacket(received, remoteIP, timeout);
				}
				while(hasReceivedPacket && !state.processInbound(received));
			
				/* Either a timeout occurred, or a valid packet arrived. Three kinds of packets can
				 * arrive:
				 * 
				 * - A new ACK			-> That was the last one, the ACK to our FIN. Done, return
				 * 							true
				 * - A simultaneous ACK	-> ACK the retransmit, keep waiting
				 * - An old FIN			-> ACK the retransmit, keep waiting
				 */
				if(!hasReceivedPacket) continue;
				else if(received.getFlags() == FlagSet.F_ACK && state.getLastReceiveType() == ReceiveType.RT_REPLY)
				{
					/* The infamous last ACK! Transition to CLOSED and return true */
					state.setState(ConnectionState.S_CLOSED);
					if(TCP.DEBUG) Log.d(sockTag(), "doPassiveClose: last ACK received. Closed!");
					/* Everything has left the network now, so we do not need to enter TIME_WAIT */
					return;
				}
				else if(received.getFlags() == FlagSet.F_FINACK &&
						state.getLastReceiveType() == ReceiveType.RT_RETRANSMITTED)
				{
					ack();
				}
				else throw new IllegalStateException("Your programmer made a mistake");
			}
			
			/* Well, we tried. Just close the connection anyway (because the host was already
			 * willing to close). That last ACK might still be somewhere, so enter TIME_WAIT. */
			state.setState(ConnectionState.S_TIME_WAIT);
			startRestartTimer();
		}
	}
	
	
	/**
	 * Sends one TCP packet through this socket by encapsulating it in an IP packet and sending
	 * that.
	 * 
	 * @param tcpPacket	The TCP packet to send
	 * 
	 * @pre	The TCP packet must have the same source port, destination port and
	 * 		destination IP address as this socket.
	 */
	protected void sendTCPPacket(TCPPacket tcpPacket) throws IOException
	{			
		/* Build the internal byte array representation of the packet */
		tcpPacket.build();
		
		/* Allow the installed interceptors to manipulate or drop the packet, tail to head */
		try
		{
			for(int i = interceptors.size() - 1; i >= 0; i--)
			{
				Interceptor interceptor = interceptors.get(i);
				if(interceptor == null) continue;
				tcpPacket = interceptor.outboundPacket(tcpPacket);
			}
			if (tcpPacket == null) return;
		} 
		catch (AssertionFailedError afe)
		{
			/* Interceptors are used for testing, so there may be failing assertions here.
			 * Make sure that we know exactly which assertion failed, by logging the stacktrace */
			afe.printStackTrace();
			throw afe;
		}
		
		/* The destination address already contains all fields (192.168.1.x) and is already
		 * in little-endian form. The protocol is always the same. */
		sendIPPacket.destination = remoteIP.getAddress();
		sendIPPacket.source = tcp.ip.getLocalAddress().getAddress();
		sendIPPacket.protocol = TCP.PROTOCOL;
		sendIPPacket.id = tcp.nextIPID;
		sendIPPacket.data = tcpPacket.getIPData();
		sendIPPacket.length = tcpPacket.getIPData().length;
		if(TCP.DEBUG) Log.d(sockTag(), "SendTCPPacket: Sending to " + IpAddress.htoa(sendIPPacket.destination));
		
		/* Increment the IP packet identification, wrapping around if necessary. Because
		 * fragmentation is not supported, the identification can be increased with every packet. */
		tcp.nextIPID = (tcp.nextIPID + 1) % (TCP.MAX_IP_ID + 1);
		
		/* Send the IP packet */
		tcp.ip.ip_send(sendIPPacket);
	}
	
	/**
	 * Blocks until a valid packet from a specific remote has been received.  
	 *     
	 * @param  in					This is where the received packet will end up
	 * @param  remote		        The IP address of the remote host from which we expect to
	 * 								receive a TCP packet. When remote is NULL, any packet is
	 * 								accepted. 
	 * @param  timeout				A timeout object whose time is decremented while we are	blocked,
	 * 								waiting for a valid packet. When the timeout has expired,
	 * 								receiveTCPPacket returns false. When timeout is NULL, we will
	 * 								wait indefinitely for a valid packet.  
	 * @return						True, when a non-corrupt packet is received from "remote".
	 * 								False, in case of a timeout.       
	 * @throws IOException			some fatal IO error occurred.                                     
	 */
	protected boolean receiveTCPPacket(TCPPacket in, IpAddress remote, Timeout timeout) throws IOException
	{
		boolean isValid = false;
		
		try
		{
			do
			{
				try 
				{	
					if (timeout == null)
					{
						/* timeout not given, wait indefinitely */
						tcp.ip.ip_receive(receiveIPPacket);
					}
					else
					{
						/* Timeout specified, wait for a time, given by the timeout. */
						timeout.start();
						tcp.ip.ip_receive_timeout(receiveIPPacket, Math.max(1, timeout.getTimeLeft()));
					}
					/* Parse the TCP header and check checksum */
					in.receiveTCPPacket(receiveIPPacket);
					
					/* Allow the installed interceptor to manipulate or drop the packet, head to
					 * tail */
					try
					{
						for(Interceptor interceptor : interceptors)
						{
							if(interceptor == null) continue;
							in = interceptor.inboundPacket(in);
						}
						if (in == null) continue;
					} 
					catch (AssertionFailedError afe)
					{
						/* Interceptors are used for testing, so there may be failing assertions
						 * here. Make sure that we know exactly which assertion failed, by logging
						 * the stacktrace */
						afe.printStackTrace();
						throw afe;
					}
					
					/* No exception, so the checksum is valid Also check whether its from the right
					 * remote */
					isValid = (remote == null || in.getSourceIP().getAddress() == remote.getAddress());
					
					if(TCP.DEBUG) Log.d(sockTag(), "receiveTCPPacket: Non-corrupt packet received with flags: " +
							in.getFlags());
				} 
				catch (ChecksumException cse)
				{
					if(TCP.ERROR) Log.e(sockTag(),"ReceiveTCPPacket() has received a corrupt packet!\n");
				}
				catch (IllegalTCPFlagsException itfe)
				{
					if(TCP.ERROR) Log.e(sockTag(),"Invalid combination of flags in received packet!\n");
				}
			} 
			while (!isValid);
			
			return true;
		}
		catch (InterruptedException ie)
		{
			return false;
		}
	}
	
	/**
	 * Resets the state of the socket thread but does not clear the receive buffers.
	 */
	private void reset()
	{
		toSend.reset(true);
		received.reset(true);
		remoteIP = null;
		remotePort = 0;
		closeCalled = false;
		
		sendBuffer = null;
		writeStatus = WriteStatus.WS_IDLE;
		writeTimeout.reset(TCP.Socket.MAX_TIME_BEFORE_RETRANSMISSION);
		readStatus = ReadStatus.RS_IDLE;
		
		sendAckOnNextIteration = false;
		transmissionsLeft = TCP.Socket.MAX_TRANSMISSIONS;
	}
	
	/**
	 * Buffers the contents of a packet. If the packet is empty, nothing happens. If the packet
	 * contains data, newPacket() is used to get a PacketData object and store the data in it.
	 *
	 * @param packet
	 */
	private void bufferData(TCPPacket packet)
	{
		if(!packet.hasData()) return;
		/* Buffer the received data */
		PacketData packetData = newPacket();
		packetData.fromPacket(received);
		receivedBuffers.offer(packetData);
		
		/* If a reader is waiting, tell him that we've got some data! */
		if (readStatus == ReadStatus.RS_WAITING)
		{
			readStatus = ReadStatus.RS_SUCCES;
			lock.notifyAll();
		}
	}
	
	
	/**
	 * Execute TIME_WAIT period and close.
	 */
	private void startRestartTimer()
	{
		restartTimeout.reset(Socket.CONNECTION_WAIT_BEFORE_RESET_TIME);
		restartTimeout.start();
	}
	
	
	/**
	 * Waits for the restart timer to time out if we are in TIME_WAIT, then changes the state to
	 * CLOSED. It also drops all packets that may still be left in the IP layer. This makes sure
	 * we have a fresh start.
	 */
	private void leaveTimeWaitPeriod()
	{
		if(state.getState() == ConnectionState.S_TIME_WAIT)
		{
			/* Flush the packets in the IP layer. If we have any time left, wait and then
			 * set the connection to S_CLOSED! */
			restartTimeout.waitForTimeout();
			state.setState(ConnectionState.S_CLOSED);
		}
	}
	
	/**
	 * Clear the receive buffers
	 */
	private void clearReceiveBuffers()
	{
		while (!receivedBuffers.isEmpty())
		{
			freePacket(receivedBuffers.remove());
		}
	}
	
	/**
	 *  Get a unique sockTag() for this socket so we can distinguish it in the logfile
	 */
	private String sockTag()
	{
		return BASE_TAG + "_" + localPort;
	}
	
	/**
	 * The local port of the socket
	 * @return
	 */
	public int getLocalPort()
	{
		return this.localPort;
	}
	
	
	/**
	 * Allows tests to install interceptors, which allows them to drop/corrupt packets in a
	 * controlled fashion. The interceptor are run head to tail in the list for incoming packets,
	 * and tail to head for outgoing packets. They will receive all packets the way they were left
	 * by the interceptors in front of them (possibly NULL). Null interceptors will be skipped. The
	 * list may be null. In this case nothing happens (meaning the previously set list of
	 * interceptors will remain in place).
	 * 
	 * @param interceptor The interceptors which live between the TCP and IP layer
	 */
	protected void installInterceptors(ArrayList<Interceptor> interceptors)
	{
		if(interceptors != null) this.interceptors = interceptors;
	}
	
	/**
	 * Directly sets the Initial sequence number of the socket. This should be called before
	 * connect or accept!
	 */
	protected void setISN(long isn)
	{
		this.state.setISN(isn);
	}	
	
}
