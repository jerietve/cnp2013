package nl.vu.cs.cn.util;

import java.nio.ByteBuffer;

/**
 * Contains some utility methods that really don't need their own class
 */
public abstract class Util
{
	/**
	 * Converts a 32-bit little-endian number to a 32-bit big-endian number, and vice versa. The
	 * numbers are enclosed in an int. Because the int is merely used as a storage for 4 bytes, the
	 * value is of no meaning.
	 * 
	 * @param little	The 32-bit x-endian number to be converted
	 * @return			The 32-bit y-endian version of the number
	 */
	public static int switchEndian(int i)
	{
		/* >>, << and >>> take precedence over & which in turn goes before |. >>> is necessary
		 * in order to treat the sign bit as just another bit. */
		return i << 24 | i << 8 & 0xff0000 | i >> 8 & 0xff00 | i >>> 24;
	}
	
	
	/**
	 * Returns a string of 16 0s and 1s of the internal representation of the short
	 * 
	 * @param value	The value to print the binary two-complements representation of
	 * 
	 * @return A string describing the internal Java binary representation of the short
	 */
	public static String toBinaryString(short value)
	{
		StringBuilder builder = new StringBuilder();
		for(int bit = 15; bit >= 0; bit--)
		{
			builder.append((value & 0x1 << bit) >> bit);
		}
		return builder.toString();
	}
	
	
	/**
	 * Debugging method for creating a string containing all values from the start to the limit
	 * of a ByteBuffer.
	 * 
	 * @param in
	 * @return
	 */
	public static String toString(ByteBuffer in)
	{
		ByteBuffer buf = in.duplicate();
		String result = "";
		buf.rewind();
		for(int i = 0; i < buf.limit(); i++)
		{
			result += buf.get() + " ";
		}
		return result;
	}
}
