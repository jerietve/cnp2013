package nl.vu.cs.cn.util;

/**
 * Used to keep track of timeouts.
 */
public class Timeout 
{
	/* The time that was set by last call to reset() in milliseconds */
	private int initialTimeLeft;
	/* The start time of the blocking operation, in milliseconds */
	private long startTime;
	/* Is the timer started? */
	private boolean running;

	/**
	 * Sets the number of seconds it takes for this timeout to "time out".
	 * @param time 		Time in seconds
	 */
	public void reset(int time)
	{
		this.initialTimeLeft = time * 1000;
		this.running = false;
	}
	
	
	/**
	 * Must be called before a blocking operation starts. From this moment on, we will keep track of
	 * the time, until stop() is called.
	 * 
	 * When the timer was already started, nothing happens.
	 */
	public void start()
	{
		if (!running)
		{
			startTime = System.currentTimeMillis();
			running = true;
		}
	}
	
	
	/**
	 * Can be called after a blocking operation has ended. We will now check whether we have already
	 * timed out. If so, an InterruptedException is thrown and this object is reset. The timer is
	 * always stopped.
	 * 
	 * @throws InterruptedException When this timeout has "timed out".
	 */
	public void stop() throws InterruptedException
	{
		if (running)
		{
			this.running = false;
			if(!hasTimeLeft())
			{
				reset(initialTimeLeft / 1000);
				throw new InterruptedException();
			}
		}
	}
	
	
	/**
	 * Can be called after a blocking operation has ended. We will now check whether we have already
	 * timed out. If so, an InterruptedException is thrown and this object is reset. The timer is
	 * not stopped
	 * 
	 * @throws InterruptedException When this timeout has "timed out".
	 */
	public void split() throws InterruptedException
	{
		if(running)
		{
			if(!hasTimeLeft())
			{
				reset(initialTimeLeft);
				throw new InterruptedException();
			}
		}
	}
	
	/**
	 * Wait until the remaining time of the timeout has expired.
	 */
	public void waitForTimeout()
	{
		if(running)
		{
			while(hasTimeLeft())
			{
				try
				{
					Thread.sleep(Math.max(0, startTime + initialTimeLeft - System.currentTimeMillis()));
				}
				catch (InterruptedException ie)
				{
					/* Wait some more */
				}
			}	
		}
	}
	
	/**
	 * @return the time left, in seconds, rounded up.
	 */
	public int getTimeLeft()
	{
		return (int)Math.ceil(((System.currentTimeMillis() - startTime) / 1000.0));
	}
	
	
	/**
	 * @return true if this timer has not expired, false if it has
	 */
	public boolean hasTimeLeft()
	{
		return System.currentTimeMillis() - startTime < initialTimeLeft;
	}
	
	/**
	 * Returns true if the timer is started, false otherwise.
	 * @return
	 */
	public boolean isRunning()
	{
		return this.running;
	}
}
