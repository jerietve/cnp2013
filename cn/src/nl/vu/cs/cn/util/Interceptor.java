package nl.vu.cs.cn.util;

import nl.vu.cs.cn.TCPPacket;

/**
 * The interceptor is inserted between the TCP and IP layer and monitors/changes packets that are
 * sent or received. It can be used to simulate packet corruption, packet loss, attacks/spoofs or
 * just for analysis of the traffic.
 */
public interface Interceptor
{
	/**
	 * Called after the packet is built and before it is turned into an IP packet which is
	 * sent to the remote.
	 * 
	 * We may call TCPPacket.corrupt() here to corrupt the packet, or modify the packet and
	 * rebuild it or return null to drop it.
	 * 
	 * @param out	The outgoing packet
	 * @return		The packet that is sent to the IP layer
	 */
	public abstract TCPPacket outboundPacket(TCPPacket out);
	
	/**
	 * Called after the packet is parsed and the checksum is verified.
	 * 
	 * We may not call TCPPacket.corrupt() since the checksum is already verified. We may however
	 * change the packet (no rebuild required!) or return null to drop it.
	 * 
	 * @param in	The incoming packet
	 * @return		The packet that is sent back to our TCP implementation
	 */
	public abstract TCPPacket inboundPacket(TCPPacket in);
}