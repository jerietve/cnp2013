package nl.vu.cs.cn.data;

import java.nio.ByteBuffer;

import junit.framework.TestCase;

import nl.vu.cs.cn.data.BufferHelper;

/**
 * This class tests the writing behavior of the {@link BufferHelper} class. This class is supposed
 * to take a ByteBuffer and put the least significant x/2 bytes of a x-byte value (long, integer)
 * into a byte array, in order to simulate or ease the working with unsigned types. Tests are
 * included for the writing of values which do not really require to be unsigned, values which do,
 * and the overflow behavior is tested as well.
 * 
 * This test class uses robust worst-case boundary value testing. That just means a lot of tests ^^
 * 
 * @deprecated The functions tested by this class have all been deprecated.
 */
@Deprecated
public class BufferHelperWritingTest extends TestCase
{
	/* Buffer initialized in setUp around a byte array of four bytes */
	private ByteBuffer buffer;
	
	public BufferHelperWritingTest()
	{
		super();
	}
	
	
	@Override
	/**
	 * Initialize buffer to wrap an array of four zero bytes.
	 */
	public void setUp() throws Exception
	{
		super.setUp();
		buffer = ByteBuffer.wrap(new byte[4]);
	}
	
	
	/** Try to insert an integer representing a value that could also really be represented by a
	 * short: [0, 2^15). */
	public void testPutFittingShort()
	{
		BufferHelper.putUnsignedShort(buffer, 0x7fff);
		assertEquals("First byte", (byte)0x7f, buffer.get(0));
		assertEquals("Second byte", (byte)0xff, buffer.get(1));
	}
	
	
	/** Try to insert an integer representing a value that could not really be represented by a
	 * short, but that still fits in 16 bits: [2^15, 2^16). */
	public void testPutNotFittingShort()
	{
		BufferHelper.putUnsignedShort(buffer, 0xffff);
		assertEquals("First byte", (byte)0xff, buffer.get(0));
		assertEquals("Second byte", (byte)0xff, buffer.get(1));
	}
	
	
	/** Try to insert an integer representing a value over 2^16 and make sure an 
	 * IllegalArgumentException is thrown and the buffer is not mutated. */
	public void testPutTooLargeShort()
	{
		int position = buffer.position();
		int limit = buffer.limit();
		try
		{
			BufferHelper.putUnsignedShort(buffer, 0xffff + 1);
			fail("IllegalArgumentException should have been thrown");
		}
		catch(IllegalArgumentException e)
		{
			// Expected behavior, ignore
		}
		/* The buffer should still be filled with zeroes (the default value for byte). */
		assertEquals("First byte should be zero", (byte)0, buffer.get(0));
		assertEquals("Second byte should be zero", (byte)0, buffer.get(1));
		assertEquals("Third byte should be zero", (byte)0, buffer.get(2));
		assertEquals("Fourth byte should be zero", (byte)0, buffer.get(3));
		assertEquals("Position should be unchanged", position, buffer.position());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
	}
	
	
	/** Try to insert an integer representing a value under 0 and make sure an 
	 * IllegalArgumentException is thrown and the buffer is not mutated. */
	public void testPutNegativeShort()
	{
		int position = buffer.position();
		int limit = buffer.limit();
		try
		{
			BufferHelper.putUnsignedShort(buffer, 0xffff * -1);
			fail("IllegalArgumentException should have been thrown");
		}
		catch(IllegalArgumentException e)
		{
			// Expected behavior, ignore
		}
		/* The buffer should still be filled with zeroes (the default value for byte). */
		assertEquals("First byte should be zero", (byte)0, buffer.get(0));
		assertEquals("Second byte should be zero", (byte)0, buffer.get(1));
		assertEquals("Third byte should be zero", (byte)0, buffer.get(2));
		assertEquals("Fourth byte should be zero", (byte)0, buffer.get(3));
		assertEquals("Position should be unchanged", position, buffer.position());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
	}
	
	
	/** Try to insert a long representing a value that could also really be represented by an
	 * integer: [0, 2^31). */
	public void testPutFittingInteger()
	{
		BufferHelper.putUnsignedInteger(buffer, 2147483647);
		assertEquals("First byte", (byte)0x7f, buffer.get(0));
		assertEquals("Second byte", (byte)0xff, buffer.get(1));
		assertEquals("Third byte", (byte)0xff, buffer.get(2));
		assertEquals("Fourth byte", (byte)0xff, buffer.get(3));
	}
	
	
	/** Try to insert a long representing a value that could not really be represented by an integer,
	 * but that still fits in 32 bits: [2^31, 2^32). */
	public void testPutNotFittingInteger()
	{
		BufferHelper.putUnsignedInteger(buffer, 4294967295L);
		assertEquals("First byte", (byte)0xff, buffer.get(0));
		assertEquals("Second byte", (byte)0xff, buffer.get(1));
		assertEquals("Third byte", (byte)0xff, buffer.get(2));
		assertEquals("Fourth byte", (byte)0xff, buffer.get(3));
	}
	
	
	/** Try to insert a long representing a value over 2^32 and make sure an 
	 * IllegalArgumentException is thrown and the buffer is not mutated. */
	public void testPutTooLargeInteger()
	{
		int position = buffer.position();
		int limit = buffer.limit();
		try
		{
			BufferHelper.putUnsignedInteger(buffer, 4294967296L);
			fail("IllegalArgumentException should have been thrown");
		}
		catch(IllegalArgumentException e)
		{
			// Expected behavior, ignore
		}
		/* The buffer should still be filled with zeroes (the default value for byte). */
		assertEquals("First byte should be zero", (byte)0, buffer.get(0));
		assertEquals("Second byte should be zero", (byte)0, buffer.get(1));
		assertEquals("Third byte should be zero", (byte)0, buffer.get(2));
		assertEquals("Fourth byte should be zero", (byte)0, buffer.get(3));
		assertEquals("Position should be unchanged", position, buffer.position());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
	}
	
	
	/** Try to insert a long representing a value under 0 and make sure an 
	 * IllegalArgumentException is thrown and the buffer is not mutated. */
	public void testPutNegativeInteger()
	{
		int position = buffer.position();
		int limit = buffer.limit();
		try
		{
			BufferHelper.putUnsignedInteger(buffer, -4294967296L);
			fail("IllegalArgumentException should have been thrown");
		}
		catch(IllegalArgumentException e)
		{
			// Expected behavior, ignore
		}
		/* The buffer should still be filled with zeroes (the default value for byte). */
		assertEquals("First byte should be zero", (byte)0, buffer.get(0));
		assertEquals("Second byte should be zero", (byte)0, buffer.get(1));
		assertEquals("Third byte should be zero", (byte)0, buffer.get(2));
		assertEquals("Fourth byte should be zero", (byte)0, buffer.get(3));
		assertEquals("Position should be unchanged", position, buffer.position());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
	}
	
	
	/** Test whether the ByteBuffer is mutated exactly as expected by inserting an unsigned short.
	 * The capacity and limit should be unchanged, and the position advanced by two. */
	public void testLimitCapacityPositionInsertingShort()
	{
		/* The capacity can't change.. Test it anyway */
		int capacity = buffer.capacity();
		int limit = buffer.limit();
		int position = buffer.position();
		
		BufferHelper.putUnsignedShort(buffer, 42);

		assertEquals("Capacity should be unchanged", capacity, buffer.capacity());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
		assertEquals("Position should be increased by two", position + 2, buffer.position());
	}
	
	
	/** Test whether the ByteBuffer is mutated exactly as expected by inserting an unsigned integer.
	 * The capacity and limit should be unchanged, and the position advanced by four. */
	public void testLimitCapacityPositionInsertingInteger()
	{
		/* The capacity can't change.. Test it anyway */
		int capacity = buffer.capacity();
		int limit = buffer.limit();
		int position = buffer.position();
		
		BufferHelper.putUnsignedInteger(buffer, 4224957290L);

		assertEquals("Capacity should be unchanged", capacity, buffer.capacity());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
		assertEquals("Position should be increased by four", position + 4, buffer.position());
	}
	
	
	/** Make sure a BufferOverflowException is thrown in case we try to input a short for which
	 * there is no space. */
	public void testBufferOverflowExceptionThrownIfNoSpaceForShort()
	{
		/* We have a four byte buffer. Decrease the limit to one, and try to insert a short. */
		buffer.limit(1);
		try
		{
			BufferHelper.putUnsignedShort(buffer, 3);
			fail("Expected BufferOverflowException trying to insert short into buffer with not enough space left");
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
	}
	
	
	/** Make sure a BufferOverflowException is thrown in case we try to input an integer for which
	 * there is no space. */
	public void testBufferOverflowExceptionThrownIfNoSpaceForInteger()
	{
		/* We have a four byte buffer. Move to position one, and try to insert an integer. */
		buffer.position(1);
		try
		{
			BufferHelper.putUnsignedInteger(buffer, 3);
			fail("Expected BufferOverflowException trying to insert integer into buffer with not enough space left");
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
	}
	
	
	/** Make sure the buffer is not changed in case we try to input a short for which there is no
	 * space. */
	public void testBufferNotMutatedIfNoSpaceForShort()
	{
		int position = buffer.position();	
		int limit = buffer.limit();	
		/* We have a four byte buffer. Decrease the limit to one, and try to insert a short. */
		buffer.limit(1);
		try
		{
			BufferHelper.putUnsignedShort(buffer, 3);
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
		/* The buffer should still be filled with zeroes (http://docs.oracle.com/javase/specs/jls/se7/html/jls-4.html#jls-4.12.5). */
		buffer.limit(buffer.capacity());
		assertEquals("First byte should be zero", (byte)0, buffer.get(0));
		assertEquals("Second byte should be zero", (byte)0, buffer.get(1));
		assertEquals("Third byte should be zero", (byte)0, buffer.get(2));
		assertEquals("Fourth byte should be zero", (byte)0, buffer.get(3));
		assertEquals("Position should be unchanged", position, buffer.position());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
	}
	
	
	/** Make sure the buffer is not changed in case we try to input an integer for which there is no
	 * space. */
	public void testBufferNotMutatedIfNoSpaceForInteger()
	{
		/* We have a four byte buffer. Move to position one, and try to insert an integer. */
		buffer.position(1);
		int position = buffer.position();
		int limit = buffer.limit();
		try
		{
			BufferHelper.putUnsignedInteger(buffer, 3);
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
		/* The buffer should still be filled with zeroes (the default value for byte). */
		assertEquals("First byte should be zero", (byte)0, buffer.get(0));
		assertEquals("Second byte should be zero", (byte)0, buffer.get(1));
		assertEquals("Third byte should be zero", (byte)0, buffer.get(2));
		assertEquals("Fourth byte should be zero", (byte)0, buffer.get(3));
		assertEquals("Position should be unchanged", position, buffer.position());
		assertEquals("Limit should be unchanged", limit, buffer.limit());
	}
}
