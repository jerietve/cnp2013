package nl.vu.cs.cn.data;

import java.nio.ByteBuffer;

import junit.framework.TestCase;

import nl.vu.cs.cn.data.BufferHelper;

/**
 * Tests the get methods of {@link BufferHelper} using robust testing.
 */
public class BufferHelperReadingTest extends TestCase
{
	private ByteBuffer buffer;
	
	
	public BufferHelperReadingTest()
	{
		super();
	}
	
	
	@Override
	/**
	 * Initialize buffer to wrap a byte array containing bytes that can be used for the testing of
	 * the reading of shorts and integers: The first byte begins with 1, the second with 0, and
	 * there are three more. Their values are 0xf0, 0x0f, 0xab, 0xcd 0xef.
	 */
	public void setUp() throws Exception
	{
		super.setUp();
		buffer = ByteBuffer.wrap(new byte[] {(byte)0xf0, (byte)0x0f, (byte)0xab, (byte)0xcd, (byte)0xef});
	}
	
	
	/** Test the reading of two bytes that will form a value that would fit into a signed short. */
	public void testGetFittingShort()
	{
		buffer.position(1);
		assertEquals(0x00000fab, BufferHelper.getUnsignedShort(buffer));
	}
	
	
	/** Test the reading of two bytes that will form a value that would not fit into a signed short,
	 * but which would fit into an unsigned short. */
	public void testGetNotFittingShort()
	{
		assertEquals(0x0000f00f, BufferHelper.getUnsignedShort(buffer));
	}
	
	
	/** Test the reading of four bytes that will form a value that would fit into a signed integer. */
	public void testGetFittingInteger()
	{
		buffer.position(1);
		assertEquals(0x0fabcdef, BufferHelper.getUnsignedInteger(buffer));
	}
	
	
	/** Test the reading of four bytes that will form a value that would not fit into a signed
	 * integer, but which would fit into an unsigned integer. */
	public void testGetNotFittingInteger()
	{
		assertEquals(4027558861L, BufferHelper.getUnsignedInteger(buffer));
	}
	
	
	/** Make sure a BufferOverflowException is thrown in case we try to read a short while there are
	 * less than two bytes left. */
	public void testBufferOverflowExceptionThrownIfNoShortLeft()
	{
		/* We have a five byte buffer. Decrease the limit to one, and try to read a short. */
		buffer.limit(1);
		try
		{
			BufferHelper.getUnsignedShort(buffer);
			fail("Expected BufferOverflowException trying to insert short into buffer with not enough space left");
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
	}
	
	
	/** Make sure a BufferOverflowException is thrown in case we try to read an integer while there
	 * are less than four bytes left */
	public void testBufferOverflowExceptionThrownIfNoIntegerLeft()
	{
		/* We have a five byte buffer. Move to position two, and try to read an integer. */
		buffer.position(2);
		try
		{
			BufferHelper.getUnsignedInteger(buffer);
			fail("Expected BufferOverflowException trying to insert integer into buffer with not enough space left");
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
	}
	
	
	/** Make sure the position is not changed in case we try to read a short while there are less
	 * than two bytes left. */
	public void testBufferNotMutatedIfNoShortLeft()
	{
		int position = buffer.position();
		/* Decrease the limit to one, and try to read a short. */
		buffer.limit(1);
		try
		{
			BufferHelper.getUnsignedShort(buffer);
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
		assertEquals("Position should be unchanged", position, buffer.position());
	}
	
	
	/** Make sure the position is not changed in case we try to read an integer while there are less
	 * than four bytes left. */
	public void testBufferNotMutatedIfNoIntegerLeft()
	{
		/* We have a five byte buffer. Increase the position to two, and try to read an integer. */
		buffer.position(2);
		int position = buffer.position();
		try
		{
			BufferHelper.getUnsignedInteger(buffer);
		}
		catch(Exception e)
		{
			// Expected behavior; ignore.
		}
		assertEquals("Position should be unchanged", position, buffer.position());
	}
}
