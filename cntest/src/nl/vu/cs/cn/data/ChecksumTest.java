package nl.vu.cs.cn.data;

import java.nio.ByteBuffer;

import junit.framework.TestCase;

import nl.vu.cs.cn.data.Checksum;

/**
 * Tests the {@link Checksum} class by giving it all sorts of nasty buffers to calculate the
 * checksum of. The results that should come out were first painstakingly calculated on paper by the
 * same programmer who is writing the introductory comments to all classes in both projects, so he
 * would like to give himself some credit for this right here right now :). I'm awesome.
 */
public class ChecksumTest extends TestCase
{
	public ChecksumTest()
	{
		super();
	}
	
	
	/** Inputting null should be the same as inputting an empty array of buffers, or 'nothing',
	 * which should result in a checksum of all 1s. */
	public void testNullInput()
	{
		assertEquals((short)0xffff, Checksum.calculate((ByteBuffer[])null));
	}
	
	
	/** Test with a single ByteBuffer created on top of an empty array, so after inversion only 1s
	 * should be left. */
	public void testEmptyArray()
	{
		ByteBuffer buf = ByteBuffer.wrap(new byte[] {});
		assertEquals((short)0xffff, Checksum.calculate(buf));
	}
	
	
	/** Test whether the complementing of input works. In case only one byte is put in, all bits
	 * should be inverted. */
	public void testComplementing()
	{
		ByteBuffer buf = ByteBuffer.wrap(new byte[] {(byte)0xf0, (byte)0xab});
		assertEquals((short)0x0f54, Checksum.calculate(buf));
	}
	
	
	/** Test whether the adding of the carry bit to the lowest bit works */
	public void testCarry()
	{
		/* We're going to add 1000000000000000 to itself to see if 1111111111111110 comes out, as it
		 * should after 0000000000000001 gets complemented. */
		ByteBuffer buf = ByteBuffer.wrap(new byte[] {(byte)0x80, (byte)0x00, (byte)0x80, (byte)0x00});
		assertEquals((short)0xfffe, Checksum.calculate(buf));
	}
	
	
	/** Test the checksum calculation on an input of four buffers: one with one byte, an empty one,
	 * null, and one with two bytes.	 */
	public void testMultipleNastyBuffers()
	{
		ByteBuffer buf1 = ByteBuffer.wrap(new byte[] {(byte)0xab});
		ByteBuffer buf2 = ByteBuffer.wrap(new byte[0]);
		ByteBuffer buf3 = null;
		ByteBuffer buf4 = ByteBuffer.wrap(new byte[] {(byte)0xbc, (byte)0xde});
		/* The calculation that should be done in the function is replicated here:
		 * 
		 * 1010 1011 1011 1100		0xab, 0xbc
		 * 1101 1110 0000 0000 +	0xde, 0
		 * ---------------------
		 * 1000 1001 1011 1101
		 * complement:
		 * 0111 0110 0100 0010 = 0x7642 = 30274
		 *  */
		assertEquals((short)30274, Checksum.calculate(buf1, buf2, buf3, buf4));
	}
	
	
	/** In this method the TCP header, including the IP pseudo header, of an empty TCP packet is
	 * constructed byte by byte, after which the result is verified. */
	public void testManualEmptyPacket()
	{
		/* We're going to send an IP packet from 192.168.0.100:1234 to 192.168.0.101:2345 using TCP
		 * with no data, sequence number 12 and all flags as described below */
		
		byte[] input = new byte[8 * 4];
		
		/* Source IP address*/
		input[0] = (byte)192;
		input[1] = (byte)168;
		input[2] = 0;
		input[3] = 100;
		/* Destination IP address */
		input[4] = (byte)192;
		input[5] = (byte)168;
		input[6] = 0;
		input[7] = 101;
		/* Reserved */
		input[8] = 0;
		/* Protocol */
		input[9] = 6;
		/* TCP segment length */
		input[10] = 0;
		input[11] = 20;
		/* Source port */
		short sourcePort = (short)1234;
		input[12] = (byte)(sourcePort >> 8);
		input[13] = (byte)sourcePort;
		/* Destination port */
		short destinationPort = (short)2345;
		input[14] = (byte)(destinationPort >> 8);
		input[15] = (byte)destinationPort;
		/* Sequence number */
		input[16] = input[17] = input[18] = 0;
		input[19] = 12;
		/* Acknowledge number */
		input[20] = input[21] = input[22] = input[23] = 0;
		/* Data offset (TCP header size), three reserved bits and NS flag (0) -> 0b01010000 */
		input[24] = 0x50;
		/* Flags, in order: CWR (0), ECE (0), URG (0), ACK (0), PSH (1), RST (0), SYN (0), FIN (0)
		 * -> 0b00001000 */
		input[25] = 0x8;
		/* Window size */
		input[26] = 0;
		input[27] = 1;
		/* Checksum, zero before checksum calculation */
		input[28] = input[29] = 0;
		/* Urgent pointer */
		input[30] = input[31] = 0;
		
		ByteBuffer inputBuf = ByteBuffer.wrap(input);
		
		/* The calculation that should be done in the function is replicated here, leaving out all
		 * zero words:
		 * 
		 * 1100 0000 1010 1000		192, 168
		 * 0000 0000 0110 0100 +	0, 100
		 * ---------------------
		 * 1100 0001 0000 1100
		 * 1100 0000 1010 1000 +	192, 168
		 * ---------------------
		 * 1000 0001 1011 0101
		 * 0000 0000 0110 0101 +	0, 101
		 * ---------------------
		 * 1000 0010 0001 1010
		 * 0000 0000 0000 0110 +	0, 6
		 * ---------------------
		 * 1000 0010 0010 0000
		 * 0000 0000 0001 0100 +	20
		 * ---------------------
		 * 1000 0010 0011 0100
		 * 0000 0100 1101 0010 +	1234
		 * ---------------------
		 * 1000 0111 0000 0110
		 * 0000 1001 0010 1001 +	2345
		 * ---------------------
		 * 1001 0000 0010 1111
		 * 0000 0000 0000 1100 +	12
		 * ---------------------
		 * 1001 0000 0011 1011
		 * 0101 0000 0000 1000 +	data offset, flags
		 * ---------------------
		 * 1110 0000 0100 0011
		 * 0000 0000 0000 0001 +	1
		 * ---------------------
		 * 1110 0000 0100 0100
		 * complement:
		 * 0001 1111 1011 1011 = 0x1FBB = 8123
		 *  */
		short output = (short)0x1FBB;
		assertEquals(output, Checksum.calculate(inputBuf));
	}
}
