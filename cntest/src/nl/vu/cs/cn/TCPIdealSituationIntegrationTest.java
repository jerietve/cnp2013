package nl.vu.cs.cn;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import nl.vu.cs.cn.executions.BaseExecution;

/**
 * This class tests different ideal scenarios of connections over a socket. All tests are set up as
 * follows. First there is a static nested class extending BaseExecution which implements the
 * scenario that the following tests will use. Next follows a method which creates the executions
 * and runs them. Then finally a bunch of tests follow which call this previous method and test
 * variables set by the executions during their run.
 */
public class TCPIdealSituationIntegrationTest extends TCPIntegrationTest
{
	public TCPIdealSituationIntegrationTest()
	{
		super();
	}

	
	/**
	 * The client connects to the accepting server.
	 * 
	 * Yeah that's it.
	 * 
	 * This is test scenario SCN22.
	 */
	private static class ConnectExecution extends BaseExecution
	{
		private boolean connectResult;
		
		@Override
		public void executeAsClient()
		{
			connectResult = socket.connect(serverIPAddress, serverPort);
		}


		@Override
		public void executeAsServer()
		{
			socket.accept();
		}
	}
	
	
	private boolean doConnectTest() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ConnectExecution();
		serverExecution = new ConnectExecution();
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doConnectTest();
		assertTrue(((ConnectExecution)clientExecution).connectResult);
	}
	
	
	public void acceptSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		/* To check this, we will just make sure the test exists within a second. Just for fun,
		 * let's set the time to 1234 to confuse people who do not read comments! */
		setMaxRunTime(1234);
		assertTrue(doConnectTest());
	}
	
	
	/**
	 * The client connects to the accepting server. The client writes a certain amount to the
	 * reading server. The server is used to verify that the data it received is exactly the data
	 * given to the client to send. The connection is not closed.
	 * 
	 * This is test scenario SCN1.
	 */
	private static class ConnectSingleWriteByClientExecution extends BaseExecution
	{
		private byte[] sendData;
		private int numBytesToRead;
		

		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			socket.write(sendData, 0, sendData.length);
		}


		@Override
		public void executeAsServer()
		{
			socket.accept();
			int numBytesReceived = 0;
			receivedData = new byte[numBytesToRead];
			while(numBytesReceived < receivedData.length)
			{
				numBytesReceived += socket.read(receivedData, numBytesReceived, receivedData.length - numBytesReceived);
			}
		}
	}
	
	
	private void doSingleWriteByClient(byte[] sendData) throws InterruptedException, ExecutionException, IOException
	{
		/* Create the client thread, which will connect and write */
		clientExecution = new ConnectSingleWriteByClientExecution();
		((ConnectSingleWriteByClientExecution)clientExecution).sendData = sendData;
		
		/* Create the server thread, which will accept and read a given number of bytes */
		serverExecution = new ConnectSingleWriteByClientExecution();
		((ConnectSingleWriteByClientExecution)serverExecution).numBytesToRead = sendData.length;
		
		runBothToCompletion(clientExecution, serverExecution);
		assertTrue(serverExecution.verifyReceivedData(sendData));
	}
	
	
	public void testClientSendOneByte() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByClient(getRandomBytes(1));
	}
	
	
	public void testClientSendTenBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByClient(getRandomBytes(10));
	}
	
	
	public void testClientSendOneHundredBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByClient(getRandomBytes(100));
	}
	
	
	public void testClientSendOneThousandBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByClient(getRandomBytes(1000));
	}
	
	
	public void testClientSendTenThousandBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByClient(getRandomBytes(10000));
	}
	
	
	public void testClientSendOneHundredThousandBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByClient(getRandomBytes(100000));
	}
	
	
	/**
	 * The client connects to the accepting server. The server writes a certain amount to the
	 * reading client. The client is used to verify that the data it received is exactly the data
	 * given to the server to send. The connection is not closed.
	 * 
	 * This is test scenario SCN2.
	 */
	private static class ConnectSingleWriteByServerExecution extends BaseExecution
	{
		private byte[] sendData;
		private int numBytesToRead;
		

		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			int numBytesReceived = 0;
			receivedData = new byte[numBytesToRead];
			while(numBytesReceived < receivedData.length)
			{
				numBytesReceived += socket.read(receivedData, numBytesReceived, receivedData.length - numBytesReceived);
			}
		}


		@Override
		public void executeAsServer()
		{
			socket.accept();
			socket.write(sendData, 0, sendData.length);
		}
	}
	
	
	private void doSingleWriteByServer(byte[] sendData) throws InterruptedException, ExecutionException, IOException
	{
		/* Create the client thread, which will connect and write */
		clientExecution = new ConnectSingleWriteByServerExecution();
		((ConnectSingleWriteByServerExecution)clientExecution).numBytesToRead = sendData.length;
		
		/* Create the server thread, which will accept and read a given number of bytes */
		serverExecution = new ConnectSingleWriteByServerExecution();
		((ConnectSingleWriteByServerExecution)serverExecution).sendData = sendData;
		
		runBothToCompletion(clientExecution, serverExecution);
		assertTrue(clientExecution.verifyReceivedData(sendData));
	}
	
	
	public void testServerSendOneThousandBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByServer(getRandomBytes(1000));
	}
	
	
	public void testServerSendTenThousandBytes() throws InterruptedException, ExecutionException, IOException
	{
		doSingleWriteByServer(getRandomBytes(10000));
	}
	
	
	/**
	 * Client and server connect, then do simultaneous close. The simultaneity comes from the fact
	 * that no implicit receiving of packets is done. Calling close() after connect() or accept()
	 * without any other calls will make sure no packets are read from the IP stack, so the one
	 * waiting for both connect() calls will be a FIN packet.
	 * 
	 * This is test scenario SCN3.
	 */
	private static class ConnectSimultanousCloseExecution extends BaseExecution
	{
		private boolean closeResult, connectResult;
		
		@Override
		public void executeAsServer()
		{
			socket.accept();
			closeResult = socket.close();
		}
		
		
		@Override
		public void executeAsClient()
		{
			connectResult = socket.connect(serverIPAddress, serverPort);
			closeResult = socket.close();
		}
	};
	
	
	private void doConnectSimultaneousClose() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ConnectSimultanousCloseExecution();
		serverExecution = new ConnectSimultanousCloseExecution();
		
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testConnectSimultaneousCloseClientConnectSucceeds() throws IOException, InterruptedException, ExecutionException
	{
		doConnectSimultaneousClose();
		assertTrue("Client should close connection nicely", ((ConnectSimultanousCloseExecution)clientExecution).connectResult);
	}
	
	
	public void testConnectSimultaneousCloseClientCloseSucceeds() throws IOException, InterruptedException, ExecutionException
	{
		doConnectSimultaneousClose();
		assertTrue("Client should close connection nicely", ((ConnectSimultanousCloseExecution)clientExecution).closeResult);
	}
	
	
	public void testConnectSimultaneousCloseServerCloseSucceeds() throws IOException, InterruptedException, ExecutionException
	{
		doConnectSimultaneousClose();
		assertTrue("Server should close connection nicely", ((ConnectSimultanousCloseExecution)serverExecution).closeResult);
	}
	
	
	/**
	 * Client connects to accepting server. Server reads while client closes immediately. The server
	 * then calls close. The server does a passive close while the client does an active close.
	 * 
	 * This is test scenario SCN4.
	 */
	private static class ConnectFourWayCloseExecution extends BaseExecution
	{
		private boolean closeResult;
		
		@Override
		public void executeAsServer()
		{
			byte[] buf = {42};
			socket.accept();
			/* Write a packet to delay our close(), while handling a closing remote */
			socket.write(buf, 0, 1);
			closeResult = socket.close();
		}
		
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			closeResult = socket.close();
		}
	};
	
	
	private void doConnectFourWayCloseTest() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ConnectFourWayCloseExecution();		
		serverExecution = new ConnectFourWayCloseExecution();
		
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testConnectFourWayCloseClientCloseSucceeds() throws IOException, InterruptedException, ExecutionException
	{
		doConnectFourWayCloseTest();
		assertTrue("Client close() method should return true", ((ConnectFourWayCloseExecution)clientExecution).closeResult);
	}
	
	
	public void testConnectFourWayCloseServerCloseSucceeds() throws IOException, InterruptedException, ExecutionException
	{
		doConnectFourWayCloseTest();
		assertTrue("Server close() method should return true", ((ConnectFourWayCloseExecution)serverExecution).closeResult);
	}
	
	
	/**
	 * Client and server connect. Both write a large, equal, amount of different bytes to each
	 * other. All this data should be buffered by the write call on the other side. Then read is
	 * called three times to get the data from the buffers. First only one buffer is needed, then
	 * multiple, and finally all data should be gotten from the buffers without requiring the
	 * receiving of another packet.
	 * 
	 * This is test scenario SCN5.
	 */
	private static class WriteToBufferExecution extends BaseExecution
	{
		private byte[] sendData;
		private final int firstReadTargetLength = 1000;
		private final int secondReadTargetLength = 9000;
		private final int thirdReadTargetLength = 90000;
		private int writeResult, firstReadResult, secondReadResult, thirdReadResult;
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			readAndWrite();
		}

		@Override
		public void executeAsServer()
		{
			socket.accept();
			readAndWrite();
		}
		
		
		private void readAndWrite()
		{
			receivedData = new byte[firstReadTargetLength + secondReadTargetLength + thirdReadTargetLength];
			
			writeResult = socket.write(sendData, 0, sendData.length);
			firstReadResult = socket.read(receivedData, 0, firstReadTargetLength);
			secondReadResult = socket.read(receivedData, firstReadTargetLength, secondReadTargetLength);
			thirdReadResult = socket.read(receivedData, firstReadTargetLength + secondReadTargetLength, thirdReadTargetLength);
		}
	}
	
	
	private void doWriteToBuffer(byte[] clientSendData, byte[] serverSendData) throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new WriteToBufferExecution();
		((WriteToBufferExecution)clientExecution).sendData = clientSendData;
		
		serverExecution = new WriteToBufferExecution();
		((WriteToBufferExecution)serverExecution).sendData = serverSendData;
		
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testWriteToBufferClientWritesAllDataAtOnce() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals("Client should write all data in one go", 100000, ((WriteToBufferExecution)clientExecution).writeResult);
	}
	
	
	public void testWriteToBufferServerWritesAllDataAtOnce() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals("Server should write all data in one go", 100000, ((WriteToBufferExecution)serverExecution).writeResult);
	}
	
	
	public void testWriteToBufferClientFirstReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals(((WriteToBufferExecution)clientExecution).firstReadTargetLength,
				((WriteToBufferExecution)clientExecution).firstReadResult);
	}
	
	
	public void testWriteToBufferServerFirstReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals(((WriteToBufferExecution)serverExecution).firstReadTargetLength,
				((WriteToBufferExecution)serverExecution).firstReadResult);
	}
	
	
	public void testWriteToBufferClientSecondReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals(((WriteToBufferExecution)clientExecution).secondReadTargetLength,
				((WriteToBufferExecution)clientExecution).secondReadResult);
	}
	
	
	public void testWriteToBufferServerSecondReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals(((WriteToBufferExecution)serverExecution).secondReadTargetLength,
				((WriteToBufferExecution)serverExecution).secondReadResult);
	}
	
	
	public void testWriteToBufferClientThirdReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals(((WriteToBufferExecution)clientExecution).thirdReadTargetLength,
				((WriteToBufferExecution)clientExecution).thirdReadResult);
	}
	
	
	public void testWriteToBufferServerThirdReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doWriteToBuffer(getRandomBytes(100000), getRandomBytes(100000));
		assertEquals(((WriteToBufferExecution)serverExecution).thirdReadTargetLength,
				((WriteToBufferExecution)serverExecution).thirdReadResult);
	}
	
	
	public void testWriteToBufferClientReceivedCorrectData() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(100000);
		doWriteToBuffer(getRandomBytes(100000), serverSendData);
		assertTrue(clientExecution.verifyReceivedData(serverSendData));
	}
	
	
	public void testWriteToBufferServerReceivedCorrectData() throws InterruptedException, ExecutionException, IOException
	{
		byte[] clientSendData = getRandomBytes(100000);
		doWriteToBuffer(clientSendData, getRandomBytes(100000));
		assertTrue(serverExecution.verifyReceivedData(clientSendData));
	}
	
	/**
	 * Sets up a connection, exchanges data and closes. After that, a new connection is set up
	 * and again data is exchanged and the connection is closed.
	 * 
	 * This is test scenario SCN28
	 */
	private static class SocketReuseExecution extends BaseExecution
	{
		private static final int DATA_LENGTH = 20000;
		private static final String TAG = "SocketReuseExecution";
		
		private byte[] sendData;
		private boolean secondConnectResult;
		private boolean secondCloseResult;
		private int writeResult;
		private int readResult;		
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			readAndWrite();
			socket.close();
			secondConnectResult = socket.connect(serverIPAddress, serverPort);
			readAndWrite();
			secondCloseResult = socket.close();
		}

		@Override
		public void executeAsServer()
		{
			socket.accept();
			readAndWrite();
			socket.close();
			socket.accept();
			readAndWrite();
			secondCloseResult = socket.close();
		}
		
		
		private void readAndWrite()
		{
			receivedData = new byte[DATA_LENGTH];
			writeResult = socket.write(sendData, 0, sendData.length);
			readResult = socket.read(receivedData, 0, receivedData.length);
		}		
		
	}
	
	private void doSocketReuse(byte[] sendData)  throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new SocketReuseExecution();
		((SocketReuseExecution)clientExecution).sendData = sendData;
		
		serverExecution = new SocketReuseExecution();
		((SocketReuseExecution)serverExecution).sendData = sendData;
		
		runBothToCompletion(clientExecution, serverExecution);		
	}
	
	public void testSocketReuseSecondConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);
		assertTrue(((SocketReuseExecution)clientExecution).secondConnectResult);
	}
	
	public void testSocketReuseSecondClientWriteSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);		
		assertEquals(SocketReuseExecution.DATA_LENGTH, ((SocketReuseExecution)clientExecution).writeResult);
	}
	
	public void testSocketReuseSecondServerWriteSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);	
		assertEquals(SocketReuseExecution.DATA_LENGTH, ((SocketReuseExecution)serverExecution).writeResult);
	}
	
	public void testSocketReuseSecondClientReceivedDataCorrect() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);	
		assertTrue(((SocketReuseExecution)clientExecution).verifyReceivedData(data));
	}
	
	public void testSocketReuseSecondServerReceivedDataCorrect() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);	
		assertTrue(((SocketReuseExecution)serverExecution).verifyReceivedData(data));
	}
	
	public void testSocketReuseSecondClientCloseSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);	
		assertTrue(((SocketReuseExecution)clientExecution).secondCloseResult);
	}
	
	public void testSocketReuseSecondServerCloseSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = getRandomBytes(SocketReuseExecution.DATA_LENGTH);
		doSocketReuse(data);		
		assertTrue(((SocketReuseExecution)serverExecution).secondCloseResult);
	}
	
	/**
	 * The client and server open a connection and the client starts writing data, which
	 * the server reads. The initial sequence number is set for the client. The following values are
	 * tested: 
	 * 	 Zero,                (test connect phase)
	 *   MAX_TCP_SEQUENCE     (test connect phase)
	 *   MAX_TCP_SEQUENCE - 1 (test connect phase)
	 *   MAX_TCP_SEQUENCE - 3 (only for read/write test)
	 *
	 * This is test scenario SCN29.
	 */
	private static class SequenceNumberOverflowExecution extends BaseExecution
	{
		private static final int DATA_LENGTH = 1000;
		
		private byte[] sendData;
		private long isn;
		private boolean connectResult = false;
		private boolean acceptReturned = false;
		private int readResult;
		private int writeResult;
		private int bytesToRead;
		
		@Override
		public void executeAsClient()
		{
			socket.setISN(isn);
			connectResult = socket.connect(serverIPAddress, serverPort);
			writeResult = socket.write(sendData, 0, sendData.length);
		}


		@Override
		public void executeAsServer()
		{
			acceptReturned = false;
			socket.accept();
			acceptReturned = true;
			receivedData = new byte[bytesToRead];
			readResult = socket.read(receivedData, 0, bytesToRead);
		}
	}
	
	private void doSequenceNumberOverflow(byte[] sendData, long isn) throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new SequenceNumberOverflowExecution();
		((SequenceNumberOverflowExecution)clientExecution).sendData = sendData;
		((SequenceNumberOverflowExecution)clientExecution).isn = isn;
		serverExecution = new SequenceNumberOverflowExecution();
		((SequenceNumberOverflowExecution)serverExecution).bytesToRead = sendData.length;
		
		runBothToCompletion(clientExecution, serverExecution);			
	}
	
	public void testSequenceNumberOverflowISNEqualsZeroConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, 0L);
		assertTrue(((SequenceNumberOverflowExecution) clientExecution).connectResult);
	}

	public void testSequenceNumberOverflowISNEqualsZeroAcceptReturns() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, 0L);		
		assertTrue(((SequenceNumberOverflowExecution) serverExecution).acceptReturned);
	}
	
	public void testSequenceNumberOverflowISNEqualsMAXConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE);		
		assertTrue(((SequenceNumberOverflowExecution) clientExecution).connectResult);
	}
	
	public void testSequenceNumberOverflowISNEqualsMAXAcceptReturns() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE);		
		assertTrue(((SequenceNumberOverflowExecution) serverExecution).acceptReturned);
	}
	
	public void testSequenceNumberOverflowISNEqualsMAXMinusOneConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE - 1);		
		assertTrue(((SequenceNumberOverflowExecution) clientExecution).connectResult);
	}
	
	public void testSequenceNumberOverflowISNEqualsMAXMinusOneAcceptReturns() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE - 1);		
		assertTrue(((SequenceNumberOverflowExecution) serverExecution).acceptReturned);
	}
	
	public void testSequenceNumberOverflowISNEqualsMaxMinusThreeWriteSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE - 3);	
		assertEquals(SequenceNumberOverflowExecution.DATA_LENGTH, ((SequenceNumberOverflowExecution) clientExecution).writeResult);
	}
	
	public void testSequenceNumberOverflowISNEqualsMaxMinusThreeReadSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE - 3);		
		assertEquals(SequenceNumberOverflowExecution.DATA_LENGTH, ((SequenceNumberOverflowExecution) serverExecution).readResult);
	}
	
	public void testSequenceNumberOverflowISNEqualsMaxMinusThreeReceivedDataCorrect() throws InterruptedException, ExecutionException, IOException
	{
		byte[] data = new byte[SequenceNumberOverflowExecution.DATA_LENGTH];
		doSequenceNumberOverflow(data, TCPPacket.MAX_TCP_SEQUENCE - 3);		
		assertTrue(((SequenceNumberOverflowExecution) serverExecution).verifyReceivedData(data));
	}

	
	/**
	 * The client connects to the accepting server. The client tries to read two packets worth of
	 * data, but the server only sends one and then closes. The client's read call should terminate
	 * prematurely, after which the client closes. This test is used to test the receiving of reply
	 * FIN packets in ESTABLISHED and ACK packets without data in FIN_WAIT_1 and the breaking off of
	 * a read() call. The client
	 * 
	 * This is test scenario SCN27.
	 */
	private static class CloseWhileReadingExecution extends BaseExecution
	{
		private boolean closeResult;
		private int readResult, writeResult;
		private static final int SEND_DATA_LENGTH = TCPPacket.MAX_TCP_DATA;
		private static final int RECEIVE_DATA_LENGTH = TCPPacket.MAX_TCP_DATA * 2;
		private byte[] sendData;
		
		@Override
		public void executeAsClient()
		{
			receivedData = new byte[RECEIVE_DATA_LENGTH];
			socket.connect(serverIPAddress, serverPort);
			readResult = socket.read(receivedData, 0, RECEIVE_DATA_LENGTH);
			closeResult = socket.close();
		}


		@Override
		public void executeAsServer()
		{
			sendData = getRandomBytes(SEND_DATA_LENGTH);
			socket.accept();
			writeResult = socket.write(sendData, 0, SEND_DATA_LENGTH);
			closeResult = socket.close();
		}
	}
	
	
	private boolean doCloseWhileReadingTest() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new CloseWhileReadingExecution();
		serverExecution = new CloseWhileReadingExecution();
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testCloseWhileReadingClientCloseSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWhileReadingTest();
		assertTrue(((CloseWhileReadingExecution)clientExecution).closeResult);
	}
	
	
	public void testCloseWhileReadingServerCloseSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWhileReadingTest();
		assertTrue(((CloseWhileReadingExecution)serverExecution).closeResult);
	}
	
	
	public void testCloseWhileReadingClientReadsOnlyOnePacket() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWhileReadingTest();
		assertEquals(CloseWhileReadingExecution.SEND_DATA_LENGTH, ((CloseWhileReadingExecution)clientExecution).readResult);
	}
	
	
	public void testCloseWhileReadingServerWritesOnePacket() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWhileReadingTest();
		assertEquals(CloseWhileReadingExecution.SEND_DATA_LENGTH, ((CloseWhileReadingExecution)serverExecution).writeResult);
	}
	
	
	public void testCloseWhileReadingClientReadDataEqualServerWriteData() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWhileReadingTest();
		clientExecution.verifyReceivedData(((CloseWhileReadingExecution)serverExecution).sendData, CloseWhileReadingExecution.SEND_DATA_LENGTH);
	}
	
	
	/**
	 * The client connects to the accepting server. The client tries to write, but the server
	 * closes. The client then writes, while the server reads. Finally the client closes as well.
	 * This is used to test receiving ACK packets with data in the FIN_WAIT_2 state.
	 * 
	 * This is scenario SCN30
	 */
	private static class CloseWriteExecution extends BaseExecution
	{
		private boolean closeResult;
		private int readResult, writeResult;
		private static final int DATA_LENGTH = TCPPacket.MAX_TCP_DATA * 3;
		private byte[] sendData;
		
		@Override
		public void executeAsClient()
		{
			receivedData = new byte[DATA_LENGTH];
			sendData = getRandomBytes(DATA_LENGTH);
			
			socket.connect(serverIPAddress, serverPort);
			readResult = socket.read(receivedData, 0, DATA_LENGTH);
			writeResult = socket.write(sendData, 0, DATA_LENGTH);
			closeResult = socket.close();
		}


		@Override
		public void executeAsServer()
		{
			receivedData = new byte[DATA_LENGTH];
			
			socket.accept();
			closeResult = socket.close();
			readResult = socket.read(receivedData, 0, DATA_LENGTH);
		}
	}
	
	
	private void doCloseWrite() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new CloseWriteExecution();
		serverExecution = new CloseWriteExecution();
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testCloseWriteClientReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWrite();
		assertEquals(0, ((CloseWriteExecution)clientExecution).readResult);
	}
	
	
	public void testCloseWriteClientWriteResult() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWrite();
		assertEquals(CloseWriteExecution.DATA_LENGTH, ((CloseWriteExecution)clientExecution).writeResult);
	}
	
	
	public void testCloseWriteServerReadResult() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWrite();
		assertEquals(CloseWriteExecution.DATA_LENGTH, ((CloseWriteExecution)serverExecution).readResult);
	}
	
	
	public void testCloseWriteClientCloseResult() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWrite();
		assertTrue(((CloseWriteExecution)clientExecution).closeResult);
	}
	
	
	public void testCloseWriteServerCloseResult() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWrite();
		assertTrue(((CloseWriteExecution)clientExecution).closeResult);
	}
	
	
	public void testCloseWriteServerReadDataEqualsClientSentData() throws InterruptedException, ExecutionException, IOException
	{
		doCloseWrite();
		serverExecution.verifyReceivedData(((CloseWriteExecution)clientExecution).sendData);
	}
	
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}
}
