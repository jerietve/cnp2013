package nl.vu.cs.cn;

import java.util.Arrays;

import junit.framework.TestCase;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.TCPPacket;
import nl.vu.cs.cn.TCPPacket.FlagSet;

/**
 * This test is very similar to the {@link TCPPacketParseTest}, except here the IP packet is really
 * sent through the IP stack. It is tested whether the TCP packet constructed using the IP packet
 * that comes out the other side has exactly the same values as the TCP packet that went in.
 */
public class TCPPacketSendTest extends TestCase 
{
	private static final int sourcePort = 1234;
	private static final int destinationPort = 5678;
	private static final int sourceAddress = 100;
	private static final int destinationAddress = 1;
	private static final byte[] testData = {5, 32, 47, -114};
	
	private TCPPacket sendTCPPacket, receiveTCPPacket;
	private IP sendIPStack, receiveIPStack;
	private IP.Packet sendIPPacket, receiveIPPacket;
	
	 public TCPPacketSendTest()
	 {
		 super();
	 }
	 
	 
	 @Override
	 public void setUp() throws Exception
	 {
		super.setUp();
		
		/* Initialize the TCPs, IPs, sockets and send TCP packet */
		sendIPStack = new IP(sourceAddress + 1);
		sendTCPPacket = new TCPPacket();
		receiveIPStack = new IP(destinationAddress + 1);
		receiveTCPPacket = new TCPPacket();

		/* Assemble the TCP packet */
		sendTCPPacket.setSource(sendIPStack.getLocalAddress(), sourcePort);
		sendTCPPacket.setDestination(receiveIPStack.getLocalAddress(), destinationPort);
		sendTCPPacket.setData(testData, 0, testData.length);
		sendTCPPacket.setFlags(FlagSet.F_ACK);
		sendTCPPacket.setSequenceNumber(0);
		sendTCPPacket.setAcknowledgmentNumber(0);
		sendTCPPacket.build();
		
		/* Put the TCP packet in an IP packet */
		sendIPPacket = new IP.Packet(receiveIPStack.getLocalAddress().getAddress(), IP.TCP_PROTOCOL, 0, sendTCPPacket.getIPData(),
				sendTCPPacket.getIPDataLength());
		sendIPPacket.source = sendIPStack.getLocalAddress().getAddress();
		receiveIPPacket = new IP.Packet();
		
		/* Send the IP packet through the IP layer */
		sendIPStack.ip_send(sendIPPacket);
		receiveIPStack.ip_receive_timeout(receiveIPPacket, 1);

		/* Parse the IP packet to get a TCP packet out */
		receiveTCPPacket.receiveTCPPacket(receiveIPPacket);
	 }
	
	
	
	@Override
	public void tearDown() throws Exception
	{
		sendTCPPacket = receiveTCPPacket = null;
		sendIPPacket = receiveIPPacket = null;
		sendIPStack = receiveIPStack = null;
		super.tearDown();
	}
	
	
	
	public void testParsedTCPPacketSourcePort()
	{
		assertEquals("The source port should be the one initially provided",
				sourcePort, receiveTCPPacket.getSourcePort());
	}
	
	
	
	public void testParsedTCPPacketDestinationPort()
	{
		assertEquals("The destination port should be the one initially provided", 
				destinationPort, receiveTCPPacket.getDestinationPort());
	}
	
	
	
	public void testParsedTCPPacketSequenceNumber()
	{
		assertEquals("The sequence number for both packets should be equal", 
				sendTCPPacket.getSequenceNumber(), receiveTCPPacket.getSequenceNumber());
	}
	
	
	
	public void testParsedTCPPacketAcknowledgementNumber()
	{
		assertEquals("The acknowledgement number for both packets should be equal", 
				sendTCPPacket.getAcknowledgmentNumber(), receiveTCPPacket.getAcknowledgmentNumber());
	}
	
	
	
	public void testParsedTCPPacketAcknowledgementFlag()
	{
		assertEquals("The acknowledgement flag for both packets should be equal",
				sendTCPPacket.getFlags().hasAck(), receiveTCPPacket.getFlags().hasAck());
	}
	
	
	
	public void testParsedTCPPacketPushFlag()
	{
		assertEquals("The push flag for both packets should be equal",
				sendTCPPacket.hasPsh(), receiveTCPPacket.hasPsh());
	}
	
	
	
	public void testParsedTCPPacketSynchronizeFlag()
	{
		assertEquals("The SYN flag for both packets should be equal",
				sendTCPPacket.getFlags().hasSyn(), receiveTCPPacket.getFlags().hasSyn());
	}
	
	
	
	public void testParsedTCPPacketFinalFlag()
	{
		assertEquals("The FIN flag for both packets should be equal",
				sendTCPPacket.getFlags().hasFin(), receiveTCPPacket.getFlags().hasFin());
	}
	
	
	
	public void testParsedTCPPacketLength()
	{
		/* We need to call build() again to generate ipData, in order to get the total length */
		receiveTCPPacket.build();
		
		assertEquals("The received packet should have the same length as the sent one",
				sendTCPPacket.getIPDataLength(), receiveTCPPacket.getIPDataLength());
	}
	
	
	public void testParsedTCPPacketData()
	{
		/* We need to call build again to generate "ipData" */
		receiveTCPPacket.build();
				
		assertTrue("The received packet should have the same data as the sent one", 
				Arrays.equals(sendTCPPacket.getIPData(), receiveTCPPacket.getIPData()));
	}
}
