package nl.vu.cs.cn.executions;

import java.util.concurrent.Callable;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;

/** 
 * An execution defines how two TCP instances interact over time. Subclasses can be derived in
 * order to test different kinds of interactions. Executions may be configurable in order to make
 * them more versatile. In this case it might be wise to implement multiple test cases per
 * Execution.
 * 
 * Two instances of the same BaseExecution class must be made. One should be initialized as the
 * server and the other must be initialized as the client. Subclasses of BaseExecution can be found
 * in all integration test classes as private static inner classes.
 */
public abstract class BaseExecution implements Callable<Void> 
{
	private boolean isClient;
	protected byte[] receivedData;
	protected Socket socket;
	protected IpAddress serverIPAddress;
	protected int serverPort;
	protected boolean doneExecuting;
	
	
	/**
	 * Initializes this execution object as client
	 * @param socket
	 * @param serverIPAddress
	 * @param serverPort
	 */
	public void initAsClient(Socket socket, IpAddress serverIPAddress, int serverPort)
	{
		this.isClient = true;
		this.socket = socket;
		this.serverPort = serverPort;
		this.serverIPAddress = serverIPAddress;
	}
	
	/**
	 * Initializes this execution object as server
	 * @param socket
	 */
	public void initAsServer(Socket socket)
	{
		this.isClient = false;
		this.socket = socket;
	}
		
	@Override
	public Void call()
	{
		try
		{
			if(isClient) executeAsClient();
			else executeAsServer();
		} 
		catch (RuntimeException e)
		{
			e.printStackTrace();
			throw e;
		}
		return null;
	}
	
	/**
	 * Execute the execution as client.
	 */
	public abstract void executeAsClient();
	
	/**
	 * Execute the execution as server.
	 */
	public abstract void executeAsServer();
	
	
	/**
	 * @return true iff this executions believes to be done executing
	 */
	public boolean isDone()
	{
		return doneExecuting;
	}
	
	/**
	 * Verify the data that should have been received by this execution object
	 * @param expectedReceivedData		The data that the remote end attempted to send to us.
	 * 									NULL must be passed if we expect no data was received
	 * @return							Whether the data matched with what we have received
	 */
	public boolean verifyReceivedData(byte[] expectedReceivedData)
	{
		if ((expectedReceivedData == null) != (receivedData == null) ||
				expectedReceivedData.length != receivedData.length)
				return false;
		
		if (expectedReceivedData != null)
		{
			for (int i=0; i<expectedReceivedData.length; i++)
			{
				if (expectedReceivedData[i] != receivedData[i])	return false;
			}
		}
		
		return true;
	}
	
	
	/**
	 * Checks if the first byteCount bytes of the received data are equal to the first byteCount
	 * bytes in expectedReceivedData
	 */
	public boolean verifyReceivedData(byte[] expectedReceivedData, int byteCount)
	{
		if(byteCount == 0) return true;
		if(expectedReceivedData == null || receivedData == null) return false;
		if(expectedReceivedData.length < byteCount || receivedData.length < byteCount) return false;
		for(int i = 0; i < byteCount; i++)
		{
			if(expectedReceivedData[i]!= receivedData[i] ) return false;
		}
		
		return true;
	}
	
	
	/**
	 * Checks the first byteCount bytes in receivedData for equality against expectedReceivedData
	 * and returns the index of the first one that doesn't match. In case the check cannot be
	 * performed, -1 is returned. In case all bytes correspond, byteCount is returned.
	 */
	public int firstIncorrectlyReceivedByte(byte[] expectedReceivedData, int byteCount)
	{
		if(byteCount == 0) return -1;
		if(expectedReceivedData == null || receivedData == null) return -1;
		if(expectedReceivedData.length < byteCount || receivedData.length < byteCount) return -1;
		for(int i = 0; i < byteCount; i++)
		{
			if(expectedReceivedData[i]!= receivedData[i] ) return i;
		}
		
		return byteCount;
	}
}
