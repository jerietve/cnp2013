package nl.vu.cs.cn;

import java.io.IOException;
import java.util.Random;

import nl.vu.cs.cn.TCPPacket.FlagSet;
import android.test.AndroidTestCase;
import android.util.Log;

/**
 * Corrupt packets in all sorts of ways and make sure the corruption is caught by the method
 * responsible for receiving packets. This testing class is useless until the TCPPacketParseTest
 * passes.
 * 
 * Whenever a packet which has been corrupted is received, it should cause an exception, which is
 * then caught higher up the stack. Here we only test the throwing of the exception. The most likely
 * exception is a ChecksumException, but IllegalTCPFlagsException and IOException are also possible.
 * 
 * Currently, the only corruption done by this class is changing one bit at a time.
 * 
 * This is scenario SCN21
 */
public class TCPPacketCorruptionTest extends AndroidTestCase
{	
	private final FlagSet[] FLAG_SET_VALUES = FlagSet.values();
	private final int NUM_PACKETS = 10;
	private final Random random;
	private final String TAG = "CNP_TCPPacketCorruptionTest";
	
	/* Not really a percentage, just the number of times to corrupt a bit, compared to the number
	 * of bits in the packet. The same bit could possibly be corrupted twice */
	private double CORRUPTION_PERCENTAGE = 0.01;
	/* The random variables needed by multiple methods */
	private TCP sendStack;
	private int receiveIP;
	
	
	public TCPPacketCorruptionTest()
	{
		super();
		random = new Random();
	}
	
	
	public void testReceivingSingleBitCorruptedPacketWithDataThrowsException()
	{
		if(TCP.DEBUG) Log.d("CNP_Test", "Data packet corruption test started");
		/* Try it a couple of times */
		for(int i = 1; i <= NUM_PACKETS; i++)
		{
			if(TCP.DEBUG) Log.d(TAG, "Testing packet " + i);
			TCPPacket tcpPacket = null;
			/* Get a packet with data */
			tcpPacket = createRandomTCPPacket(true);
			/* We now have an IP packet. Corrupt one bit at a time, a number of times! */
			for(int corruption = 0; corruption < (int)(tcpPacket.getIPDataLength() * CORRUPTION_PERCENTAGE); corruption++)
			{
				/* Define which bit we're going to corrupt, then corrupt it */
				int byteIndex = random.nextInt(tcpPacket.getIPDataLength());
				int bitIndex = random.nextInt(8);
				corruptAndCheck(tcpPacket, byteIndex, bitIndex);
			}
		}
		if(TCP.DEBUG) Log.d("CNP_Test", "Data packet corruption test finished");
	}
	
	
	public void testReceivingSingleBitCorruptedPacketWithoutDataThrowsException()
	{
		TCPPacket tcpPacket = null;
		/* Get a packet without data */
		tcpPacket = createRandomTCPPacket(false);
		/* Corrupt all bits separately one by one */
		for(int byteIndex = 0; byteIndex < TCPPacket.TCP_HEADER_NO_OPTIONS_SIZE_BYTES; byteIndex++)
		{
			for(int bitIndex = 0; bitIndex < 8; bitIndex++)
			{
				corruptAndCheck(tcpPacket, byteIndex, bitIndex);
			}
		}
	}


	/**
	 * Corrupts the given bit in the packet, then tries to receive it, making sure an exception is
	 * thrown. If not, fail() is called.
	 * 
	 * @param tcpPacket
	 * @param byteIndex
	 * @param bitIndex
	 */
	private void corruptAndCheck(TCPPacket tcpPacket, int byteIndex, int bitIndex)
	{
		tcpPacket.corrupt(byteIndex, bitIndex);
		/* Now create an IP packet from this TCP packet */
		IP.Packet ipPacket = createIPPacket(tcpPacket);
		/* Try to receive it and make sure it fails */
		TCPPacket receiveTcpPacket = new TCPPacket();
		try
		{
			receiveTcpPacket.receiveTCPPacket(ipPacket);
			/* No exception! Not okay */
			fail("Corrupted packet was accepted! After changing byte " + byteIndex + " bit " + bitIndex + " in " + tcpPacket.toString());
		}
		catch(IOException e)
		{
			/* The header length field was corrupted, and it was caught */
		}
		catch(IllegalTCPFlagsException e)
		{
			/* The flag field corruption resulted in an illegal combination */
		}
		catch(ChecksumException e)
		{
			/* Any other bit was corrupted */
		}
		/* Un-corrupt the bit for the next iteration */
		tcpPacket.corrupt(byteIndex, bitIndex);
	}
	
	
	/**
	 * Creates a TCP packet with random source and destination ports, random but legal flags, random
	 * sequence and acknowledgment numbers and random data of random length, unless withData is set
	 * to false in which case no data is added.
	 * 
	 * @param withData
	 * @return A new TCPPacket
	 */
	private TCPPacket createRandomTCPPacket(boolean withData)
	{
		int dataLength;
		byte[] data;
		if(withData)
		{
			/* Generate an array of random length filled with random bytes */
			dataLength = random.nextInt(TCPPacket.MAX_TCP_DATA);
			data = new byte[dataLength];
			random.nextBytes(data);
		}
		else
		{
			dataLength = 0;
			data = new byte[] {};
		}
		
		/* Generate a random sending and receiving socket (IP address-port combination) */
		int sendIP = random.nextInt(254) + 1;
		int sendPort = random.nextInt(TCPPacket.MAX_TCP_PORT_NR);
		receiveIP = random.nextInt(254) + 1;
		int receivePort = random.nextInt(TCPPacket.MAX_TCP_PORT_NR);
		TCP receiveStack = null;
		try
		{
			sendStack = new TCP(sendIP);
			receiveStack = new TCP(receiveIP);
		}
		catch(IOException e)
		{
			fail("Failed to create TCP!");
		}
		TCPPacket sendPacket = new TCPPacket();
		
		/* Get random legal flags and seq and ack numbers */
		FlagSet flagSet = FLAG_SET_VALUES[random.nextInt(FLAG_SET_VALUES.length - 1) + 1]; // do not end up with INVALID
		long seqNumber = Math.abs(random.nextLong() % TCPPacket.MAX_TCP_SEQUENCE);
		long ackNumber = Math.abs(random.nextLong() % TCPPacket.MAX_TCP_SEQUENCE);
		
		/* Assemble the packet */
		sendPacket.setSource(sendStack.getLocalAddress(), sendPort);
		sendPacket.setDestination(receiveStack.getLocalAddress(), receivePort);
		sendPacket.setData(data, 0, dataLength);
		sendPacket.setFlags(flagSet);
		sendPacket.setSequenceNumber(seqNumber);
		sendPacket.setAcknowledgmentNumber(ackNumber);
		sendPacket.build();
		
		return sendPacket;
	}
	
	
	public IP.Packet createIPPacket(TCPPacket tcpPacket)
	{
		IP.IpAddress receiveIpAddr = IP.IpAddress.getAddress("192.168.0." + receiveIP);
		
		/* Construct a fake IP packet containing the TCP header and data of "sendPacket" */
		IP.Packet ipPacket = new IP.Packet(receiveIpAddr.getAddress(), IP.TCP_PROTOCOL, 0, 
				tcpPacket.getIPData(), tcpPacket.getIPDataLength());
		ipPacket.source = sendStack.getLocalAddress().getAddress();
		
		return ipPacket;
	}
}
