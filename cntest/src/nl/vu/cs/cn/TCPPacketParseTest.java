package nl.vu.cs.cn;

import java.util.Arrays;

import junit.framework.TestCase;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.TCPPacket;
import nl.vu.cs.cn.TCPPacket.FlagSet;

/**
 * This is a test class for the TCPPacket class'
 * {@link TCPPacket#receiveTCPPacket(nl.vu.cs.cn.IP.Packet)} method. A TCP packet is created and
 * wrapped in an IP packet. This IP packet is then used to construct another TCP packet whose values
 * are asserted to be equal to the values the original packet was supposed to have, as much as
 * possible. Some values are asserted by checking against the sent packet, because these values
 * cannot be imposed upon a packet directly, so for these fields it is assumed the original packet
 * has the right values.
 */
public class TCPPacketParseTest extends TestCase 
{
	private static final int sourcePort = 1234;
	private static final int destinationPort = 5678;
	
	private TCP sendStack, receiveStack;
	private TCPPacket sendPacket, receivePacket;
	private IP.Packet ipPacket;
	
	
	public TCPPacketParseTest() 
	{
		super();
	}
	
	
	@Override
	public void setUp() throws Exception
	{
		super.setUp();
		/* Some "signed" bytes to test with */
		byte[] originalData = {5, 32, 47, -114};
		
		/* Generate dummy IP Address (not relevant here) */
		IP.IpAddress dummyIpAddr = IP.IpAddress.getAddress("192.168.0.1");
		
		sendStack = new TCP(100);
		sendPacket = new TCPPacket();
		receiveStack = new TCP(1);
		receivePacket = new TCPPacket();
		
		/* Assemble the packet */
		sendPacket.setSource(sendStack.getLocalAddress(), sourcePort);
		sendPacket.setDestination(receiveStack.getLocalAddress(), destinationPort);
		sendPacket.setData(originalData, 0, originalData.length);
		sendPacket.setFlags(FlagSet.F_ACK);
		sendPacket.setSequenceNumber(0);
		sendPacket.setAcknowledgmentNumber(0);
		sendPacket.build();

		/* Construct a fake IP packet containing the TCP header and data of "sendPacket" */
		ipPacket = new IP.Packet(dummyIpAddr.getAddress(), IP.TCP_PROTOCOL, 0, sendPacket.getIPData(), sendPacket.getIPDataLength());
		ipPacket.source = sendStack.getLocalAddress().getAddress();
		
		/* Parse the packet */
		receivePacket.receiveTCPPacket(ipPacket);
	}
	
	
	
	@Override
	public void tearDown() throws Exception
	{
		sendStack = receiveStack = null;
		sendPacket = receivePacket = null;
		ipPacket = null;
		super.tearDown();
	}
	
	
	
	public void testParsedTCPPacketSourcePort()
	{
		assertEquals("The source port should be the one initially provided",
				sourcePort, receivePacket.getSourcePort());
	}
	
	
	
	public void testParsedTCPPacketDestinationPort()
	{
		assertEquals("The destination port should be the one initially provided", 
				destinationPort, receivePacket.getDestinationPort());
	}
	
	
	
	public void testParsedTCPPacketSequenceNumber()
	{
		assertEquals("The sequence number for both packets should be equal", 
				sendPacket.getSequenceNumber(), receivePacket.getSequenceNumber());
	}
	
	
	
	public void testParsedTCPPacketAcknowledgementNumber()
	{
		assertEquals("The acknowledgement number for both packets should be equal", 
				sendPacket.getAcknowledgmentNumber(), receivePacket.getAcknowledgmentNumber());
	}
	
	
	
	public void testParsedTCPPacketAcknowledgementFlag()
	{
		assertEquals("The acknowledgement flag for both packets should be equal",
				sendPacket.getFlags().hasAck(), receivePacket.getFlags().hasAck());
	}
	
	
	
	public void testParsedTCPPacketPushFlag()
	{
		assertEquals("The push flag for both packets should be equal",
				sendPacket.hasPsh(), receivePacket.hasPsh());
	}
	
	
	
	public void testParsedTCPPacketSynchronizeFlag()
	{
		assertEquals("The SYN flag for both packets should be equal",
				sendPacket.getFlags().hasSyn(), receivePacket.getFlags().hasSyn());
	}
	
	
	
	public void testParsedTCPPacketFinalFlag()
	{
		assertEquals("The FIN flag for both packets should be equal",
				sendPacket.getFlags().hasFin(), receivePacket.getFlags().hasFin());
	}
	
	
	
	public void testParsedTCPPacketLength()
	{
		/* We need to call build() again to generate ipData, in order to get the total length */
		receivePacket.build();
		
		assertEquals("The received packet should have the same length as the sent one",
				sendPacket.getIPDataLength(), receivePacket.getIPDataLength());
	}
	
	
	public void testParsedTCPPacketData()
	{
		/* We need to call build again to generate "ipData" */
		receivePacket.build();
				
		assertTrue("The received packet should have the same data as the sent one", 
				Arrays.equals(sendPacket.getIPData(), receivePacket.getIPData()));
	}
}
