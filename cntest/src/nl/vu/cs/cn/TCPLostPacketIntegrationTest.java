package nl.vu.cs.cn;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import android.util.Log;

import nl.vu.cs.cn.util.Interceptor;
import nl.vu.cs.cn.TCP.Socket;
import nl.vu.cs.cn.TCPPacket.FlagSet;
import nl.vu.cs.cn.executions.BaseExecution;
import nl.vu.cs.cn.test.PacketFlowInspector;

/**
 * This class works the same as most integration tests: First there is a static nested class
 * extending BaseExecution which implements the scenario that the following tests will use. Next
 * follows a method which creates the executions and runs them. Then finally a bunch of tests follow
 * which call this previous method and test variables set by the executions during their run.
 * 
 * First up however are classes extending Interceptor, which take care of the dropping of packets.
 * These are DropInterceptors.
 * 
 * These tests test whether the TCP implementation responds well to lost packets.
 * 
 * We have the following scenarios. Each with different tests. In each scenario one or more packets
 * are dropped. This list describes the scenarios and what is dropped quickly. Please have a look
 * at the test documentation for a much better description.
 * - Test scenario SCN16: Dropping packets during connect
 * 		- single SYN
 * 		- all SYNs but one
 * 		- all SYNs
 * 		- single SYNACK
 * 		- all SYNACKs but one
 * 		- all SYNACKs
 * 		- the first ACK, client does nothing
 * 		- all ACKs but one, client does nothing
 * 		- all ACKs, client does nothing
 * 		- the first ACK, client writes
 * 		- the first ACK, client closes
 * - Test scenario SCN17: Four way close
 * 		- single first-FIN
 * 		- all first-FINs but one
 * 		- all first-FINs
 * 		- single second-FIN
 * 		- all second-FINs but one
 * 		- all second FINs
 * - Test scenario SCN18: Two way close
 * 		- One FIN
 * 		- FIN exchange
 * 		- All FIN exchanges except one
 * 		- All FIN exchanges
 * - Test scenario SCN19: Simple write
 * 		- Single data
 * 		- All but one data
 * 		- All data
 * 		- Single ACK
 * 		- All ACKs
 * - Test scenario SCN20: Simultaneous write
 * 		- Single data
 * 		- All but one data
 * 		- All data
 * 		- Single ACK
 * 		- All ACKs
 *
 */
public class TCPLostPacketIntegrationTest extends TCPIntegrationTest 
{
	private static final String TAG = "CNP_TCPLostPacketIntegrationTest";
	
	/** Allows a test to send this many data packets before we start dropping inbound packets */
	private static final int OUTBOUND_PACKETS_TO_SKIP = 3;
	/** The amount of data to send in the tests so that some packets will be dropped and some won't */
	private static final int DATA_LENGTH = 100000;
	/** A copy of {@link Socket#MAX_TRANSMISSIONS} because we need it so often */
	private static final int MAX_TRANSMISSIONS = Socket.MAX_TRANSMISSIONS;
	
	/**
	 * This interceptor supports some functionality that allows a test to configure it
	 * to drop a specific sequence of OUTBOUND packets after a specific INBOUND packet is received. 
	 * 
	 * Some explanation:
	 * 		TARGET: The target defines a set of outbound packets, some of which we want to drop. 
	 * 
	 * 		TRIGGER: The trigger specifies an inbound packet that "triggers" a sequence of dropped
	 * 		packets of the "target" type. We can only trigger the DropInterceptor once!!
	 * 
	 * 		DROPCOUNT: specifies how many packets should be dropped in one sequence. After
	 * 		"dropcount" packets are dropped, the Interceptor will not drop any more packets, even if
	 * 		the trigger is retriggered. When dropcount is negative, all packets of the target type
	 * 		will be dropped forever.
	 * 
	 * In AbstractDropInterceptor, both the target and the trigger can be defined using the
	 * isTarget() and isTrigger() methods. 
	 */
	private static abstract class AbstractDropInterceptor implements Interceptor
	{
		private int dropCount;	
		private int dropped;
		private boolean triggered;
		
		public AbstractDropInterceptor(int dropCount)
		{
			this.dropCount = dropCount;
			this.dropped = 0;
			this.triggered = false;
		}
		
		/**
		 * When we receive a packet of the trigger specification, start the drop sequence.
		 */
		@Override
		public TCPPacket inboundPacket(TCPPacket in)
		{
			if (isTrigger(in)) startDropSequence();
			return in;
		}	
	
		/* Drop outgoing packets when they meet the target spec and we must drop > 0 packets */
		@Override
		public TCPPacket outboundPacket(TCPPacket out)
		{
			if (isTarget(out)) 
			{
				/* After we are triggered, we will drop packets until we get to dropCount. However,
				 * if dropCount < 0, we will drop forever */
				if(triggered && ((dropped < dropCount) || dropCount < 0))
				{
					dropped++;
					if(TCP.DEBUG) Log.d(TAG, "TARGET PACKET DROPPED(" + dropped + ")!");
					return null;
				}
				else
				{
					if(TCP.DEBUG) Log.d(TAG, "TARGET PACKET DELIVERED!");
					return out;
				}
			}
			else return out;
		}	
		
		/**
		 * Starts the drop sequence if it hasn't been started yet.
		 */
		protected void startDropSequence()
		{
			if (!triggered)
			{
				if(TCP.DEBUG) Log.d(TAG, "DROP INTERCEPTOR IS TRIGGERED!!!!");
				triggered = true;
			}
		}
		
		/* These methods specify which packets are in the "target set" and which packets are
		 * "triggers". */
		protected abstract boolean isTarget(TCPPacket packet);
		protected abstract boolean isTrigger(TCPPacket packet);
	}
	
	/**
	 * An Interceptor with the same behavior as AbstractDropInterceptor. The trigger and target
	 * sets can be easily specified using FlagSets.
	 */
	private static class FlagBasedDropInterceptor extends AbstractDropInterceptor
	{
		private FlagSet targetFlags;
		private FlagSet triggerFlags;
		
		public FlagBasedDropInterceptor(FlagSet targetFlags, FlagSet triggerFlags, int dropCount)
		{
			super(dropCount);
			this.targetFlags = targetFlags;
			this.triggerFlags = triggerFlags;
			
			if (triggerFlags == null) startDropSequence();
		}

		@Override
		protected boolean isTarget(TCPPacket packet)
		{
			if(packet == null) return false;
			return (packet.getFlags() == targetFlags);
		}
		
		@Override
		protected boolean isTrigger(TCPPacket packet)
		{
			if(packet == null) return false;
			return (packet.getFlags() == triggerFlags);
		}
	}	
	
	/**
	 * This interceptor first lets the connection get established, then receives
	 * acknowledgmentsToSkip ACKs before triggering and dropping all subsequent outgoing ACKs
	 * having data
	 *
	 * Target: All outgoing ACKs having data
	 * Trigger: Having received "acknowledgmentsToSkip" number of acknowledgments
	 */
	private static class DataDropInterceptor extends AbstractDropInterceptor
	{
		private int acknowledgmentsToSkip;
		private boolean connectionEstablished;
		
		public DataDropInterceptor(int dropCount, int acknowledgmentsToSkip)
		{
			super(dropCount);
			this.acknowledgmentsToSkip = acknowledgmentsToSkip;
			this.connectionEstablished = false;
			if(acknowledgmentsToSkip == 0) startDropSequence();
		}

		@Override
		protected boolean isTarget(TCPPacket packet)
		{
			return (packet.getFlags() == FlagSet.F_ACK && packet.hasData());
		}
		
		@Override
		protected boolean isTrigger(TCPPacket packet)
		{			
			if(!connectionEstablished)
			{
			    if(packet.getFlags() == FlagSet.F_SYNACK || packet.getFlags() == FlagSet.F_ACK)
			        connectionEstablished = true;
			    return false;
			}
			else 
			{
			    if (packet.getFlags() == FlagSet.F_ACK)
			        return (acknowledgmentsToSkip-- <= 0);
			    else 
			        return false;
			}
		}
	}	
	
	/**
	 * Will drop the first acknowledgments of a data transmission.
	 * 
	 * Target: All outgoing ACKs
	 * Trigger: Having received "dataPacketsToAwait" number of data packets
	 */
	private static class AcknowledgmentDropInterceptor extends AbstractDropInterceptor
	{
		private int dataPacketsToAwait;
		private boolean connectionEstablished;
		private boolean firstAckWasSkipped;
		
		public AcknowledgmentDropInterceptor(int dropCount, int dataPacketsToAwait)
		{
			super(dropCount);
			this.dataPacketsToAwait = dataPacketsToAwait;
			this.connectionEstablished = false;
		}

		@Override
		protected boolean isTarget(TCPPacket packet)
		{
			if (!firstAckWasSkipped)
			{
				/* We didn't skip the first ACK yet, so this must be it! */
				firstAckWasSkipped = true;
				return false;
			}	
			else
				return (packet.getFlags() == FlagSet.F_ACK);
		}
		
		@Override
		protected boolean isTrigger(TCPPacket packet)
		{
			/* This code makes sure that we do not drop the first ACK that is still part of
			 * the three-way-handshake */
			if (!connectionEstablished)
			{
				if (packet.getFlags() == FlagSet.F_SYNACK)
				{
					connectionEstablished = true;
					firstAckWasSkipped = false;
				}
				if (packet.getFlags() == FlagSet.F_ACK) 
				{
					connectionEstablished = true;			
					firstAckWasSkipped = true;
				}
			}
			
			if (packet.getFlags() == FlagSet.F_ACK && packet.hasData()) dataPacketsToAwait--;
			return (dataPacketsToAwait <= 0);
		}	
	}
	

	/**
	 * Client connects to server, while server is accepting(). Used to test lost-packet behavior
	 * during the connect phase.
	 * 
	 * This is used for scenarios SCN16 and SCN26.
	 */
	private static class SimpleConnectExecution extends BaseExecution
	{
		public boolean connectResult;
		
		@Override
		public void executeAsServer()
		{
			socket.accept();
		}
		
		
		@Override
		public void executeAsClient()
		{
			connectResult = socket.connect(serverIPAddress, serverPort);
		}
	};
	
	private boolean doSimpleConnect(Interceptor clientInterceptor, Interceptor clientInspector,	Interceptor serverInterceptor,
			Interceptor serverInspector) throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new SimpleConnectExecution();
		serverExecution = new SimpleConnectExecution();
		addClientInterceptor(clientInterceptor);
//		addClientInterceptor(clientInspector);
		addServerInterceptor(serverInterceptor);
//		addServerInterceptor(serverInspector);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	/**
	 * This is scenario SCN16.
	 */
	private void doSimpleConnectLostSYN(int synsToDrop, boolean failIfTimeout) throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_SYN, null, synsToDrop);
		PacketFlowInspector clientFlowInspector = new PacketFlowInspector(PacketFlowInspector.connectDroppedSYN(synsToDrop));
		PacketFlowInspector serverFlowInspector = new PacketFlowInspector(PacketFlowInspector.acceptDroppedSYN(synsToDrop));
		if (!doSimpleConnect(clientInterceptor, clientFlowInspector, null, serverFlowInspector) && failIfTimeout)
			fail("Test timed out!");
	}
	
	public void testSimpleConnectLostSYNConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doSimpleConnectLostSYN(1, true);
		assertTrue(((SimpleConnectExecution)clientExecution).connectResult);
	}
	
	public void testSimpleConnectLostAllSYNButOneConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		/* This test has been known to need a little more time */
		setMaxRunTime(15000);
		doSimpleConnectLostSYN(MAX_TRANSMISSIONS - 1, true);
		assertTrue(((SimpleConnectExecution)clientExecution).connectResult);
	}
	
	public void testSimpleConnectLostAllSYNConnectFails() throws InterruptedException, ExecutionException, IOException
	{
		doSimpleConnectLostSYN(MAX_TRANSMISSIONS, false);
		assertFalse(((SimpleConnectExecution)clientExecution).connectResult);
	}
	
	/**
	 * This is scenario SCN26.
	 */
	private void doSimpleConnectLostSYNACK(int synacksToDrop, boolean failIfTimeout)
			throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_SYNACK, null, synacksToDrop);
		PacketFlowInspector clientFlowInspector = new PacketFlowInspector(PacketFlowInspector.connectDroppedSYNACK(synacksToDrop));
		PacketFlowInspector serverFlowInspector = new PacketFlowInspector(PacketFlowInspector.acceptDroppedSYNACK(synacksToDrop));
		if (!doSimpleConnect(null, clientFlowInspector, serverInterceptor, serverFlowInspector) && failIfTimeout)
			fail("Test timed out!");
	}
	
	public void testSimpleConnectLostSYNACKConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doSimpleConnectLostSYNACK(1, true);
		assertTrue(((SimpleConnectExecution)clientExecution).connectResult);
	}
	
	
	public void testSimpleConnectLostAllSYNACKButOneConnectSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doSimpleConnectLostSYNACK(MAX_TRANSMISSIONS - 1, true);
		assertTrue(((SimpleConnectExecution)clientExecution).connectResult);
	}
	
	
	public void testSimpleConnectLostAllSYNACKConnectFails() throws InterruptedException, ExecutionException, IOException
	{
		doSimpleConnectLostSYNACK(-1, false);
		assertFalse(((SimpleConnectExecution)clientExecution).connectResult);
	}
	

	/**
	 * Client connects to server, while server is accepting(). Used to test lost-packet behavior
	 * during the connect phase. This modified version of the SimpleConnectExecution lets the client
	 * stay awake for a given amount of time, to make sure the SocketThread still exists to reply
	 * to SYNACKs coming in after its connect() call already succeeded.
	 * 
	 * This is scenario SCN23.
	 */
	private static class ConnectReadExecution extends BaseExecution
	{
		private boolean connectResult;
		private byte[] sendData = {42};
		
		@Override
		public void executeAsServer()
		{
			socket.accept();
			socket.write(sendData, 0, 1);
		}
		
		
		@Override
		public void executeAsClient()
		{
			receivedData = new byte[1];
			connectResult = socket.connect(serverIPAddress, serverPort);
			socket.read(receivedData, 0, 1);
		}
	};
	
	
	/**
	 * @return false iff the test timed out
	 */
	private boolean doSimpleConnectLostACK(int acksToDrop) throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ConnectReadExecution();
		serverExecution = new ConnectReadExecution();
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_ACK, null, acksToDrop);
		addClientInterceptor(clientInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testConnectLostACKConnectSucceedsIfClientReads() throws InterruptedException, ExecutionException, IOException
	{
		doSimpleConnectLostACK(1);
		assertTrue("connect() should succeed", ((ConnectReadExecution)clientExecution).connectResult);
	}
	
	
	public void testConnectLostACKAcceptSucceedsIfClientReads() throws InterruptedException, ExecutionException, IOException
	{
		assertTrue("accept() should succeed", doSimpleConnectLostACK(1));
	}
	
	
	public void testConnectLostAllButOneACKIfClientReads() throws InterruptedException, ExecutionException, IOException
	{
		assertTrue("accept() should succeed", doSimpleConnectLostACK(MAX_TRANSMISSIONS - 1));
		assertTrue("connect() should succeed", ((ConnectReadExecution)clientExecution).connectResult);
	}
	
	
	public void testConnectLostAllACKIfClientReads() throws InterruptedException, ExecutionException, IOException
	{
		assertFalse("accept() should fail", doSimpleConnectLostACK(MAX_TRANSMISSIONS));
		assertTrue("connect() should succeed", ((ConnectReadExecution)clientExecution).connectResult);
	}
	
	
	/**
	 * This is SCN24, used for the tests of dropping the ACK packet of the three-way connect
	 * handshake, after which the client immediately starts writing.
	 */
	private static class ConnectWriteExecution extends BaseExecution
	{
		private boolean connectResult;
		private int writeResult;
		private byte[] sendData;
		private static final int writeSize = TCPPacket.MAX_TCP_DATA * 2;
		
		@Override
		public void executeAsServer()
		{
			receivedData = new byte[writeSize];
			socket.accept();
			socket.read(receivedData, 0, writeSize);
		}
		
		
		@Override
		public void executeAsClient()
		{
			sendData = getRandomBytes(writeSize);
			connectResult = socket.connect(serverIPAddress, serverPort);
			writeResult = socket.write(sendData, 0, writeSize);
		}
	};
	
	
	/**
	 * Uses the ConnectWriteExecution and drops the first ACK in the three-way connect handshake.
	 */
	private boolean doLostHandshakeACKClientWrites() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ConnectWriteExecution();
		serverExecution = new ConnectWriteExecution();
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_ACK, null, 1);
		addClientInterceptor(clientInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testLostHandshakeACKConnectSucceedsIfClientWrites() throws InterruptedException, ExecutionException, IOException
	{
		doLostHandshakeACKClientWrites();
		assertTrue(((ConnectWriteExecution)clientExecution).connectResult);
	}
	
	
	public void testLostHandshakeACKAcceptSucceedsIfClientWrites() throws InterruptedException, ExecutionException, IOException
	{
		assertTrue("Test should not time out", doLostHandshakeACKClientWrites());
	}
	
	
	public void testLostHandshakeACKWriteSucceedsIfClientWrites() throws InterruptedException, ExecutionException, IOException
	{
		doLostHandshakeACKClientWrites();
		assertEquals("client.write() should have written all it tried to write",
				ConnectWriteExecution.writeSize, ((ConnectWriteExecution)clientExecution).writeResult);
	}
	
	
	public void testLostHandshakeACKReadSucceedsIfClientWrites() throws InterruptedException, ExecutionException, IOException
	{
		doLostHandshakeACKClientWrites();
		assertTrue("server.read() should have read all the client was supposed to write",
				serverExecution.verifyReceivedData(((ConnectWriteExecution)clientExecution).sendData));
	}
	
	
	/**
	 * This is SCN24, used for the tests of dropping the ACK packet of the three-way connect
	 * handshake, after which the client immediately starts writing.
	 */
	private static class ConnectCloseExecution extends BaseExecution
	{
		private boolean connectResult, closeResult;
		
		@Override
		public void executeAsServer()
		{
			socket.accept();
			closeResult = socket.close();
		}
		
		
		@Override
		public void executeAsClient()
		{
			connectResult = socket.connect(serverIPAddress, serverPort);
			closeResult = socket.close();
		}
	};
	
	
	/**
	 * Uses the ConnectCloseExecution and drops the first ACK in the three-way connect handshake.
	 */
	private boolean doLostHandshakeACKClientCloses() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ConnectCloseExecution();
		serverExecution = new ConnectCloseExecution();
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_ACK, null, 1);
		addClientInterceptor(clientInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testLostHandshakeACKConnectSucceedsIfClientCloses() throws InterruptedException, ExecutionException, IOException
	{
		doLostHandshakeACKClientCloses();
		assertTrue(((ConnectCloseExecution)clientExecution).connectResult);
	}
	
	
	public void testLostHandshakeACKClientClosesFinishesQuickly() throws InterruptedException, ExecutionException, IOException
	{
		/* Set a low timeout and check for timeout */
		setMaxRunTime(2000);
		assertTrue(doLostHandshakeACKClientCloses());
	}
	
	
	public void testLostHandshakeACKClientCloseSucceedsIfClientCloses() throws InterruptedException, ExecutionException, IOException
	{
		doLostHandshakeACKClientCloses();
		assertTrue(((ConnectCloseExecution)clientExecution).closeResult);
	}
	
	
	public void testLostHandshakeACKServerCloseSucceedsIfClientCloses() throws InterruptedException, ExecutionException, IOException
	{
		doLostHandshakeACKClientCloses();
		assertTrue(((ConnectCloseExecution)serverExecution).closeResult);
	}
	
	
	/**
	 * The client connects, writes a single byte and closes. The server connects and closes and
	 * then reads. This makes sure that the close() of the server overlaps with the write() of
	 * the client, resulting in a normal 4-way close.
	 * 
	 * The tests refer to "first FINs" and "second FINs". The first fins are those sent by the
	 * server's call to close (since that is the first one), while second FINs are generated by
	 * the client calling close().
	 * 
	 * This is scenario SCN17.
	 */
	private static class FourWayCloseExecution extends BaseExecution
	{
		private boolean closeResult;
		
		@Override
		public void executeAsServer()
		{
			byte[] buf = new byte[1];
			socket.accept();
			closeResult = socket.close();
			if (socket.read(buf, 0, 1) == -1)
				 fail("Read returned -1");
		}
		
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			byte[] buf = {42};
			/* Write() should succeed because close() MUST buffer and ACK all data that it receives */
			if (socket.write(buf, 0, 1) == -1)
				 fail("Write returned -1");			
			closeResult = socket.close();
		}
	};
	
	private boolean doFourWayClose(Interceptor clientInterceptor, Interceptor serverInterceptor) 
			throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new FourWayCloseExecution();
		serverExecution = new FourWayCloseExecution();
		addClientInterceptor(clientInterceptor);
		addServerInterceptor(serverInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	public void testFourWayCloseLostFirstFINCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, 1);
		if (!doFourWayClose(null, serverInterceptor))
			fail("Test timed out!");
		assertTrue(((FourWayCloseExecution)serverExecution).closeResult);		
	}
	
	public void testFourWayCloseLostAllFirstFINsButOneCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS - 1);
		if (!doFourWayClose(null, serverInterceptor))
			fail("Test timed out!");
		assertTrue(((FourWayCloseExecution)serverExecution).closeResult);			
	}
	
	public void testFourWayCloseLostAllFirstFINsCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS);
		if(doFourWayClose(null, serverInterceptor))
			fail("Test didn't time out!");
		assertTrue(((FourWayCloseExecution)serverExecution).closeResult);			
	}
	
	public void testFourWayCloseLostSecondFINCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, 1);
		if (!doFourWayClose(clientInterceptor, null))
			fail("Test timed out!");
		assertTrue(((FourWayCloseExecution)serverExecution).closeResult);
	}
	
	public void testFourWayCloseLostAllSecondFINsButOneCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS - 1);
		if (!doFourWayClose(clientInterceptor, null))
			fail("Test timed out!");
		assertTrue(((FourWayCloseExecution)serverExecution).closeResult);		
	}
	
	public void testFourWayCloseLostAllSecondFINsCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS);
		if(doFourWayClose(clientInterceptor, null))
			fail("Test didn't time out!");
		assertTrue(((FourWayCloseExecution)serverExecution).closeResult);
	}
	
	/**
	 * Client and server connect, then do simultaneous close. The simultaneity comes from the fact
	 * that no implicit receiving of packets is done. Calling close() after connect() or accept()
	 * without any other calls will make sure no packets are read from the IP stack, so the one
	 * waiting for both connect() calls will be a FIN packet.
	 * 
	 * Because the situation is symmetric, it is sufficient to only test the close() return value 
	 * at the client side.
	 * 
	 * This is scenario SCN18.
	 */
	private static class SimultaneousCloseExecution extends BaseExecution
	{
		private boolean closeResult;
		
		@Override
		public void executeAsServer()
		{
			socket.accept();
			closeResult = socket.close();
		}
		
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			closeResult = socket.close();
		}
	};
	
	private boolean doSimultaneousClose(Interceptor clientInterceptor, Interceptor serverInterceptor) 
			throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new SimultaneousCloseExecution();
		serverExecution = new SimultaneousCloseExecution();
		addClientInterceptor(clientInterceptor);
		addServerInterceptor(serverInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testSimultaneousCloseSingleFINLostCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, 1);
		if (!doSimultaneousClose(clientInterceptor, null))
			fail("Test timed out!");
		assertTrue(((SimultaneousCloseExecution)clientExecution).closeResult);		
	}
	
	public void testSimultaneousCloseFirstFINExchangeLostCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, 1);
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, 1);
		if (!doSimultaneousClose(clientInterceptor, serverInterceptor))
			fail("Test timed out!");
		assertTrue(((SimultaneousCloseExecution)clientExecution).closeResult);		
	}
	
	public void testSimultaneousCloseAllFINExchangesLostButOneCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS - 1);
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS - 1);
		if (!doSimultaneousClose(clientInterceptor, serverInterceptor))
			fail("Test timed out!");
		assertTrue(((SimultaneousCloseExecution)clientExecution).closeResult);		
	}
	
	public void testSimultaneousCloseAllFINExchangesLostCloseSucceeds()  throws InterruptedException, ExecutionException, IOException
	{
		FlagBasedDropInterceptor clientInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS);
		FlagBasedDropInterceptor serverInterceptor = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, MAX_TRANSMISSIONS);
		if(!doSimultaneousClose(clientInterceptor, serverInterceptor)) fail("Test timed out!");
		assertTrue(((SimultaneousCloseExecution)clientExecution).closeResult);		
	}
	
	
	/**
	 * The client connects to the accepting server. The server writes a certain amount to the
	 * reading client. The client is used to verify that the data it received is exactly the data
	 * given to the server to send. The connection is not closed.
	 * 
	 * This is scenario SCN19.
	 */
	private static class SingleWriteExecution extends BaseExecution
	{
		private byte[] sendData;
		private int numBytesToRead;
		public int writeResult;

		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			int numBytesReceived = 0;
			receivedData = new byte[numBytesToRead];
			while(numBytesReceived < receivedData.length)
			{
				numBytesReceived += socket.read(receivedData, numBytesReceived, receivedData.length - numBytesReceived);
			}
		}


		@Override
		public void executeAsServer()
		{
			socket.accept();
			writeResult = socket.write(sendData, 0, sendData.length);
		}
	}	
	
	private boolean doSingleWrite(byte[] sendData, Interceptor clientInterceptor, Interceptor serverInterceptor) 
		throws InterruptedException, ExecutionException, IOException
	{
		/* Create the client thread, which will connect and read */
		clientExecution = new SingleWriteExecution();
		((SingleWriteExecution)clientExecution).numBytesToRead = sendData.length;
		
		/* Create the server thread, which will accept and write */
		serverExecution = new SingleWriteExecution();
		((SingleWriteExecution)serverExecution).sendData = sendData;

		addClientInterceptor(clientInterceptor);
		addServerInterceptor(serverInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	/* Lost data packet tests.
	 * 
	 * The server will start dropping outbound packets after having received
	 * OUTBOUND_PACKETS_TO_SKIP acknowledgments */
	
	public void testSingleWriteSingleDataLostIsTransmissionOK() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
		DataDropInterceptor serverInterceptor = new DataDropInterceptor(1, OUTBOUND_PACKETS_TO_SKIP);
		if (!doSingleWrite(serverSendData, null, serverInterceptor))
			fail("Test timed out!");
		assertTrue(clientExecution.verifyReceivedData(serverSendData));
	}
	
	public void testSingleWriteAllButOneDataLostIsTransmissionOK() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
		DataDropInterceptor serverInterceptor = new DataDropInterceptor(MAX_TRANSMISSIONS - 1, OUTBOUND_PACKETS_TO_SKIP);
		/* This test has a habit of timing out */
		setMaxRunTime(15000);
		if (!doSingleWrite(serverSendData, null, serverInterceptor))
			fail("Test timed out!");
		assertTrue(clientExecution.verifyReceivedData(serverSendData));
	}

	
	/* Do not "skip" any packets here. We will just drop all acknowledgments to make sure that write
	 * fails with -1 (since no data was transmitted). */
	public void testSingleWriteAllDataLostCausedWriteToFail() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
		DataDropInterceptor serverInterceptor = new DataDropInterceptor(MAX_TRANSMISSIONS, 0);
		doSingleWrite(serverSendData, null, serverInterceptor);
		assertEquals(-1, ((SingleWriteExecution)serverExecution).writeResult);
	}
	
	/* Lost ACK tests 
	 * 
	 * The client will start dropping acknowledgments after having received OUTBOUND_PACKETS_TO_SKIP
	 * data packets */
	
	public void testSingleWriteSingleACKLostIsTransmissionOK() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
		AcknowledgmentDropInterceptor clientInterceptor = new AcknowledgmentDropInterceptor(1, OUTBOUND_PACKETS_TO_SKIP);
		if (!doSingleWrite(serverSendData, clientInterceptor, null))
			fail("Test timed out!");
		assertTrue(clientExecution.verifyReceivedData(serverSendData));
	}
	
	/**
	 * This test no longer works because of the nondeterministic behavior of simultaneous transfers
	 * with lost packets. Because the ACKs to be dropped by the client might either be combined with
	 * data packets or not, it's impossible to know how many should be dropped.
	 */
//	public void testSingleWriteAllButOneACKLost() throws InterruptedException, ExecutionException, IOException
//	{
//		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
//		AcknowledgmentDropInterceptor clientInterceptor = new AcknowledgmentDropInterceptor(MAX_TRANSMISSIONS - 1,
//				OUTBOUND_PACKETS_TO_SKIP);
//		/* This test *sometimes* takes a little longer for some reason */
//		setMaxRunTime(25000);
//		if (!doSingleWrite(serverSendData, clientInterceptor, null))
//			fail("Test timed out!");
//		assertTrue(clientExecution.verifyReceivedData(serverSendData));
//	}

	/* Do not "skip" any packets here. We will just drop all acknowledgments to make sure
	 * that write fails with -1 (since no data was transmitted). */
	public void testSingleWriteAllACKLostCausedWriteToFail() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
		AcknowledgmentDropInterceptor clientInterceptor = new AcknowledgmentDropInterceptor(MAX_TRANSMISSIONS, 0);
		doSingleWrite(serverSendData, clientInterceptor, null);
		assertEquals(-1, ((SingleWriteExecution)serverExecution).writeResult);
	} 
	
	
	/**
	 * Client and server connect. Both write a large, equal, amount of different bytes to each
	 * other. All this data should be buffered by the write call on the other side. Then read is
	 * called three times to get the data from the buffers. First only one buffer is needed, then
	 * multiple, and finally all data should be gotten from the buffers without requiring the
	 * receiving of another packet.
	 * 
	 * This is scenario SCN20.
	 */
	private static class SimultaneousWriteExecution extends BaseExecution
	{
		private byte[] sendData;
		private int writeResult;
		private int bytesRead;
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			readAndWrite();
			doneExecuting = true;
		}

		@Override
		public void executeAsServer()
		{
			socket.accept();
			readAndWrite();
			doneExecuting = true;
		}
		
		
		private void readAndWrite()
		{
			receivedData = new byte[sendData.length];
			
			writeResult = socket.write(sendData, 0, sendData.length);
			bytesRead = socket.read(receivedData, 0, sendData.length);
		}
	}	
	
	private boolean doSimultaneousWrite(byte[] clientSendData, byte[] serverSendData, Interceptor clientInterceptor,
			Interceptor serverInterceptor) throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new SimultaneousWriteExecution();
		((SimultaneousWriteExecution)clientExecution).sendData = clientSendData;
		
		serverExecution = new SimultaneousWriteExecution();
		((SimultaneousWriteExecution)serverExecution).sendData = serverSendData;

		addClientInterceptor(clientInterceptor);
		addServerInterceptor(serverInterceptor);
		return runBothToCompletion(clientExecution, serverExecution);
	}	
	
	
	/* Lost data packet tests.
	 * 
	 * The client will start dropping outbound data packets after having received
	 * OUTBOUND_PACKETS_TO_SKIP acknowledgments. The client does not receive ACKs in this case, so
	 * it should retransmit the data packets. We should therefore test whether the received data at
	 * the server side is OK. */
	
	public void testSimultaneousWriteSingleDataLostIsTransmissionOK() throws InterruptedException, ExecutionException, IOException
	{
		byte[] clientSendData = getRandomBytes(DATA_LENGTH);
		DataDropInterceptor clientInterceptor = new DataDropInterceptor(1, OUTBOUND_PACKETS_TO_SKIP);
		if (!doSimultaneousWrite(clientSendData, getRandomBytes(100000), clientInterceptor, null))
			fail("Test timed out!");
		assertTrue(serverExecution.verifyReceivedData(clientSendData));
	}
	
	public void testSimultaneousWriteAllButOneDataLostIsTransmissionOK() throws InterruptedException, ExecutionException, IOException
	{
		byte[] clientSendData = getRandomBytes(DATA_LENGTH);
		DataDropInterceptor clientInterceptor = new DataDropInterceptor(MAX_TRANSMISSIONS - 1, OUTBOUND_PACKETS_TO_SKIP);
		/* This test has a habit of timing out */
		setMaxRunTime(15000);
		if (!doSimultaneousWrite(clientSendData, getRandomBytes(DATA_LENGTH), clientInterceptor, null))
			fail("Test timed out!");
		assertTrue(serverExecution.verifyReceivedData(clientSendData));
	}
	
	
	/**
	 * Here the client and server send data to each other at the same time. They both ACK this data
	 * at the same time. The client has now received two ACK packets (one with, one without data).
	 * They both send each other data packets again, and when the data packet from the server
	 * arrives at the client, its trigger goes off. Every next data packet from the client to the
	 * server will be lost. The writing from the server to the client, however, works perfectly fine
	 * because the client does not drop any empty ACK packets. The client will keep on retrying to
	 * send its third packet 10 times, but all attempts will fail.
	 */
	public void testSimultaneousWriteAllDataLostReceivedCorrectNumberOfBytes() throws InterruptedException, ExecutionException, IOException
	{
		byte[] clientSendData = getRandomBytes(DATA_LENGTH);
		DataDropInterceptor clientInterceptor = new DataDropInterceptor(MAX_TRANSMISSIONS, 3);
		doSimultaneousWrite(clientSendData, getRandomBytes(DATA_LENGTH), clientInterceptor, null);
		assertEquals(TCPPacket.MAX_TCP_DATA * 2, ((SimultaneousWriteExecution)clientExecution).writeResult);
	}
	
	
	/* Lost ACK tests.
	 * 
	 * The server will start dropping acknowledgments after having received
	 * OUTBOUND_PACKETS_TO_SKIP data packets. If not all ACKs to a data packet are dropped, the
	 * client should receive one and retransmit. In this case all data should arrive at the server
	 * okay. */
	
	public void testSimultaneousWriteSingleACKLostIsTransmissionOK() throws InterruptedException, ExecutionException, IOException
	{
		byte[] clientSendData = getRandomBytes(DATA_LENGTH);
		AcknowledgmentDropInterceptor serverInterceptor = new AcknowledgmentDropInterceptor(1, OUTBOUND_PACKETS_TO_SKIP);
		if (!doSimultaneousWrite(clientSendData, getRandomBytes(DATA_LENGTH), null, serverInterceptor))
			fail("Test timed out!");
		assertTrue(serverExecution.verifyReceivedData(clientSendData));
	}
	
	/**
	 * The test for dropping all but one of the ACKs to the client's data can no longer work,
	 * because this is a nondeterministic situation. The client will send a packet to the server,
	 * and the server drops the reply. The server then also drops its own simultaneous data packet
	 * to the client. Then around one second later both the client and server decide to retransmit
	 * their data packets. If the server's read-from-network timeout, 1 second, times out right
	 * before it receives the retransmission from the client, the server will first send its own
	 * retransmission and then the ACK, resulting in two packets to drop. If, however, the client's
	 * retransmission comes in right before the IP layer timeout expires, the server will ACK the
	 * client's data in the same packet it uses to transmit its own retransmission. This results in
	 * only one packet to drop. Because the AcknowledgmentDropInterceptor can only drop a fixed
	 * number of packets, doing this test has become impossible.
	 */
//	public void testSimultaneousWriteAllButOneACKLost() throws InterruptedException, ExecutionException, IOException
//	{
//		byte[] clientSendData = getRandomBytes(DATA_LENGTH);
//		AcknowledgmentDropInterceptor serverInterceptor = new AcknowledgmentDropInterceptor((MAX_TRANSMISSIONS - 1) * 2, OUTBOUND_PACKETS_TO_SKIP);
//		if (doSimultaneousWrite(clientSendData, getRandomBytes(DATA_LENGTH), null, serverInterceptor))
//			fail("Test timed out!");
//		assertTrue(serverExecution.verifyReceivedData(clientSendData));
//	}

	/**
	 * The client writes to the server. The server counts 1, 2, 3 data packets and then drops the
	 * ACK to the third data packet. Any further replies and data packets from the server, however
	 * many there may be (see JavaDoc above
	 * {@link TCPLostPacketIntegrationTest#testSimultaneousWriteAllButOneACKLost}), are dropped. Only the first
	 * two packets sent from the client to the server are confirmed to have arrived, so the amount
	 * write returns is the size of two full packets. The server has received confirmation for three
	 * of its packets, and these are also the only three which have arrived. However, both the
	 * client and the server are blocked in the read() call, so they wouldn't normally know they had
	 * received this data until the connection was closed. To simulate this, we interrupt the
	 * threads. In this case too read() must return how much it has read thus far.
	 */
	public void testSimultaneousWriteAllACKLostMultipleChecks() throws InterruptedException, ExecutionException, IOException
	{
		byte[] serverSendData = getRandomBytes(DATA_LENGTH);
		byte[] clientSendData = getRandomBytes(DATA_LENGTH);
		AcknowledgmentDropInterceptor serverInterceptor = new AcknowledgmentDropInterceptor(MAX_TRANSMISSIONS * 2, 3);
		doSimultaneousWrite(clientSendData, serverSendData, null, serverInterceptor);
		assertEquals("Client has successfully written two full packets",
				TCPPacket.MAX_TCP_DATA * 2,	((SimultaneousWriteExecution)clientExecution).writeResult);
		assertTrue("Server has successfully received three full packets",
				serverExecution.verifyReceivedData(clientSendData, TCPPacket.MAX_TCP_DATA * 2));
		assertEquals("Server read() shows it has received three packets upon interrupt",
				TCPPacket.MAX_TCP_DATA * 3, ((SimultaneousWriteExecution)serverExecution).bytesRead);
		assertEquals("Server has successfully written three full packets",
				TCPPacket.MAX_TCP_DATA * 3,	((SimultaneousWriteExecution)serverExecution).writeResult);
		assertTrue("Client has successfully received three full packets",
				clientExecution.verifyReceivedData(serverSendData, TCPPacket.MAX_TCP_DATA * 3));
		assertEquals("Client read() shows it has received three packets upon interrupt",
				TCPPacket.MAX_TCP_DATA * 3, ((SimultaneousWriteExecution)clientExecution).bytesRead);
	}
	
	
	/**
	 * This scenario uses the simultaneous close execution. It drops packets in an intricate way
	 * however, dropping both the FIN sent by the server and the ACK sent by the server as a reply
	 * to the first FIN it receives from the client. This results in the three way close scenario,
	 * where the client does the special step from FIN_WAIT_1 immediately to TIME_WAIT: a three
	 * way close.
	 */
	private void doThreeWayClose() throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new SimultaneousCloseExecution();
		serverExecution = new SimultaneousCloseExecution();
		FlagBasedDropInterceptor finDropper = new FlagBasedDropInterceptor(FlagSet.F_FINACK, null, 1);
		FlagBasedDropInterceptor ackDropper = new FlagBasedDropInterceptor(FlagSet.F_ACK, null, 1);
		addServerInterceptor(finDropper);
		addServerInterceptor(ackDropper);
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testThreeWayCloseClientCloseSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doThreeWayClose();
		assertTrue(((SimultaneousCloseExecution)clientExecution).closeResult);
	}
	
	
	public void testThreeWayCloseServerCloseSucceeds() throws InterruptedException, ExecutionException, IOException
	{
		doThreeWayClose();
		assertTrue(((SimultaneousCloseExecution)serverExecution).closeResult);
	}
	
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}
}
