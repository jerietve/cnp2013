package nl.vu.cs.cn;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import nl.vu.cs.cn.IP.IpAddress;
import nl.vu.cs.cn.TCP.Socket;

import android.test.AndroidTestCase;

/**
 * This class tests that the TCP class and its subclasses have exactly the public methods they are
 * supposed to. For all methods that should be there it is checked whether they exist (by name and
 * parameter types), if they have the correct return type, if they throw the right exceptions and if
 * they have the right modifiers. Finally it is checked that no methods which are not on the list
 * exist with the public modifier.
 */
public class TCPInterfaceTest extends AndroidTestCase
{
	/* TCP class */
	
	/* public TCP(int address) throws IOException */
	
	public void testTCPConstructorExists() throws NoSuchMethodException
	{
		TCP.class.getConstructor(int.class);
	}
	
	
	public void testTCPConstructorThrowsIOException() throws NoSuchMethodException
	{
		Constructor<TCP> constructor = TCP.class.getConstructor(int.class);
		Class<?>[] exceptions = constructor.getExceptionTypes();
		assertEquals("public TCP(int address) should throw one kind of exception", 1, exceptions.length);
		assertEquals("public TCP(int address) should throw IOException", IOException.class, exceptions[0]);
	}
	
	
	public void testTCPConstructorIsPublic() throws NoSuchMethodException
	{
		Constructor<TCP> constructor = TCP.class.getConstructor(int.class);
		int modifiers = constructor.getModifiers();
		assertEquals(Modifier.PUBLIC, modifiers);
	}
	
	/* public Socket socket() */
	
	public void testTCPSocketNoParamMethodExists() throws NoSuchMethodException
	{
		TCP.class.getMethod("socket");
	}
	
	
	public void testTCPSocketNoParamMethodReturnsSocket() throws NoSuchMethodException
	{
		Method socketMethod = TCP.class.getMethod("socket");
		assertEquals(Socket.class, socketMethod.getReturnType());
	}
	
	
	public void testTCPSocketNoParamMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method socketMethod = TCP.class.getMethod("socket");
		Class<?>[] exceptions = socketMethod.getExceptionTypes();
		assertEquals("public Socket socket() should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testTCPSocketNoParamMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(TCP.class.getMethod("socket"));
	}
	
	/* public Socket socket(int port) */
	
	public void testTCPSocketIntMethodExists() throws NoSuchMethodException
	{
		TCP.class.getMethod("socket", int.class);
	}
	
	
	public void testTCPSoscketIntMethodReturnsSocket()throws NoSuchMethodException
	{
		Method socketMethod = TCP.class.getMethod("socket", int.class);
		assertEquals(Socket.class, socketMethod.getReturnType());
	}
	
	
	public void testTCPSocketIntMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method socketMethod = TCP.class.getMethod("socket", int.class);
		Class<?>[] exceptions = socketMethod.getExceptionTypes();
		assertEquals("public Socket socket() should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testTCPSocketIntMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(TCP.class.getMethod("socket", int.class));
	}
	
	/* TCP.Socket inner class */
	
	/* public boolean connect(IpAddress dst, int port) */
	
	public void testSocketConnectMethodExists() throws NoSuchMethodException
	{
		Socket.class.getMethod("connect", IpAddress.class, int.class);
	}
	
	
	public void testSocketConnectMethodReturnsBoolean() throws NoSuchMethodException
	{
		Method connectMethod = Socket.class.getMethod("connect", IpAddress.class, int.class);
		assertEquals(boolean.class, connectMethod.getReturnType());
	}
	
	
	public void testSocketConnectMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method connectMethod = Socket.class.getMethod("connect", IpAddress.class, int.class);
		Class<?>[] exceptions = connectMethod.getExceptionTypes();
		assertEquals("public boolean connect(IpAddress dst, int port) should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testSocketConnectMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(Socket.class.getMethod("connect", IpAddress.class, int.class));
	}
	
	/* public void accept() */
	
	public void testSocketAcceptMethodExists() throws NoSuchMethodException
	{
		Socket.class.getMethod("accept");
	}
	
	
	public void testSocketAcceptMethodIsVoid() throws NoSuchMethodException
	{
		Method acceptMethod = Socket.class.getMethod("accept");
		assertEquals(void.class, acceptMethod.getReturnType());
	}
	
	
	public void testSocketAcceptMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method acceptMethod = Socket.class.getMethod("accept");
		Class<?>[] exceptions = acceptMethod.getExceptionTypes();
		assertEquals("public void accept() should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testSocketAcceptMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(Socket.class.getMethod("accept"));
	}
	
	/* public int read(byte[] buf, int offset, int maxlen) */
	
	public void testSocketReadMethodExists() throws NoSuchMethodException
	{
		Socket.class.getMethod("read", byte[].class, int.class, int.class);
	}
	
	
	public void testSocketReadMethodReturnsInt() throws NoSuchMethodException
	{
		Method readMethod = Socket.class.getMethod("read", byte[].class, int.class, int.class);
		assertEquals(int.class, readMethod.getReturnType());
	}
	
	
	public void testSocketReadMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method readMethod = Socket.class.getMethod("read", byte[].class, int.class, int.class);
		Class<?>[] exceptions = readMethod.getExceptionTypes();
		assertEquals("public int read(byte[] buf, int offset, int maxlen) should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testSocketReadMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(Socket.class.getMethod("read", byte[].class, int.class, int.class));
	}
	
	/* public int write(byte[] buf, int offset, int len) */
	
	public void testSocketWriteMethodExists() throws NoSuchMethodException
	{
		Socket.class.getMethod("write", byte[].class, int.class, int.class);
	}
	

	public void testSocketWriteMethodReturnsInt() throws NoSuchMethodException
	{
		Method writeMethod = Socket.class.getMethod("write", byte[].class, int.class, int.class);
		assertEquals(int.class, writeMethod.getReturnType());
	}
	
	
	public void testSocketWriteMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method writeMethod = Socket.class.getMethod("write", byte[].class, int.class, int.class);
		Class<?>[] exceptions = writeMethod.getExceptionTypes();
		assertEquals("public int write(byte[] buf, int offset, int len) should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testSocketWriteMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(Socket.class.getMethod("write", byte[].class, int.class, int.class));
	}
	
	/* public boolean close() */
	
	public void testSocketCloseMethodExists() throws NoSuchMethodException
	{
		Socket.class.getMethod("close");
	}
	
	
	public void testSocketCloseMethodReturnsBoolean()throws NoSuchMethodException
	{
		Method closeMethod = Socket.class.getMethod("close");
		assertEquals(boolean.class, closeMethod.getReturnType());
	}
	
	
	public void testSocketCloseMethodThrowsNoExceptions() throws NoSuchMethodException
	{
		Method closeMethod = Socket.class.getMethod("close");
		Class<?>[] exceptions = closeMethod.getExceptionTypes();
		assertEquals("public boolean close() should not throw exceptions", 0, exceptions.length);
	}
	
	
	public void testSocketCloseMethodIsPublic() throws NoSuchMethodException
	{
		assertIsPublic(Socket.class.getMethod("close"));
	}
	
	/* No other method check */

	/**
	 * Checks TCP itself (not its subclasses) to make sure no methods public that shouldn't be
	 * there, are. This test can only be trusted if all TCP method specific tests pass.
	 * 
	 * @throws NoSuchMethodException 
	 */
	public void testNoOtherPublicMethodsExistInTCP() throws NoSuchMethodException
	{
		/* The list: */
		Method[] validMethods = {
				TCP.class.getMethod("socket"),
				TCP.class.getMethod("socket", int.class)
		};
		
		Method[] methods = TCP.class.getMethods();
		
		/* For all TCP's own methods, verify they are 'on the list' */
		for(Method method : methods)
		{
			if(method.getDeclaringClass().equals(TCP.class) && Modifier.isPublic(method.getModifiers()))
			{
				boolean isValid = false;
				for(Method validMethod : validMethods)
				{
					if(method.equals(validMethod)) isValid = true;
				}
				if(!isValid) fail("Unexpected method found in TCP: " + method.getName());
			}
		}
	}
	
	/**
	 * Checks that no other public constructors of TCP exist. This test can only be trusted if all
	 * constructor specific tests pass.
	 * 
	 * @throws NoSuchMethodException
	 */
	// Sadly, this test won't run because "TCP.class.getConstructors()" crashes the VM with a SIGSEGV.
//	public void testNoOtherPublicConstructorsExistsInTCP() throws NoSuchMethodException
//	{
//		/* The only valid one */
//		Constructor<?> validConstructor = TCP.class.getConstructor(int.class);
//		
//		Constructor<?>[] constructors = TCP.class.getConstructors();
//		boolean failed = false;
//		for(Constructor<?> constructor : constructors)
//		{
//			if(Modifier.isPublic(constructor.getModifiers()) && !constructor.equals(validConstructor))
//			{
//				failed = true;
//				break;
//			}
//		}
//		if(failed) fail("TCP exports an invalid public constructor");
//	}
	
	
	/**
	 * Checks Socket to make sure no methods public that shouldn't be there, are. This test can only
	 * be trusted if all Socket method specific tests pass.
	 * 
	 * @throws NoSuchMethodException 
	 */
	public void testNoOtherPublicMethodsExistInSocket() throws NoSuchMethodException
	{
		/* The list: */
		Method[] validMethods = {
				Socket.class.getMethod("connect", IpAddress.class, int.class),
				Socket.class.getMethod("accept"),
				Socket.class.getMethod("read", byte[].class, int.class, int.class),
				Socket.class.getMethod("write", byte[].class, int.class, int.class),
				Socket.class.getMethod("close"),
		};
		
		Method[] methods = Socket.class.getMethods();
		
		/* For all Socket's own methods, verify they are 'on the list' */
		for(Method method : methods)
		{
			if(method.getDeclaringClass().equals(Socket.class) && Modifier.isPublic(method.getModifiers()))
			{
				boolean isValid = false;
				for(Method validMethod : validMethods)
				{
					if(method.equals(validMethod)) isValid = true;
				}
				if(!isValid) fail("Unexpected method found in TCP: " + method.getName());
			}
		}
	}
	
	
	/**
	 * Checks for the existance of any innner classes of TCP outside Socket. If there are, it makes
	 * sure none of them have any public methods.
	 */
	public void testNoOtherPublicMethodsExistsInOtherInnerClassesOfTCP()
	{
		Class<?>[] innerClasses = TCP.class.getClasses();
		for(Class<?> class1 : innerClasses)
		{
			if(class1.equals(Socket.class)) continue;
			Method[] methods = class1.getMethods();
			for(Method method : methods)
			{
				if(method.getDeclaringClass().equals(class1) && Modifier.isPublic(method.getModifiers()))
				{
					fail("An inner class of TCP other than Socket has a public method");
				}
			}
		}
	}
	
	/* Helper methods */
	
	/**
	 * Asserts that the method is exactly public, not static public or anything else.
	 * 
	 * @param method	The method to check
	 */
	private void assertIsPublic(Method method)
	{
		int modifiers = method.getModifiers();
		assertEquals(Modifier.PUBLIC, modifiers);
	}
}
