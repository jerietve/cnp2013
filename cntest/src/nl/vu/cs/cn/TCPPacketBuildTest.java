package nl.vu.cs.cn;

import junit.framework.TestCase;
import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.TCPPacket;
import nl.vu.cs.cn.TCPPacket.FlagSet;

/**
 * This class tests the building of TCP packets using the setter methods. The construction of TCP
 * packets from IP packets is tested in TCPPacketParseTest. These tests make sure the following
 * behavior works as it should:
 * 
 * -	Packets are constructed using certain default values. They are not valid in this state.
 * -	Packets can have certain fields set.
 * -	Setting any of these fields invalidates the packet.
 * -	Setting illegal values is impossible and does not change the packet.
 * -	Building a packet which has illegal field values is impossible.
 * -	Building a packet which has only legal field values creates a valid packet.
 */
public class TCPPacketBuildTest extends TestCase
{
	private TCPPacket packet;
	private final IP.IpAddress validSourceAddress = IP.IpAddress.getAddress("192.168.0.100");
	private final IP.IpAddress validDestinationAddress = IP.IpAddress.getAddress("192.168.0.101");
	private final int validSourcePort = 1234;
	private final int validDestinationPort = 2345;
	private final int lowestValidPort = 0;
	private final int highestValidPort = TCPPacket.MAX_TCP_PORT_NR;
	private final long validSequenceNumber = 1234567L;
	private final long lowestValidSequenceNumber = 0;
	private final long highestValidSequenceNumber = TCPPacket.MAX_TCP_SEQUENCE;
	private final long validAcknowledgmentNumber = 1234564L;
	private final long lowestValidAcknowledgmentNumber = 0;
	private final long highestValidAcknowledgmentNumber = TCPPacket.MAX_TCP_SEQUENCE;
	private final byte[] validData = {1, 2, 3, 4, 5};
	private final int validDataOffset = 1;
	private final int validDataLength = 3;
	private final byte[] emptyData = {};
	private final byte[] tooMuchData = new byte[TCPPacket.MAX_TCP_DATA + 1];
	
	
	public TCPPacketBuildTest()
	{
		super();
	}
	
	
	@Override
	public void setUp() throws Exception
	{
		super.setUp();
		packet = new TCPPacket();
	}
	
	
	public void testNewPacketNeedsBuild()
	{
		assertTrue(packet.needsBuild());
	}
	
	
	public void testPshFlagSetByDefault()
	{
		assertTrue(packet.hasPsh());
	}
	
	
	public void testNewPacketNotBuildableWithoutSettingSource()
	{
		setEveryRequiredFieldBut(packet, "source");
		try
		{
			packet.build();
			fail("Build should have thrown RuntimeException");
		}
		catch(RuntimeException e)
		{
			/* Expected behavior */
		}
	}
	
	
	public void testNewPacketNotBuildableWithoutSettingDestination()
	{
		setEveryRequiredFieldBut(packet, "destination");
		try
		{
			packet.build();
			fail("Build should have thrown RuntimeException");
		}
		catch(RuntimeException e)
		{
			/* Expected behavior */
		}
	}
	
	
	public void testNewPacketNotBuildableWithoutSettingSequenceNumber()
	{
		setEveryRequiredFieldBut(packet, "sequenceNumber");
		try
		{
			packet.build();
			fail("Build should have thrown RuntimeException");
		}
		catch(RuntimeException e)
		{
			/* Expected behavior */
		}
	}
	
	public void testNewPacketBuildableWithoutSettingAcknowledgementNumber()
	{
		setEveryRequiredFieldBut(packet, "acknowledgmentNumber");
		try
		{
			packet.build();
		}
		catch(RuntimeException e)
		{
			fail("Build should not have thrown RuntimeException");
		}
	}
	
	public void testNewPacketNotBuildableWithoutSettingAcknowledgementNumberWhileACKisSet()
	{
		setEveryRequiredFieldBut(packet, "acknowledgmentNumber");
		try
		{
			packet.setFlags(FlagSet.F_ACK);
			packet.build();
			fail("Build should have thrown RuntimeException");
		}
		catch(RuntimeException e)
		{
			/* Expected behavior */
		}
	}
	
	public void testSettingValidSourcePortWorks()
	{
		packet.setSource(validSourceAddress, validSourcePort);
		assertEquals(validSourcePort, packet.getSourcePort());
	}
	
	
	public void testSettingLowestValidSourcePortWorks()
	{
		packet.setSource(validSourceAddress, lowestValidPort);
		assertEquals(lowestValidPort, packet.getSourcePort());
	}
	
	
	public void testSettingHighestValidSourcePortWorks()
	{
		packet.setSource(validSourceAddress, highestValidPort);
		assertEquals(highestValidPort, packet.getSourcePort());
	}
	
	
	public void testSettingLowInvalidSourcePortFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setSource(validSourceAddress, lowestValidPort - 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The source port should not have changed", validSourcePort, packet.getSourcePort());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingHighInvalidSourcePortFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setSource(validSourceAddress, highestValidPort + 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The source port should not have changed", validSourcePort, packet.getSourcePort());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingInvalidSourceIPFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setSource(null, validSourcePort);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The source address should not have changed", validSourceAddress, packet.getSourceIP());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingValidDestinationPortWorks()
	{
		packet.setDestination(validDestinationAddress, validDestinationPort);
		assertEquals(validDestinationPort, packet.getDestinationPort());
	}
	
	
	public void testSettingLowestValidDestinationPortWorks()
	{
		packet.setDestination(validDestinationAddress, lowestValidPort);
		assertEquals(lowestValidPort, packet.getDestinationPort());
	}
	
	
	public void testSettingHighestValidDestinationPortWorks()
	{
		packet.setDestination(validDestinationAddress, highestValidPort);
		assertEquals(highestValidPort, packet.getDestinationPort());
	}
	
	
	public void testSettingLowInvalidDestinationPortFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setDestination(validDestinationAddress, lowestValidPort - 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The destination port should not have changed", validDestinationPort, packet.getDestinationPort());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingHighInvalidDestinationPortFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setDestination(validDestinationAddress, highestValidPort + 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The destination port should not have changed", validDestinationPort, packet.getDestinationPort());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingInvalidDestinationIPFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setDestination(null, validDestinationPort);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The destination address should not have changed", validDestinationAddress, packet.getDestinationIP());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingValidSequenceNumberWorks()
	{
		packet.setSequenceNumber(validSequenceNumber);
		assertEquals(validSequenceNumber, packet.getSequenceNumber());
	}
	
	
	public void testSettingLowestValidSequenceNumberWorks()
	{
		packet.setSequenceNumber(lowestValidSequenceNumber);
		assertEquals(lowestValidSequenceNumber, packet.getSequenceNumber());
	}
	
	
	public void testSettingHighestValidSequenceNumberWorks()
	{
		packet.setSequenceNumber(highestValidSequenceNumber);
		assertEquals(highestValidSequenceNumber, packet.getSequenceNumber());
	}
	
	
	public void testSettingLowInvalidSequenceNumberFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setSequenceNumber(lowestValidSequenceNumber - 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The sequence number should not have changed", validSequenceNumber, packet.getSequenceNumber());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingHighInvalidSequenceNumberFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setSequenceNumber(highestValidSequenceNumber + 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The sequence number should not have changed", validSequenceNumber, packet.getSequenceNumber());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingValidAcknowledgmentNumberWorks()
	{
		packet.setAcknowledgmentNumber(validAcknowledgmentNumber);
		assertEquals(validAcknowledgmentNumber, packet.getAcknowledgmentNumber());
	}
	
	
	public void testSettingLowestValidAcknowledgmentNumberWorks()
	{
		packet.setAcknowledgmentNumber(lowestValidAcknowledgmentNumber);
		assertEquals(lowestValidAcknowledgmentNumber, packet.getAcknowledgmentNumber());
	}
	
	
	public void testSettingHighestValidAcknowledgmentNumberWorks()
	{
		packet.setAcknowledgmentNumber(highestValidAcknowledgmentNumber);
		assertEquals(highestValidAcknowledgmentNumber, packet.getAcknowledgmentNumber());
	}
	
	
	public void testSettingLowInvalidAcknowledgmentNumberFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setAcknowledgmentNumber(lowestValidAcknowledgmentNumber - 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The acknowledgment number should not have changed", validAcknowledgmentNumber, packet.getAcknowledgmentNumber());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingHighInvalidAcknowledgmentNumberFails()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		try
		{
			packet.setAcknowledgmentNumber(highestValidAcknowledgmentNumber + 1);
			fail("IllegalArgumentException expected");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
		assertEquals("The acknowledgment number should not have changed", validAcknowledgmentNumber, packet.getAcknowledgmentNumber());
		assertFalse("The packet should not need building", packet.needsBuild());
	}
	
	
	public void testSettingValidDataSucceeds()
	{
		packet.setData(validData, validDataOffset, validDataLength);
	}
	
	
	public void testSettingEmptyDataSucceeds()
	{
		packet.setData(emptyData, 0, 0);
	}
	
	
	public void testSettingNullDataSucceeds()
	{
		packet.setData(null, 0, 0);
	}
	
	
	public void testSettingTooMuchDataFails()
	{
		try
		{
			packet.setData(tooMuchData, 0, tooMuchData.length);
			fail("IllegalArgumentException should have been thrown");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
		}
	}
	
	public void testSettingSYN()
	{
		packet.setFlags(FlagSet.F_SYN);
		assertTrue(packet.getFlags().hasSyn());
		assertFalse(packet.getFlags().hasFin());
		assertFalse(packet.getFlags().hasAck());		
	}
	
	public void testSettingSYNACK()
	{
		packet.setFlags(FlagSet.F_SYNACK);
		assertTrue(packet.getFlags().hasSyn());
		assertFalse(packet.getFlags().hasFin());
		assertTrue(packet.getFlags().hasAck());		
	}
	
	public void testSettingFINACK()
	{
		packet.setFlags(FlagSet.F_FINACK);
		assertFalse(packet.getFlags().hasSyn());
		assertTrue(packet.getFlags().hasFin());
		assertTrue(packet.getFlags().hasAck());		
	}
	
	public void testSettingACK()
	{
		packet.setFlags(FlagSet.F_ACK);
		assertFalse(packet.getFlags().hasSyn());
		assertFalse(packet.getFlags().hasFin());
		assertTrue(packet.getFlags().hasAck());		
	}
	
	public void testSettingNoFlags()
	{
		try
		{
			packet.setFlags(FlagSet.F_INVALID);
			fail("IllegalArgumentException expected");
		}
		catch (IllegalArgumentException e)
		{
			//We expect this to happen
		}	
	}
	
	public void testSettingSourceInvalidatesPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.setSource(validSourceAddress, validSourcePort);
		assertTrue(packet.needsBuild());
	}
	
	
	public void testSettingDestinationInvalidatesPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.setDestination(validDestinationAddress, validDestinationPort);
		assertTrue(packet.needsBuild());
	}
	
	
	public void testSettingSequenceNumberInvalidatesPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.setSequenceNumber(validSequenceNumber);
		assertTrue(packet.needsBuild());
	}
	
	
	public void testSettingAcknowledgmentNumberInvalidatesPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.setAcknowledgmentNumber(validAcknowledgmentNumber);
		assertTrue(packet.needsBuild());
	}
	
	
	public void testSettingDataInvalidatesPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.setData(validData, validDataOffset, validDataLength);
		assertTrue(packet.needsBuild());
	}
	
	
	public void testSettingFlagsInvalidatesPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.setFlags(FlagSet.F_ACK);
		assertTrue(packet.needsBuild());
	}
	
	
	public void testSuccessfullyBuiltPacketDoesNotNeedBuild()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		assertFalse(packet.needsBuild());
	}
	
	
	/** Should just work.. Nothing special */
	public void testBuildingAlreadyBuiltPacket()
	{
		setAllFieldsToLegalValuesAndBuild(packet);
		packet.build();
	}
	
	
	/**
	 * Sets all fields of packet to legal values, except field. Field is not touched.
	 * 
	 * @param field	The name of the field which should not be set.
	 */
	private void setEveryRequiredFieldBut(TCPPacket packet, String field)
	{
		if(!field.equals("source")) packet.setSource(validSourceAddress, validSourcePort);
		if(!field.equals("destination")) packet.setDestination(validDestinationAddress, validDestinationPort);
		if(!field.equals("sequenceNumber")) packet.setSequenceNumber(validSequenceNumber);
		if(!field.equals("flags")) packet.setFlags(FlagSet.F_SYN);
		if(!field.equals("acknowledgmentNumber")) packet.setAcknowledgmentNumber(validAcknowledgmentNumber);
	}
	
	
	/**
	 * Read the name.
	 * 
	 * @param packet	The packet to make valid
	 */
	private void setAllFieldsToLegalValuesAndBuild(TCPPacket packet)
	{
		packet.setSource(validSourceAddress, validSourcePort);
		packet.setDestination(validDestinationAddress, validDestinationPort);
		packet.setFlags(FlagSet.F_SYN);
		packet.setSequenceNumber(validSequenceNumber);
		packet.setAcknowledgmentNumber(validAcknowledgmentNumber);
		packet.setData(validData, validDataOffset, validDataLength);
		packet.build();
	}
}
