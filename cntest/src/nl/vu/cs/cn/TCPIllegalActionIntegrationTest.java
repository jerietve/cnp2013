package nl.vu.cs.cn;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import nl.vu.cs.cn.TCP.Socket;
import nl.vu.cs.cn.executions.BaseExecution;

/**
 * This class tests different illegal scenarios, such as calling read or write before having
 * connected, or with otherwise invalid input. The first bunch of tests require no connection, so
 * they do not use an execution. All other tests work as in most other integration tests: first a
 * private static nested class extending BaseExecution describing the scenario, then a do* method
 * handling the setup of these executions, then a load of test methods which provide the do* method
 * with different input and check results.
 */
public class TCPIllegalActionIntegrationTest extends TCPIntegrationTest
{
	public TCPIllegalActionIntegrationTest()
	{
		super();
	}
	
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
	}
	
	
	/**
	 * First some tests for trying to take actions which are not allowed without a connection.
	 * Because no connection is required, hence no threads, these tests are set up a little
	 * different from the rest.
	 */
	
	/**
	 * This is test scenario SCN6.
	 */
	public void testReadWithoutConnectionFails() throws IOException
	{
		byte[] buffer = new byte[] {42};
		TCP tcp = new TCP(CLIENT_ADDRESS);
		Socket socket = tcp.socket();
		assertEquals(0, socket.read(buffer, 0, 1));
	}
	
	
	public void testReadWithoutConnectionDoesNotMutateBuffer() throws IOException
	{
		byte[] buffer = new byte[] {42};
		TCP tcp = new TCP(CLIENT_ADDRESS);
		Socket socket = tcp.socket();
		socket.read(buffer, 0, 1);
		assertEquals(42, buffer[0]);
	}
	

	/**
	 * This is test scenario SCN7.
	 */
	public void testWriteWithoutConnectionFails() throws IOException
	{
		byte[] buffer = new byte[] {42};
		TCP tcp = new TCP(CLIENT_ADDRESS);
		Socket socket = tcp.socket();
		assertEquals(-1, socket.write(buffer, 0, 1));
	}
	
	
	public void testWriteWithoutConnectionDoesNotMutateBuffer() throws IOException
	{
		byte[] buffer = new byte[] {42};
		TCP tcp = new TCP(CLIENT_ADDRESS);
		Socket socket = tcp.socket();
		socket.write(buffer, 0, 1);
		assertEquals(42, buffer[0]);
	}
	

	/**
	 * This is test scenario SCN8.
	 */
	public void testCloseWithoutConnectionFails() throws IOException
	{
		TCP tcp = new TCP(CLIENT_ADDRESS);
		Socket socket = tcp.socket();
		assertFalse(socket.close());
	}
	
	
	/**
	 * Make sure calling accept() while a connection is open does not disturb that connection.
	 * Client and server connect. They start writing data to each other, both calling accept() in
	 * the process.
	 * 
	 * This is test scenario SCN9.
	 */
	private static class IllegalAcceptExecution extends BaseExecution
	{
		private byte[] sendData;
		private long acceptTime;
		

		@Override
		public void executeAsClient()
		{
			receivedData = new byte[200];
			socket.connect(serverIPAddress, serverPort);
			socket.write(sendData, 0, 100);
			long startTime = System.currentTimeMillis();
			socket.accept();
			acceptTime = System.currentTimeMillis() - startTime;
			socket.read(receivedData, 0, 100);
			socket.read(receivedData, 100, 100);
		}
		

		@Override
		public void executeAsServer()
		{
			receivedData = new byte[100];
			socket.accept();
			socket.read(receivedData, 0, 100);
			socket.write(sendData, 0, 100);
			long startTime = System.currentTimeMillis();
			socket.accept();
			acceptTime = System.currentTimeMillis() - startTime;
			socket.write(sendData, 100, 100);
		}
	}
	
	
	private void doIllegalAccept() throws InterruptedException, ExecutionException, IOException
	{
		clientSendData = getRandomBytes(100);
		serverSendData = getRandomBytes(200);
		
		clientExecution = new IllegalAcceptExecution();
		((IllegalAcceptExecution)clientExecution).sendData = clientSendData;
		
		serverExecution = new IllegalAcceptExecution();
		((IllegalAcceptExecution)serverExecution).sendData = serverSendData;
		
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testAcceptDuringConnectionDoesNotBreakConnection() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalAccept();
		assertTrue("Client should have received all data sent by the server", clientExecution.verifyReceivedData(serverSendData));
	}
	
	
	public void testAcceptDuringConnectionDoesNotBreakConnection2() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalAccept();
		assertTrue("Server should have received all data sent by the client", serverExecution.verifyReceivedData(clientSendData));
	}
	
	
	public void testAcceptByServerDuringConnectReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalAccept();
		assertLessThan("Server accept() should return within 100 ms", 100, ((IllegalAcceptExecution)serverExecution).acceptTime);
	}
	
	
	public void testAcceptByClientDuringConnectReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalAccept();
		assertLessThan("Client accept() should return within 100 ms", 100, ((IllegalAcceptExecution)clientExecution).acceptTime);
	}
	
	
	/**
	 * Make sure calling connect() while a connection is open does not disturb that connection.
	 * Client and server connect. They start writing data to each other, both calling connect() to
	 * a third (non existent) server in the process.
	 * 
	 * This is test scenario SCN10.
	 */
	private static class IllegalConnectExecution extends BaseExecution
	{
		private byte[] sendData;
		private long connectTime;
		

		@Override
		public void executeAsClient()
		{
			receivedData = new byte[200];
			socket.connect(serverIPAddress, serverPort);
			socket.write(sendData, 0, 100);
			long startTime = System.currentTimeMillis();
			socket.accept();
			connectTime = System.currentTimeMillis() - startTime;
			socket.read(receivedData, 0, 100);
			socket.read(receivedData, 100, 100);
		}
		

		@Override
		public void executeAsServer()
		{
			receivedData = new byte[100];
			socket.accept();
			socket.read(receivedData, 0, 100);
			socket.write(sendData, 0, 100);
			long startTime = System.currentTimeMillis();
			socket.accept();
			connectTime = System.currentTimeMillis() - startTime;
			socket.write(sendData, 100, 100);
		}
	}
	
	
	private void doIllegalConnect() throws InterruptedException, ExecutionException, IOException
	{
		clientSendData = getRandomBytes(100);
		serverSendData = getRandomBytes(200);
		
		clientExecution = new IllegalConnectExecution();
		((IllegalConnectExecution)clientExecution).sendData = clientSendData;
		
		serverExecution = new IllegalConnectExecution();
		((IllegalConnectExecution)serverExecution).sendData = serverSendData;
		
		runBothToCompletion(clientExecution, serverExecution);
	}
	
	
	public void testConnectDuringConnectionDoesNotBreakConnection() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalConnect();
		assertTrue("Client should have received all data sent by the server", clientExecution.verifyReceivedData(serverSendData));
	}
	
	
	public void testConnectDuringConnectionDoesNotBreakConnection2() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalConnect();
		assertTrue("Server should have received all data sent by the client", serverExecution.verifyReceivedData(clientSendData));
	}
	
	
	public void testConnectByServerDuringConnectReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalConnect();
		assertLessThan("Server connect() should return within 100 ms", 100, ((IllegalConnectExecution)serverExecution).connectTime);
	}
	
	
	public void testConnectByClientDuringConnectReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doIllegalConnect();
		assertLessThan("Client connect() should return within 100 ms", 100, ((IllegalConnectExecution)clientExecution).connectTime);
	}
	
	
	/**
	 * This test is a little different from the rest. It's more like a unit test really, but
	 * because a connection is required it was put with the integration tests. We're going to test
	 * the response of read and write to invalid or strange input parameters. Both should respond
	 * in exactly the same manner, so only one execution is used to test both (separately).
	 * 
	 * In order to make sure the client doesn't read anything, the server does send it some data.
	 * This data is read after the failing call to read.
	 * 
	 * This is test scenario SCN11.
	 */
	private static class ReadWriteExecution extends BaseExecution
	{
		/* True for reading, false for writing */
		private boolean readingTest;
		private static final int realDataLength = 100;
		/* The buffer to not receive any data into/send any data from (client) */
		private byte[] unusedBuffer;
		private int offset, maxlen;
		/* The buffer of data to send (server) */
		private byte[] sendData;
		/* The time spent in the read/write call */
		private long returnTime;
		/* The value returned by read/write */
		private int returnValue;
		
		
		@Override
		public void executeAsClient()
		{
			socket.connect(serverIPAddress, serverPort);
			long startTime = System.currentTimeMillis();
			if(readingTest)	returnValue = socket.read(unusedBuffer, offset, maxlen);
			else returnValue = socket.write(unusedBuffer, offset, maxlen);
			returnTime = System.currentTimeMillis() - startTime;
			if(readingTest)
			{
				receivedData = new byte[realDataLength];
				socket.read(receivedData, 0, realDataLength);
			}
			else socket.write(sendData, 0, realDataLength);
		}
		

		@Override
		public void executeAsServer()
		{
			socket.accept();
			/* Make sure the client doesn't have anything to read for a while */
			long startTime = System.currentTimeMillis();
			while(System.currentTimeMillis() - 300 < startTime)
			{
				try
				{
					/* Max because if the time has increased since the check in while, a negative
					 * value might be fed to sleep */
					Thread.sleep(Math.max(0, 300 - (System.currentTimeMillis() - startTime)));
				}
				catch(InterruptedException e)
				{
					/* Sleep some more */
				}
			}
			if(readingTest) socket.write(sendData, 0, realDataLength);
			else
			{
				receivedData = new byte[realDataLength];
				socket.read(receivedData, offset, realDataLength);
			}
		}
	}
	
	
	private void doReadWriteTest(boolean readingTest, byte[] unusedBuffer, int offset, int maxlen)
			throws InterruptedException, ExecutionException, IOException
	{
		clientExecution = new ReadWriteExecution();
		((ReadWriteExecution)clientExecution).readingTest = readingTest;
		((ReadWriteExecution)clientExecution).unusedBuffer = unusedBuffer;
		((ReadWriteExecution)clientExecution).offset = offset;
		((ReadWriteExecution)clientExecution).maxlen = maxlen;
		
		serverExecution = new ReadWriteExecution();
		((ReadWriteExecution)serverExecution).readingTest = readingTest;
		
		byte[] sendData = getRandomBytes(ReadWriteExecution.realDataLength);
		byte[] copyOfSendData = new byte[ReadWriteExecution.realDataLength];
		System.arraycopy(sendData, 0, copyOfSendData, 0, sendData.length);
		if(readingTest) ((ReadWriteExecution)serverExecution).sendData = sendData;
		else ((ReadWriteExecution)clientExecution).sendData = sendData;
		
		runBothToCompletion(clientExecution, serverExecution);
		
		/* Make sure the data sent after the test was complete did arrive */
		if(readingTest) clientExecution.verifyReceivedData(copyOfSendData);
		else serverExecution.verifyReceivedData(copyOfSendData);
	}
	
	
	public void testReadNullBufferReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, null, 0, 0);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testReadNegativeOffsetReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42}, -1, 0);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testReadNegativeMaxlenReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42}, 0, -1);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testReadBeyondBufferLengthReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42, 43, 44}, 2, 2);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testReadEmptyArrayReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[0], 0, 0);
		assertEquals(0, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testReadMaxlenZeroReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42, 43}, 1, 0);
		assertEquals(0, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testReadNullBufferReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, null, 0, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testReadNegativeOffsetReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42}, -1, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testReadNegativeMaxlenReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42}, 0, -1);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testReadBeyondBufferLengthReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42, 43, 44}, 2, 2);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testReadEmptyArrayReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[0], 0, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testReadMaxlenZeroReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42, 43}, 1, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testReadNullBufferDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, null, 0, 0);
		assertNull(((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testReadNegativeOffsetDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42}, -1, 0);
		assertEquals(new byte[] {42}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testReadNegativeMaxlenDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42}, 0, -1);
		assertEquals(new byte[] {42}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testReadBeyondBufferLengthDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42, 43, 44}, 2, 2);
		assertEquals(new byte[] {42, 43, 44}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testReadEmptyArrayDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[0], 0, 0);
		assertEquals(new byte[0], ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testReadMaxlenZeroDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(true, new byte[] {42, 43}, 1, 0);
		assertEquals(new byte[] {42, 43}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testWriteNullBufferReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, null, 0, 0);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testWriteNegativeOffsetReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42}, -1, 0);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testWriteNegativeMaxlenReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42}, 0, -1);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testWriteBeyondBufferLengthReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42, 43, 44}, 2, 2);
		assertEquals(-1, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testWriteEmptyArrayReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[0], 0, 0);
		assertEquals(0, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testWriteMaxlenZeroReturnValue() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42, 43}, 1, 0);
		assertEquals(0, ((ReadWriteExecution)clientExecution).returnValue);
	}
	
	
	public void testWriteNullBufferReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, null, 0, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testWriteNegativeOffsetReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42}, -1, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testWriteNegativeMaxlenReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42}, 0, -1);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testWriteBeyondBufferLengthReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42, 43, 44}, 2, 2);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testWriteEmptyArrayReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[0], 0, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testWriteMaxlenZeroReturnsImmediately() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42, 43}, 1, 0);
		assertLessThan("Read should return within 100 ms", 100, ((ReadWriteExecution)clientExecution).returnTime);
	}
	
	
	public void testWriteNullBufferDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, null, 0, 0);
		assertNull(((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testWriteNegativeOffsetDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42}, -1, 0);
		assertEquals(new byte[] {42}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testWriteNegativeMaxlenDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42}, 0, -1);
		assertEquals(new byte[] {42}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testWriteBeyondBufferLengthDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42, 43, 44}, 2, 2);
		assertEquals(new byte[] {42, 43, 44}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testWriteEmptyArrayDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[0], 0, 0);
		assertEquals(new byte[0], ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	public void testWriteMaxlenZeroDoesNotMutateBuffer() throws InterruptedException, ExecutionException, IOException
	{
		doReadWriteTest(false, new byte[] {42, 43}, 1, 0);
		assertEquals(new byte[] {42, 43}, ((ReadWriteExecution)clientExecution).unusedBuffer);
	}
	
	
	@Override
	protected void tearDown() throws Exception
	{
		super.tearDown();
	}
}
