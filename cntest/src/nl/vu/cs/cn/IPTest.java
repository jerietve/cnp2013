package nl.vu.cs.cn;

import java.io.IOException;

import nl.vu.cs.cn.IP;
import nl.vu.cs.cn.TCP;
import junit.framework.TestCase;

/**
 * Tests whether the IP layer saves packets if the destination is not ready to receive them yet.
 */
public class IPTest extends TestCase
{
	private IP ip1, ip2;
	private IP.Packet sendPacket, receivePacket;
	
	
	public IPTest()
	{
		super();
	}
	
	
	@Override
	public void setUp() throws Exception
	{
		super.setUp();
		ip1 = new IP(100);
		ip2 = new IP(101);
		sendPacket = new IP.Packet(ip2.getLocalAddress().getAddress(), TCP.PROTOCOL, 42, new byte[] { (byte)42 }, 1);
		receivePacket = new IP.Packet();
	}
	
	
	/* Make sure the IP player really buffers a packet */
	public void testSendSleepReceiveIPPacket() throws IOException, InterruptedException
	{
		ip1.ip_send(sendPacket);
		Thread.sleep(1000);
		ip2.ip_receive_timeout(receivePacket, 1);
	}
}
