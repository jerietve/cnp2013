package nl.vu.cs.cn.test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.test.AndroidTestCase;

import nl.vu.cs.cn.TCP.Socket;
import nl.vu.cs.cn.TCPPacket;
import nl.vu.cs.cn.TCPPacket.FlagSet;
import nl.vu.cs.cn.util.Interceptor;


/**
 * For now this class only does one simple thing. It checks the FlagSet of every incoming and
 * outgoing packet to a preset sequence, and calls fail in case an unexpected packet it seen.
 */
public class PacketFlowInspector extends AndroidTestCase implements Interceptor
{
	/** List of PacketEvents we expect to happen, exactly in this order */
	private LinkedList<PacketEvent> expectedPacketEvents = new LinkedList<PacketEvent>();
	/** List of PacketEvents that actually happened */
	private LinkedList<PacketEvent> actualPacketEvents = new LinkedList<PacketEvent>();
	/** Must we throw an AssertionFailedError if anything happens that's not on the list? */
	private boolean mustCheck;
	/** Must we make a new list of what does actually happen? */
	private boolean mustRecord;
	
	
	public PacketFlowInspector()
	{
		this(true, true);
	}
	
	
	/**
	 * @param check		Check the packet events and fail if something I don't expect happens?
	 * @param record	Record the packet events so you can ask me what happened afterward?
	 */
	public PacketFlowInspector(boolean check, boolean record)
	{
		this.mustCheck = check;
		this.mustRecord = record;
	}
	
	
	/**
	 * Creates a new instance and immediately adds the events for your convenience.
	 * 
	 * @param events
	 */
	public PacketFlowInspector(List<PacketEvent> events)
	{
		expectedPacketEvents.addAll(events);
	}
	
	
	@Override
	public TCPPacket inboundPacket(TCPPacket in)
	{
		if(mustRecord)
		{
			actualPacketEvents.add(new PacketEvent(true, in));
		}
		if(mustCheck)
		{
			if(expectedPacketEvents.isEmpty()) fail("No more packets should arrive, but " + in.getFlags() + " arrived");
			assertEquals(expectedPacketEvents.remove(), new PacketEvent(true, in));
		}
		return in;
	}	

	@Override
	public TCPPacket outboundPacket(TCPPacket out)
	{
		if(mustRecord)
		{
			/* Outgoing packets might have been dropped by earlier interceptors */
			if(out == null) actualPacketEvents.add(null);
			else actualPacketEvents.add(new PacketEvent(false, out));
		}
		if(mustCheck && out != null)
		{
			if(expectedPacketEvents.isEmpty()) fail("No more packets should be sent, but " + out.getFlags() + " was sent");
			assertEquals(expectedPacketEvents.remove(), new PacketEvent(false, out));
		}
		return out;
	}
	
	
	public void addEvent(PacketEvent event)
	{
		expectedPacketEvents.add(event);
	}
	
	
	public void addAllEvents(List<PacketEvent> events)
	{
		expectedPacketEvents.addAll(events);
	}
	
	
	public LinkedList<PacketEvent> getActualPacketEvents()
	{
		return actualPacketEvents;
	}
	
	
	/**
	 * @return The list of packets you would expect in case of a normal connect call
	 */
	public static List<PacketEvent> normalConnect()
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		result.add(new PacketEvent(false, FlagSet.F_SYN));
		result.add(new PacketEvent(true, FlagSet.F_SYNACK));
		result.add(new PacketEvent(false, FlagSet.F_ACK));
		return result;
	}
	
	
	/**
	 * @return The list of packets you would expect in case of a normal accept call
	 */
	public static List<PacketEvent> normalAccept()
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		result.add(new PacketEvent(true, FlagSet.F_SYN));
		result.add(new PacketEvent(false, FlagSet.F_SYNACK));
		result.add(new PacketEvent(true, FlagSet.F_ACK));
		return result;
	}
	
	
	/**
	 * @return The list of packets you would expect in case of a connect call where packetsToDrop
	 * SYN packets are dropped
	 */
	public static List<PacketEvent> connectDroppedSYN(int packetsToDrop)
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		int dropped = -1;
		while(dropped < packetsToDrop && dropped < Socket.MAX_TRANSMISSIONS)
		{
			result.add(new PacketEvent(false, FlagSet.F_SYN));
			dropped++;
		}
		if(dropped < Socket.MAX_TRANSMISSIONS)
		{
			result.add(new PacketEvent(true, FlagSet.F_SYNACK));
			result.add(new PacketEvent(false, FlagSet.F_ACK));
		}
		return result;
	}
	
	
	/**
	 * @return The list of packets you would expect in case of an accept call where packetsToDrop
	 * SYN packets are dropped
	 */
	public static List<PacketEvent> acceptDroppedSYN(int packetsToDrop)
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		if(packetsToDrop < Socket.MAX_TRANSMISSIONS)
		{
			result.add(new PacketEvent(true, FlagSet.F_SYN));
			result.add(new PacketEvent(false, FlagSet.F_SYNACK));
			result.add(new PacketEvent(true, FlagSet.F_ACK));
		}
		return result;
	}
	
	
	/**
	 * @return The list of packets you would expect in case of a connect call where packetsToDrop
	 * SYNACK packets are dropped
	 */
	public static List<PacketEvent> connectDroppedSYNACK(int packetsToDrop)
	{
		return connectDroppedSYN(packetsToDrop);
	}
	
	
	/**
	 * @return The list of packets you would expect in case of an accept call where packetsToDrop
	 * SYNACK packets are dropped
	 */
	public static List<PacketEvent> acceptDroppedSYNACK(int packetsToDrop)
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		int dropped = -1;
		while(dropped < packetsToDrop && dropped < Socket.MAX_TRANSMISSIONS)
		{
			result.add(new PacketEvent(true, FlagSet.F_SYN));
			result.add(new PacketEvent(false, FlagSet.F_SYNACK));
			dropped++;
		}
		if(packetsToDrop < Socket.MAX_TRANSMISSIONS)
		{
			result.add(new PacketEvent(true, FlagSet.F_ACK));
		}
		return result;
	}
	
	
	/**
	 * The list for the following situation: you close, packetToDrop FIN packets are lost, the other
	 * side at the same time sends one packet and then closes.
	 */
	public static List<PacketEvent> immediateFourWayCloseLostFINCloser(int packetsToDrop)
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		result.add(new PacketEvent(true, FlagSet.F_FINACK));
		result.add(new PacketEvent(false, FlagSet.F_ACK));
		result.add(new PacketEvent(true, FlagSet.F_ACK));
		result.add(new PacketEvent(false, FlagSet.F_FINACK));
		result.add(new PacketEvent(true, FlagSet.F_ACK));
		for(int i = 0; i < packetsToDrop && i < Socket.MAX_TRANSMISSIONS; i++)
			result.add(new PacketEvent(false, FlagSet.F_FINACK));
		result.add(new PacketEvent(true, FlagSet.F_ACK));
		return result;
	}
	
	
	/**
	 * The list for the following situation: the other side closes, packetToDrop FIN packets are
	 * lost, you at the same time send one packet and then close.
	 */
	public static List<PacketEvent> immediateFourWayCloseLostFINClosee(int packetsToDrop)
	{
		ArrayList<PacketEvent> result = new ArrayList<PacketEvent>();
		result.add(new PacketEvent(true, FlagSet.F_ACK));
		result.add(new PacketEvent(false, FlagSet.F_ACK));
		result.add(new PacketEvent(true, FlagSet.F_FINACK));
		result.add(new PacketEvent(false, FlagSet.F_ACK));
		if(packetsToDrop < Socket.MAX_TRANSMISSIONS)
		{
			result.add(new PacketEvent(true, FlagSet.F_FINACK));
			result.add(new PacketEvent(false, FlagSet.F_ACK));
		}
		return result;
	}
}