package nl.vu.cs.cn.test;

import junit.framework.AssertionFailedError;
import android.test.AndroidTestCase;

/**
 * Can be extended instead of AndroidTestCase to provide a couple of extra assert methods.
 */
public abstract class MoreAssert extends AndroidTestCase
{
	public static void assertLessThan(String message, long expected, long actual)
	{
		if(!(actual < expected))
				throw new AssertionFailedError(message + ": expected less than " + expected + " but was " + actual);
	}
	
	
	public static void assertEquals(byte[] expected, byte[] actual)
	{
		/* assertTrue(Arrays.equals(expected, actual)); doesn't provide any info as to what went
		 * wrong. We can do better: */
		if(actual == null && expected == null) return;
		if(actual == null && expected != null) throw new AssertionFailedError("Expected not null, is null");
		if(actual != null && expected == null) throw new AssertionFailedError("Expected null, is not null");
		/* They're both not null */
		if(actual.length != expected.length)
			throw new AssertionFailedError("Arrays are of different lengt: expected " +	expected.length + ", found " + actual.length);
		for(int i = 0; i < actual.length; i++)
		{
			if(actual[i] != expected[i])
				throw new AssertionFailedError("Value at index " + i + " should be " + expected[i]+ ", found " + actual[i]);
		}
	}
}
