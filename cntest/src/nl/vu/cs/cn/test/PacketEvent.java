package nl.vu.cs.cn.test;

import nl.vu.cs.cn.TCPPacket;
import nl.vu.cs.cn.TCPPacket.FlagSet;

/**
 * A PacketEvent is simply a container describing things that can happen involving packets. At the
 * moment it only describes whether a packet was received or sent, the FlagSet of that packet and
 * whether or not it contained data.
 */
public class PacketEvent
{
	public boolean incoming;
	public FlagSet flagSet;
	public boolean hasData;
	
	
	/**
	 * Creates a new PacketEvent with all fields set. The packet is quickly used to set all fields,
	 * after which you can use it again for whatever you want.
	 */
	public PacketEvent(boolean incoming, TCPPacket packet)
	{
		this.incoming = incoming;
		this.flagSet = packet.getFlags();
		this.hasData = packet.hasData();
	}
	
	
	/**
	 * Creates a new PacketEvent with only the incoming and flagSet fields set.
	 */
	public PacketEvent(boolean incoming, FlagSet flagSet)
	{
		this.incoming = incoming;
		this.flagSet = flagSet;
	}
	
	
	public boolean equals(Object other)
	{
		if(other.getClass() != this.getClass()) return false;
		PacketEvent otherPacketEvent = (PacketEvent)other;
		return this.incoming == otherPacketEvent.incoming && this.flagSet == otherPacketEvent.flagSet;
	}
	
	
	public String toString()
	{
		return (incoming ? "Incoming " : "Outgoing ") + flagSet;
	}
}
