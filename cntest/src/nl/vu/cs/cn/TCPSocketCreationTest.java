package nl.vu.cs.cn;

import nl.vu.cs.cn.TCP;
import nl.vu.cs.cn.TCP.Socket;
import junit.framework.TestCase;

/**
 * This class tests the creation of sockets on a TCP stack. Two kinds of sockets exist: client
 * sockets which are bound to any free port, and server sockets which are bound to a given port.
 * It should not be possible for multiple sockets on the same TCP stack to bound to the same port.
 * There are very many ports, and even though the code should be written such that once they are
 * all taken, no more ports can be bound, testing for this crashes Android before that ever happens.
 */
public class TCPSocketCreationTest extends TestCase
{
	private TCP tcp;
	
	
	public TCPSocketCreationTest()
	{
		super();
	}
	
	
	@Override
	public void setUp() throws Exception
	{
		super.setUp();
		tcp = new TCP(100);
	}
	
	
	@Override
	public void tearDown() throws Exception
	{
		tcp = null;
		super.tearDown();
	}
	
	
	public void testCreateValidSocketNoPort()
	{
		Socket socket = tcp.socket();
		assertTrue("Client sockets get assigned a port number between " + TCP.Socket.MIN_FREE_PORT_NUMBER +	" and " +
				TCP.Socket.MAX_PORT_NUMBER,	socket.getLocalPort() >= 1024 && socket.getLocalPort() <= TCP.Socket.MAX_PORT_NUMBER);
	}
	
	
	public void testCreateValidSocketWithPort()
	{
		Socket socket = tcp.socket(80);
		assertEquals("Server ports should bind to valid port 80", 80, socket.getLocalPort());
	}
	
	
	public void testCreateValidSocketLowestPort()
	{
		Socket socket = tcp.socket(0);
		assertEquals("Server ports should bind to valid port 0", 0, socket.getLocalPort());
	}
	
	
	public void testCreateValidSocketHighestPort()
	{
		Socket socket = tcp.socket(TCP.Socket.MAX_PORT_NUMBER);
		assertEquals("Server ports should bind to valid port MAX_PORT_NUMBER",
				TCP.Socket.MAX_PORT_NUMBER, socket.getLocalPort());
	}
	
	
	public void testCreateInvalidSocketBelowLowestPort()
	{
		try
		{
			tcp.socket(-1);
			fail("Server ports should not bind to invalid port -1");
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
			return;
		}
		catch (Exception e)
		{
			fail("IllegalArgumentException expected");
		}
	}
	
	
	public void testCreateInvalidSocketAboveHighestPort()
	{
		try
		{
			tcp.socket(TCP.Socket.MAX_PORT_NUMBER + 1);
			fail("Server ports should not bind to invalid port " + (TCP.Socket.MAX_PORT_NUMBER + 1));
		}
		catch(IllegalArgumentException e)
		{
			/* Expected behavior */
			return;
		}
		catch (Exception e)
		{
			fail("IllegalArgumentException expected");
		}
	}
	
	
	public void testCreatingTwoSocketsSamePortFails()
	{
		tcp.socket(1234);
		try
		{
			tcp.socket(1234);
			fail("Two ports should not both be able to bind to port 1234");
		}
		catch(RuntimeException e)
		{
			/* Expected behavior */
			return;
		}
		catch (Exception e)
		{
			fail("RuntimeException expected");
		}
	}
	
	
	public void testCreateTwoClientSocketsGetDifferentPorts()
	{
		Socket socket1 = tcp.socket();
		Socket socket2 = tcp.socket();
		assertTrue("The two sockets should have different ports", socket1.getLocalPort() != socket2.getLocalPort());
	}
}
