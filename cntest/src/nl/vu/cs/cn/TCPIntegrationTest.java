package nl.vu.cs.cn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.util.Log;

import nl.vu.cs.cn.util.Interceptor;
import nl.vu.cs.cn.TCPPacket.FlagSet;
import nl.vu.cs.cn.executions.BaseExecution;
import nl.vu.cs.cn.test.MoreAssert;
import nl.vu.cs.cn.test.PacketEvent;
import nl.vu.cs.cn.test.PacketFlowInspector;

/**
 * This is the base class for all integration tests. It is subclasses by all real integration tests.
 * Integration tests work by creating two executions. These executions are subclasses of
 * {@link BaseExecution}. Usually a client and server will be of the same type, and interact with
 * one another. Because almost all integration tests need these two executions, they are mostly
 * handled in this integration test base class. Some variables can be set such as the maximum time
 * a test should be allowed to run and any {@link Interceptor interceptors} that must be present in
 * the client and server execution. After that, the executions are provided to the method
 * {@link #runBothToCompletion(BaseExecution, BaseExecution)}. This method runs both executions, and
 * cancels them if they take too long. In this case, it waits a little while until either they have
 * both made known that they are really done, or until a short timeout expires. The reason for this
 * is that the tests may require variables set by the executions which are set only after they are
 * interrupted.
 */
public abstract class TCPIntegrationTest extends MoreAssert
{
	/* A single test may take 13 seconds at most! 
	 * This seemed reasonable since we have at most 10 transmissions, each with
	 * a 1 sec timeout + 3 seconds */
	protected final int MAX_TEST_EXECUTION_TIME = 13000;
	private final int MAX_WAIT_CANCELLED_EXECUTIONS = 100;
	
	protected final int SERVER_PORT = 1234;
	protected final int SERVER_ADDRESS = 101;
	protected final int CLIENT_ADDRESS = 100;
	protected final IP.IpAddress SERVER_IP = IP.IpAddress.getAddress("192.168.0." + SERVER_ADDRESS);
	protected byte[] clientSendData, serverSendData;

	/* The executions (subclassed per test) */
	protected BaseExecution clientExecution, serverExecution;
	
	/* The variables set for testing */
	private ArrayList<Interceptor> clientInterceptors, serverInterceptors;
	private int maxRunTime;
	private boolean tellMeExactlyWhatHappened;
	/* These will help you see what exactly happened */
	private PacketFlowInspector clientPreInspector, clientPostInspector, serverPreInspector, serverPostInspector;

	/* Log tag */
	protected static final String TAG = "CNP_TCPTest";
	
	
	/**
	 * Creates a new integration test, with no interceptors and a maximum runtime of
	 * MAX_TEST_EXECUTION_TIME.
	 */
	public TCPIntegrationTest()
	{
		super();
	}
	
	
	@Override
	protected void setUp() throws Exception
	{
		super.setUp();
		clientInterceptors = new ArrayList<Interceptor>();
		serverInterceptors = new ArrayList<Interceptor>();
		maxRunTime = MAX_TEST_EXECUTION_TIME;
		tellMeExactlyWhatHappened = true;
	}
	
	
	protected void addClientInterceptor(Interceptor nextInterceptor)
	{
		clientInterceptors.add(nextInterceptor);
	}
	
	
	protected void addServerInterceptor(Interceptor nextInterceptor)
	{
		serverInterceptors.add(nextInterceptor);
	}
	
	
	/**
	 * @param maxRunTime	milliseconds
	 */
	public void setMaxRunTime(int maxRunTime)
	{
		this.maxRunTime = maxRunTime;
	}
	
	
	/**
	 * Runs two executions to completion, after installing any previously provided interceptors. 
	 * This method will complete within maxRunTime + MAX_WAIT_CANCELLED_EXECUTIONS / 100 seconds.
	 * 
	 * @param clientExecution		The execution for the client
	 * @param serverExecution		The execution for the server
	 * 
	 * @return true iff the executions finished in time
	 * 
	 * @throws InterruptedException	Thrown from the executions
	 * @throws ExecutionException	Thrown from the executions
	 * @throws IOException			In case creating the TCP fails
	 */
	protected boolean runBothToCompletion(BaseExecution clientExecution, BaseExecution serverExecution)
		throws InterruptedException, ExecutionException, IOException
	{
		/* Create sockets, turn off background receiving and install interceptors when specified */
		TCP.Socket clientSocket = new TCP(CLIENT_ADDRESS).socket();
		TCP.Socket serverSocket = new TCP(SERVER_ADDRESS).socket(SERVER_PORT);

		/* If we want to know exactly what happened, add two PacketFlowInspectors that track what
		 * happens. */
		if(tellMeExactlyWhatHappened)
		{
			clientPreInspector = new PacketFlowInspector(false, true);
			clientPostInspector = new PacketFlowInspector(false, true);
			serverPreInspector = new PacketFlowInspector(false, true);
			serverPostInspector = new PacketFlowInspector(false, true);
			clientInterceptors.add(clientPreInspector);
			serverInterceptors.add(serverPreInspector);
			clientInterceptors.add(0, clientPostInspector);
			serverInterceptors.add(0, serverPostInspector);
		}
		if (clientInterceptors != null) clientSocket.installInterceptors(clientInterceptors);
		if (serverInterceptors != null) serverSocket.installInterceptors(serverInterceptors);
		
		/* Initialize executions */
		clientExecution.initAsClient(clientSocket, SERVER_IP, SERVER_PORT);
		serverExecution.initAsServer(serverSocket);
		
		/* Execute the threads and get Futures for the results */
		ExecutorService executorService = Executors.newFixedThreadPool(2);
		Future<Void> clientFuture = executorService.submit(clientExecution);
		Future<Void> serverFuture = executorService.submit(serverExecution);
		
		boolean bothCompleted = true;
		
		/* Wait for at most maxRunTime milliseconds for both the server and client to finish */
		long startTime = System.currentTimeMillis();
		try
		{
			clientFuture.get(maxRunTime, TimeUnit.MILLISECONDS);
		}
		catch (TimeoutException te) {
			clientFuture.cancel(true);
			bothCompleted = false;
		}
		long stopTime = System.currentTimeMillis();
		
		try
		{
			serverFuture.get(maxRunTime - (stopTime - startTime), TimeUnit.MILLISECONDS);
		}
		catch (TimeoutException te) {
			serverFuture.cancel(true);	
			bothCompleted = false;
		}		
		
		if(!bothCompleted)
		{
			/* The timeout expired and the futures were cancelled. They're still running however,
			 * and might still need to set values which are required by the tests. Some executions
			 * are as nice as to set their doneExecuting field to true because they know they are
			 * done. Others don't, maybe they can't. So let's check that field for a little while,
			 * and if nothing happens, well.. too bad: just return false. */
			startTime = System.currentTimeMillis();
			if(TCP.DEBUG) Log.d(TAG, "Waiting for executions to exit");
			while((!clientExecution.isDone() || !serverExecution.isDone()) &&
					System.currentTimeMillis() - startTime < MAX_WAIT_CANCELLED_EXECUTIONS)
			{
				Thread.sleep(10);
			}
			if(TCP.DEBUG) Log.d(TAG, "Done waiting for executions to exit waiting");
		}
		
		/* Now that everything is done, print exactly what happened */
		if(tellMeExactlyWhatHappened) printExactlyWhatHappened();
		
		return bothCompleted;
	}
	
	
	protected void printExactlyWhatHappened()
	{
		if(!TCP.INFO) return;
		Log.i(TAG, "Ok so here's the deal. Here's a list of all packet transfers.");
		LinkedList<PacketEvent> clientPreEvents = clientPreInspector.getActualPacketEvents();
		LinkedList<PacketEvent> clientPostEvents = clientPostInspector.getActualPacketEvents();
		LinkedList<PacketEvent> serverPreEvents = serverPreInspector.getActualPacketEvents();
		LinkedList<PacketEvent> serverPostEvents = serverPostInspector.getActualPacketEvents();
		Log.i(TAG, "Client sent+received " + clientPreEvents.size() + " packets, server " + serverPreEvents.size());
		Log.i(TAG, "CLIENT          SERVER");
		int largestListLength = Math.max(clientPostEvents.size(), serverPostEvents.size());
		for(int i = 0; i < largestListLength; i++)
		{
			boolean clientListWasEmpty = clientPreEvents.isEmpty();
			boolean serverListWasEmpty = serverPreEvents.isEmpty();
			PacketEvent clientPreEvent = clientListWasEmpty ? null : clientPreEvents.remove();
			PacketEvent clientPostEvent = clientListWasEmpty ? null : clientPostEvents.remove();
			PacketEvent serverPreEvent = serverListWasEmpty ? null : serverPreEvents.remove();
			PacketEvent serverPostEvent = serverListWasEmpty ? null : serverPostEvents.remove();
			
			/* Check if a packet was dropped. A packet was dropped if it didn't reach the
			 * postInspector (which is behind the drop interceptor for outgoing packets). */
			if(clientPostEvent == null && !clientListWasEmpty)
			{
				/* The packet sent by the client was dropped. This means we shouldn't be looking at
				 * the event from the server, because the packet didn't reach the server. Put that
				 * event back! */
				if(!serverListWasEmpty)
				{
					serverPreEvents.add(0, serverPreEvent);
					serverPostEvents.add(0, serverPostEvent);
				}
				/* Now draw the failed transfer */
				Log.i(TAG, packetSixChars(clientPreEvent.flagSet, clientPreEvent.hasData) + " >---X    ");
			}
			else if(serverPostEvent == null && !serverListWasEmpty)
			{
				/* The next packet was one sent by the server. It got dropped. */
				if(!clientListWasEmpty)
				{
					clientPreEvents.add(0, clientPreEvent);
					clientPostEvents.add(0, clientPostEvent);
				}
				Log.i(TAG, "          X---< " + packetSixChars(serverPreEvent.flagSet, serverPreEvent.hasData));
			}
			else 
			{
				/* Ok so no packets were dropped. Find out if we're dealing with a simultaneous
				 * transfer or a simple transfer. */
				if(clientPreEvent == null || serverPreEvent == null)
				{
					Log.w(TAG, "Unexpected null event. I'm not telling you anything until you fix me.");
					return;
				}
				boolean clientIncoming = clientPreEvent.incoming;
				boolean serverIncoming = serverPreEvent.incoming;
				if(!clientIncoming && serverIncoming)
				{
					/* The client sent a packet to the server */
					Log.i(TAG, packetSixChars(clientPostEvent.flagSet, clientPostEvent.hasData) + " >------> ");
				}
				else if(clientIncoming && !serverIncoming)
				{
					/* The server sent a packet to the client */
					Log.i(TAG, "       <------< " + packetSixChars(serverPostEvent.flagSet, serverPostEvent.hasData));
				}
				else 
				{
					/* Simultaneous packets. First get the next two events because they crossed each
					 * other. Even before that do a quick sanity check: */
					i++;
					if(clientIncoming && serverIncoming)
					{
						Log.w(TAG, "Now I see two incoming packets on both sides which have not been sent.. This can't be.. Dying!");
						return;
					}
					PacketEvent nextClientEvent = clientPreEvents.remove();
					clientPostEvents.remove();
					PacketEvent nextServerEvent = serverPreEvents.remove();
					serverPostEvents.remove();
					/* And another sanity check */
					if(!nextClientEvent.incoming || !nextServerEvent.incoming)
					{
						Log.w(TAG, "Now I see outgoing packets which I'm sure must both arrive, but one or both didn't.. This can't be.. Dying!");
						return;
					}
					/* Print the sending of the packets */
					Log.i(TAG, packetSixChars(clientPostEvent.flagSet, clientPostEvent.hasData) + " >--\\/--< " +
							packetSixChars(serverPostEvent.flagSet, serverPostEvent.hasData));
					/* Now print the arriving */
					Log.i(TAG, "       <--/\\--> ");
				}
			}
		}
	}
	
	
	private static String packetSixChars(FlagSet flagSet, boolean hasData)
	{
		switch(flagSet)
		{
			case F_SYN:
				return "SYN   ";
			case F_SYNACK:
				return "SYNACK";
			case F_ACK:
				if(hasData) return "DATA  ";
				else return "EMPTY ";
			case F_FINACK:
				return "FINACK";
			default:
				return "INVALD";
		}
	}
	
	
	/**
	 * Fills an array with number bytes randomly generated. There is no guarantee this function will
	 * return the same result if called again.
	 * 
	 * @param number	The length of the array to return
	 * 
	 * @return	An array filled with randomly generated bytes
	 */
	protected static byte[] getRandomBytes(int number)
	{
		byte[] result = new byte[number];
		Random random = new Random();
		random.nextBytes(result);
		return result;
	}
	
	
	@Override
	protected void tearDown() throws Exception
	{
		clientExecution = serverExecution = null;
		clientSendData = serverSendData = null;
		clientInterceptors = serverInterceptors = null;
		super.tearDown();
	}
}
